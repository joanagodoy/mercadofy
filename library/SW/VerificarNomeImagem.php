<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VerificarNomeImagem
 *
 * @author FrancisG
 */
class SW_VerificarNomeImagem {

    public $imagem;
    public $prefix;
    public $pasta;
    public $extensao;
    public $newName;
    public $Arquivos = array();

    public function nameVerify($imagem, $pasta, $newName = NULL, $prefix = 'SW2_') {
        $this->imagem = $imagem;
        $this->newName = $newName;
        $this->pasta = $pasta;
        $this->extensao = $this->imagem['type'];
        $this->prefix = $prefix;

        switch ($this->extensao) {
            case 'image/pjpeg':
                $ext = 'jpg';
                break;
            case 'image/jpg':
                $ext = 'jpg';
                break;
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/gif':
                $ext = 'gif';
                break;
            case 'image/x-png':
                $ext = 'png';
                break;
            case 'image/png':
                $ext = 'png';
                break;
            default:
                $ext = 'jpg';
        }

// VERIFICA SE O DIRETORIO EXISTE, CASO NAO EXISTA
// ELE CRIA O MESMO COM PERMISSÃƒO 777
        if (!is_dir($this->pasta)) {
            $mask = umask(0);
            mkdir($this->pasta, 0777);
            umask($mask);
        }

// ABRE O DIRETORIO INDICADO
        $paste = opendir($this->pasta);

// CRIO DUAS VARIAVES PARA COMPARAÃ‡ÃƒO UTILIZADA MAIS A FRENTE
        $condicao = false;

        while ($item = readdir($paste)) {
            if ($item != "." && $item != "..") {
                $name = basename($item, '.' . $ext);
                $contagem = explode("_", $name);

// NOVO ARRAY
                if (isset($contagem[1])) {
                    $this->Arquivos[] = $contagem[1];
                }
            }
        }

// Ordenacao
        $i = 0;
        if (!empty($this->Arquivos)) {
            sort($this->Arquivos, SORT_NUMERIC);

// Execucao ordenada
            foreach ($this->Arquivos as $contagem) {

                if ($i > $contagem || $i != $contagem) {
                    $this->newName = $this->prefix . $this->newName . $i . '.' . $ext;
                    $condicao = true;
                    break;
                } else {
                    $i++;
                    $condicao = false;
                }
            }
        }
        if (!$condicao) {
            $this->newName = $this->prefix . $this->newName . $i . '.' . $ext;
        }
    }

    public function novoNome() {
        return $this->pasta . $this->newName;
    }

}

?>
