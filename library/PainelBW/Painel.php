<?php

class PainelBW_Painel extends Zend_Controller_Action {

    private $base;
    private $module;
    private $controller;
    private $action;

    public function isMobile() {
        $iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
        $palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
        $berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
        $ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $symbian = strpos($_SERVER['HTTP_USER_AGENT'], "Symbian");

        if ($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true) {
            return true;
        } else {
            return false;
        }
    }

    public function init() {
        $this->_initVars();
        Zend_Auth::getInstance()->hasIdentity();
        $controller = $this->controller;
        $action = $this->action;

        $objCategoria = new Application_Model_DbTable_Categoria();

        $sqlCategoriasFiltros = "SELECT
                        c.*,
                        upper(left(remove_accents(c.nome),1)) as letra
                    FROM 
                        categorias c,
                        produtos p
                    WHERE 
                        p.categoria = c.id
                    GROUP BY
                        c.id
                    ORDER BY c.nome";

        $arrayListCategorias = $objCategoria->getAdapter()->fetchAll($sqlCategoriasFiltros);

        Zend_Layout::getMvcInstance()->assign('arrayListCatFiltros', $arrayListCategorias);

        $sqlMenu = "SELECT
                    *
                    FROM 
                    categorias
                    WHERE (categoria is null OR categoria = 0)
                    ORDER BY nome";

        $arrayListMenuPai = $objCategoria->getAdapter()->fetchAll($sqlMenu);
        $arrayListItensMenu = array();

        if (!empty($arrayListMenuPai)) {
            foreach ($arrayListMenuPai as $umMenuPai) {
                $arrayListItensMenuFilho = array();
                $sqlSubmenu = "SELECT
                                *
                                FROM categorias
                                WHERE categoria = {$umMenuPai['id']}
                                ORDER BY nome";
                $arrayListMenuFilho = $objCategoria->getAdapter()->fetchAll($sqlSubmenu);

                if (!empty($arrayListMenuFilho)) {
                    foreach ($arrayListMenuFilho as $umMenuFilho) {
                        $arrayListItensMenuFilho[] = array(
                            'id' => $umMenuFilho['id'],
                            'nome' => $umMenuFilho['nome'],
                            'categoria' => $umMenuFilho['categoria'],
                            'icone' => strtolower($umMenuFilho['icone']),
                            'thumb' => strtolower($umMenuFilho['thumb']),
                            'foto' => strtolower($umMenuFilho['foto']),
                            'descricao' => $umMenuFilho['descricao'],
                            'meta' => $umMenuFilho['meta'],
                            'codigofiltro' => $umMenuFilho['codigofiltro'],
                            'description' => $umMenuFilho['description']
                        );
                    }
                }

                $arrayListItensMenu[]['menu'] = array(
                    'id' => $umMenuPai['id'],
                    'nome' => $umMenuPai['nome'],
                    'categoria' => $umMenuPai['categoria'],
                    'icone' => strtolower($umMenuPai['icone']),
                    'thumb' => strtolower($umMenuPai['thumb']),
                    'foto' => strtolower($umMenuPai['foto']),
                    'descricao' => $umMenuPai['descricao'],
                    'meta' => $umMenuPai['meta'],
                    'meta' => $umMenuPai['meta'],
                    'codigofiltro' => $umMenuPai['codigofiltro'],
                    'submenu' => $arrayListItensMenuFilho
                );
            }
        }

        Zend_Layout::getMvcInstance()->assign('arrayListMenu', $arrayListItensMenu);

    }

    private function _initVars() {
        $this->base = $this->getFrontController()->getBaseUrl();
        $this->module = $this->view->Module = $this->getRequest()->getModuleName();
        $this->controller = $this->view->Controller = $this->getRequest()->getControllerName();
        $this->action = $this->view->Action = $this->getRequest()->getActionName();

        // verifica se tenho um mercado com o subdominio validado
        // mesma funcao no ajaxcontroller.php
        $objMercado = new Application_Model_DbTable_Mercado();
//        $qMercado = $objMercado->fetchRow("subdomain = '".URL."' AND ativo in (2,3)");
        $qMercado = $objMercado->fetchRow("id = 19 AND ativo in (2,3)");        

        $this->mercadoMaster = $this->view->mercadoMaster = $qMercado;

        if (!empty($_SESSION['idMercadoMaster'])) {
            if ($_SESSION['idMercadoMaster'] != $qMercado['id']) {
                
                $_SESSION['idMercadoMaster'] = $qMercado['id'];
                // zera o carrinho também
                unset($_SESSION['Default']['usuario']['carrinho']);
            }
        } else {
            $_SESSION['idMercadoMaster'] = $qMercado['id'];
        }

        Zend_Layout::getMvcInstance()->assign('mercadoMaster', $qMercado);


        $objBoasVindas = new Application_Model_DbTable_Lightbox();

        if (!empty($_SESSION['idMercadoMaster'])) {
            $qBoasVindas = $objBoasVindas->fetchAll("mercado = {$qMercado['id']}","tipo asc, ordem");
        } else {
            $qBoasVindas = $objBoasVindas->fetchAll("mercado is null","tipo asc, ordem");
        }

        Zend_Layout::getMvcInstance()->assign('boasVindas', $qBoasVindas);

        //tempo de cookie das mensagens de boas vindas
        $objCookie = new Application_Model_DbTable_MercadoCookie();

        if (!empty($_SESSION['idMercadoMaster'])) {
            $qCookie = $objCookie->fetchRow("mercado = {$qMercado['id']}");
            $qCookie['cookie_name'] = $qMercado['subdomain'];
        } else {
            $qCookie = $objCookie->fetchRow("mercado is null");
            $qCookie['cookie_name'] = 'suacomprafacil';
        }

        Zend_Layout::getMvcInstance()->assign('cookie', $qCookie);

        

    }

}
