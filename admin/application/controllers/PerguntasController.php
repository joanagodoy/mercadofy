<?php

class Default_PerguntasController extends PainelBW_Painel {

    public $request;

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }

    public function listagemAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $modelBW = new Application_Model_DbTable_ModelBW();

        $columns = array('mercados_textos.texto', 'mercados_textos.tipo');
        $sql = "SELECT "
                . "mercados_textos.texto, "
                . "IF(mercados_textos.tipo = 1, 'Perguntas Frequentes', 'Privacidade') AS tipoTexto, "
                . "mercados_textos.id "
                . "FROM mercados_textos "
                . "WHERE mercado = {$_SESSION['logado']['usuario']['idMercado']} ";

        $output = $modelBW->dinamicTable($sql, $columns, $post);
        echo json_encode($output);
    }

    public function indexAction() {
        
    }

    public function cadastroAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objPerguntas = new Application_Model_DbTable_Perguntas();


        if (!empty($post)) {

            if (isset($post['excluir'])) {
                $objPerguntas->delete("id = {$id}");
                $this->_redirect('/perguntas/');
            }

            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'texto' => array('Texto', $post['texto']),
                    'tipo' => array('Tipo', $post['tipo'])
                )
            );

            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {
                $arrayUsuario = array(
                    'texto' => $post['texto'],
                    'tipo' => $post['tipo'],
                    'Mercado' => $_SESSION['logado']['usuario']['idMercado']
                );

                if (empty($id)) {
                    $objPerguntas->save($arrayUsuario);
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    unset($_POST);
                } else {
                    $objPerguntas->save($arrayUsuario, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                }
            }
        }

        if (!empty($id)) {
            $this->view->id = $id;
            $dadosUsuario = $objPerguntas->fetchRow("id = {$id} AND mercado = {$_SESSION['logado']['usuario']['idMercado']}");
            if (!empty($dadosUsuario)) {
                $dadosUsuario = $dadosUsuario;
                $this->view->dadosUsuario = $dadosUsuario;
            } else {
                $this->_redirect('/perguntas/');
            }
        }
    }


}
