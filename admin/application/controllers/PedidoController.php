<?php

class Default_PedidoController extends PainelBW_Painel
{

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }

    public function indexAction() {
        $post = $this->getRequest()->getPost();
        $objMercado = new Application_Model_DbTable_Mercado();
        $objPedido = new Application_Model_DbTable_Pedido();

        if (empty($_SESSION['logado'])) {
            $this->_redirect('/autenticacao/logout');
        }

        if ($_SESSION['logado']['usuario']['idMercado'] != 0) {
            $str = " m.id = {$_SESSION['logado']['usuario']['idMercado']} ";    
        } else {
            $str = " 1 = 1 ";

            if (!empty($post['mercado'])) {
                $str = " m.id = {$post['mercado']} ";    
            }
            
        }

        if (!empty($post['devalor'])) {

            $ateValor = str_replace(".","",$post['devalor']);
            $ateValor = str_replace(",",".",$ateValor);

            $str .= " AND pedidos.valortotal >= {$ateValor} ";    
        }


        if (!empty($post['atevalor'])) {

            $ateValor = str_replace(".","",$post['atevalor']);
            $ateValor = str_replace(",",".",$ateValor);

            $str .= " AND pedidos.valortotal <= {$ateValor} ";    
        }

        if (!isset($post['from'])) {
            $dataInicial = date('Y-m-d');
            $_POST['from'] = date('d/m/Y');
        } else {
            $dataInicial = join('-', array_reverse(explode('/', $post['from'])));
            
        }

        $str .= " AND date(pedidos.data) >= '{$dataInicial}' ";

        if (!isset($post['to'])) {
            $dataFinal = date('Y-m-d');
            $_POST['to'] = date('d/m/Y');
        } else {
            $dataFinal = join('-', array_reverse(explode('/', $post['to'])));
            
        }

        $str .= " AND date(pedidos.data) <= '{$dataFinal}' ";

        if (!empty($post['mercado'])) {
            $str .= " AND m.id = {$post['mercado']} ";
        }

        $sql = "SELECT "
            . "pedidos.id, "
            . "date_format(pedidos.data,'%d/%m/%Y') as data, "
            . "date_format(pedidos.dataentregaretirada,'%d/%m/%Y') as dataentregaretirada, "
            . "pedidos.valortotal, "
            . "IF(pedidos.status = 1, 'Ativo', 'Inativo') AS status, "
            . "IF(pedidos.statuspagamento = 1, 'Ativo', 'Inativo') AS statuspagamento, "
            . "pedidos.horaentrega, "
            . "pedidos.horaretirada, "
            . "pedidos.tipoentrega, "
            . "m.nome as nomeMercado, "
            . "pedidos.id "
            . "FROM pedidos "
            . "LEFT JOIN mercados m ON (m.id = pedidos.idMercado) "
            . "LEFT JOIN clientes c ON (c.id = pedidos.idCliente) "
            . "WHERE {$str} "
            . "ORDER BY pedidos.id desc ";

        $this->view->arrayPedidos = $objMercado->getAdapter()->fetchAll($sql);

        $this->view->arrayMercados = $objMercado->fetchAll("1 = 1", "nome ASC");
    }


    public function verificaCodBarras($idProduto) {
        $objProduto = new Application_Model_DbTable_Produto();

        $dadosProduto = $objProduto->fetchRow("id = {$idProduto}");

        $path = ROOT_PATH . '/public/uploads/produtos/codbarras/' . $idProduto . ".png";

        if (!file_exists($path)) {

            $barcodeOptions = array('text' => $dadosProduto['ean']);
            $rendererOptions = array();
            $barcodeImage = Zend_Barcode::draw(
                'code128', 'image', $barcodeOptions, $rendererOptions
            );

            ob_start();
            imagepng($barcodeImage);
            imagedestroy($barcodeImage);
            $imagedata = ob_get_contents();
            ob_end_clean();
            $response = base64_encode($imagedata);
            file_put_contents($path,base64_decode($response));
        }
    }

    public function impressaoAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();
        $objPedido = new Application_Model_DbTable_Pedido();
        $objProduto = new Application_Model_DbTable_Produto();
        $objPedidoItem = new Application_Model_DbTable_PedidoItem();
        $objProdutoMercado = new Application_Model_DbTable_ProdutoMercado();
        $objStatus = new Application_Model_DbTable_PedidoStatus();

        $this->view->arrayListStatus = $objStatus->fetchAll();

        if (empty($id)) {
            $this->redirect('/');
        } else {
            $this->view->id = $id;

            $dadosUsuario = $objPedido->getDadosPedido($id , $_SESSION['logado']['usuario']['idMercado']);

            foreach ($dadosUsuario['itens'] as $umItem) {
                $this->verificaCodBarras($umItem['produto']['id']);
            }

            if (!empty($dadosUsuario)) {
                $this->view->dadosPedido = $dadosUsuario;
            } else {
                $this->_redirect('/pedido/');
            }
        }
    }

    public function visualizarAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();
        $objPedido = new Application_Model_DbTable_Pedido();
        $objProduto = new Application_Model_DbTable_Produto();
        $objPedidoItem = new Application_Model_DbTable_PedidoItem();
        $objProdutoMercado = new Application_Model_DbTable_ProdutoMercado();
        $objStatus = new Application_Model_DbTable_PedidoStatus();

        $this->view->arrayListStatus = $objStatus->fetchAll();

        if (empty($id)) {
            $this->redirect('/');
        } else {
            $this->view->id = $id;

            $dadosUsuario = $objPedido->getDadosPedido($id , $_SESSION['logado']['usuario']['idMercado']);

            foreach ($dadosUsuario['itens'] as $umItem) {
                $this->verificaCodBarras($umItem['produto']['id']);
            }

            if (!empty($dadosUsuario)) {
                $this->view->dadosPedido = $dadosUsuario;
            } else {
                $this->_redirect('/pedido/');
            }

            if (!empty($post)) {
                $arrayUsuario = array(
                    'status' => $post['status']
                );

                $alterou = false;
                if ($dadosUsuario['status']['id'] != $post['status']) {
                    $alterou = true;
                }

                $objPedido->save($arrayUsuario, "id = {$id}");

                foreach ($dadosUsuario['itens'] as $umItem) { 
                    $arrayUsuario = array(
                        'observacoes' => $post['observacoes_'.$umItem['id']]
                    );

                    $objPedidoItem->save($arrayUsuario, "id = {$umItem['id']}");
                }

                $dadosUsuario = $objPedido->getDadosPedido($id , $_SESSION['logado']['usuario']['idMercado']);
                $front = Zend_Controller_Front::getInstance()->getBaseUrl();
                $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;
                
                $troco = "";

                if (!empty($dadosUsuario['troco'])) { 
                    $troco = " Troco para: " . $dadosUsuario['troco'];
                }

                $this->view->dadosPedido = $dadosUsuario;

                // envia e-mail para o usuário da alteração de status
                if ($alterou) {

                    //se status cancelar, voltar estoque do produto para o mercado
                    if ($post['status'] == '6') {
                        foreach($dadosUsuario['itens'] as $umItem) {
                            $qEstoque = $objProdutoMercado->fetchRow("idProduto = {$umItem['produto']['id']} AND idMercado = {$dadosUsuario['idMercado']}");
                            if (!empty($qEstoque)) {
                                $arrayProduto = array(
                                    'estoque' => $qEstoque['estoque'] + $umItem['quantidade']
                                );
                                $objProdutoMercado->save($arrayProduto, "id = {$qEstoque['id']}");
                            }
                        }
                    }

                    $mail = new Zend_Mail('utf-8');
                    
                    $mail->setSubject('Pedido '.$dadosUsuario['status']['nome'].'');
                    $mail->setFrom('mercadofiqueemcasa@gmail.com', $this->mercadoMaster['nome']);
                    
                    $html = array();
                    $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
		            $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
					$html[] = '<tr>';
					$html[] = '<td width="600" align="center">';
					$html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
					$html[] = '<tr>';
					$html[] = '<td height="80" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
					
					if (!empty($this->mercadoMaster['foto2'])) {
							
					  $html[] = '<img src="' . $baseUrl2 . '/uploads/mercados/'.$dadosUsuario['mercado']['foto2'].'" width="200" />';      
					} else {
					  $html[] = '<img src="' . $baseUrl . '/images/logo-email.png" />';      
					}
					
					$html[] = '</td>';
					$html[] = '</tr>';

					$html[] = '<tr><td><table style="width:540px!important;" align="center">';
					
					$html[] = '<tr><td bgcolor="#FFFFFF" valign="center" colspan="4">';
					$html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                    if ($dadosUsuario['status']['id'] == 1) {
                        $html[] = 'Olá '.$dadosUsuario['cliente']['nome'].'<br /><br />';
                        $html[] = 'Seu pedido foi devidamente pago e já entrou no sistema para tratamento!<br /><br />';
                        $html[] = 'Obrigado pelo privilégio de poder abastecer sua casa. Sua segurança e conforto são a nossa missão e prioridade ❤!<br /><br />';
                        $html[] = 'Enviamos abaixo o relatório de suas compras, este serve como recibo, é bom sempre dar uma olhadinha só pra garantir que não faltou nada e se tiver algum problema já pode dar uma avisadinha pra gente no Whatsapp ou aqui no W-mail para que possamos garantir sua satisfação. <br /><br />';
//                        $html[] = 'É importante resaltar que precisamos de um adulto acima de 18 anos presenta para fazer a entrega, principalmente caso haja bebidas alcóolicas no seu pedido. <br /><br />';
//                        $html[] = 'Pela situação atual, podem faltar alguns produtos no nosso estoque, caso isso aconteça a gente substituirá por um produto similar da mesma qualidade ou superior.  <br /><br />';
                        $html[] = 'Mais uma ultima coisa, por favor continue se cuidando, a gente sabe que é meio chato então escolhemos aqui um vídeo engraçado para animar sua quarentena, fique seguro e dê MUITAS risadas. <br /><br />';
                        $html[] = 'Forte abraço (virtual obviamente), em breve estaremos entregando suas comprinhas.<br /><br />';
                        $html[] = '<a href="'.$baseUrl.'" style="background:#71a505!important;color:#ffffff!important;display:block!important;height:40px!important;line-height:40px!important;text-decoration:none!important;width:200px!important;text-align:center!important;margin:0 auto!important;">Assistir Vídeo!</a><br />';
                    }else{
                        $html[] = 'Olá '.$dadosUsuario['cliente']['nome'].', <br><br> Estamos enviando essa mensagem para avisar que o status do seu pedido '.str_pad($id, 10, '0', STR_PAD_LEFT).' foi alterado para '.$dadosUsuario['status']['nome'].'.<br />';
                    }
                                        
                    $html[] = '</p>';
					$html[] = '<table style="width:100%!important;margin-bottom:20px;border-bottom:1px solid #eaeaea!important;"><tr>';
					
                    if ($dadosUsuario['status']['id'] == 6) { 

                        $html[] = '<td style="color:#71a505!important;font-size:14px!important;"><table><tr><td><img style="padding-right:5px;" src="' . $baseUrl . '/images/check.jpg" /></td><td>Pedido<br />Recebido</td></tr></table></td>';    

                        $html[] = '<td style="color:#b2281d!important;font-size:14px!important;"><table><tr><td><img style="padding-right:5px;" src="' . $baseUrl . '/images/cancel.jpg" /></td><td>Pedido<br />Cancelado</td></tr></table></td>';    


                    } else {

                        
                         foreach($this->view->arrayListStatus as $umStatus) {

                            //cancelado nao mostra
                            if ($umStatus['id'] == 6) {
                                continue;
                            }
                                                        
                            
                            //Aguardando Pagamento Boleto nao mostra
                            if ($umStatus['id'] == 8) {
                                continue;
                            }
                            
                            //Null nao mostra
                            if ($umStatus['id'] == 7) {
                                continue;
                            }

                            //se for retirada nao mostra pronto para entrega
                            if ($dadosUsuario['tipoentrega'] == 2 && $umStatus['id'] == 3) {
                                continue;
                            }

                            //se for entrega nao mostra pronto para retirada
                            if ($dadosUsuario['tipoentrega'] == 1 && $umStatus['id'] == 4) {
                                continue;
                            }
                            
                           


                            $color = "#666666";
                            $icon = "arrow.jpg";
                            /* variáveis para usar no status cancelado, no cancelado deve ficar assim: Pedido Recebido (em verde) > Pedido Cancelado (em vermelho)
                            $colorCancel = "#b2281d";
                            $iconCancel = "cancel.jpg";
                            */
                            if ($dadosUsuario['status']['id'] >= $umStatus['id']) {
                                $color = "#71a505";
                                $icon = "check.jpg";
                            }

                            $html[] = '<td style="color:'.$color.'!important;font-size:14px!important;"><table><tr><td><img style="padding-right:5px;" src="' . $baseUrl . '/images/'.$icon.'" /></td><td>Pedido<br />'.$umStatus['nome'].'</td></tr></table></td>';    

                            
                        }
                    }
                   
                    
                    //$html[] = '<td style="color:#666666!important;font-size:14px!important;"><table><tr><td><img style="padding-right:5px;" src="' . $baseUrl . '/images/arrow.jpg" /></td><td>Pedido<br />Em separação</td></tr></table></td>';
                    //$html[] = '<td style="color:#666666!important;font-size:14px!important;"><table><tr><td><img style="padding-right:5px;" src="' . $baseUrl . '/images/arrow.jpg" /></td><td>Pedido<br />Pronto para entrega</td></tr></table></td>';
                    //$html[] = '<td style="color:#666666!important;font-size:14px!important;"><table><tr><td><img style="padding-right:5px;" src="' . $baseUrl . '/images/arrow.jpg" /></td><td>Pedido<br />Entregue</td></tr></table></td>';
					
                    $html[] = '</tr></table>';
		            $html[] = '</td></tr>';
					
                    foreach ($dadosUsuario['itens'] as $umItem) {

                        $totalQde = 0;
                        $totalPreco = 0;

                        if ($umItem['produto']['tipo'] == 0 ) { 
                            $totalPreco = number_format($umItem['preco'] * $umItem['quantidade'], 2, ',', '.');
                            
                            if ($umItem['quantidade'] < 1000) {
                                $totalQde = $umItem['quantidade'] . " grama(s)"; 
                            } else {
                                $totalQde = $umItem['quantidade'] / 1000 . " kg"; 
                            }
                            
                        } else {
                            $totalPreco = number_format($umItem['preco'], 2, ',', '.');
                            $totalQde = $umItem['quantidade'];
                        } 


                        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;">'.$umItem['produto']['nome'].'</td>';
                        $html[] = '<td cellpadding="0" cellspacing="0" style="color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;"><table style="width:100%!important;"><tr><td style="width:70%!important;text-align:right!important;">'.$totalQde.'</td><td style="width:30%!important;text-align:right!important;">R$ '.$totalPreco.'</td></tr></table></td></tr>';    
                    }
					
					
					$html[] = '<tr><td cellpadding="0" cellspacing="0" style="width: 100%!important;height: 10px!important;margin: 0!important;padding: 0!important;" colspan="2">&nbsp;</td></tr>';
					
					$html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-top:1px solid #eaeaea!important;border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Supermercado</td>';
					$html[] = '<td cellpadding="0" cellspacing="0" style="border-top:1px solid #eaeaea!important;border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'.$dadosUsuario['mercado']['nome'].'</td>';
					$html[] = '</tr>';
		            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;width:50%;">Número do pedido</td>';
					$html[] = '<td cellpadding="0" cellspacing="0" style="text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;width:50%;">#'. str_pad($dadosUsuario['id'], 10, '0', STR_PAD_LEFT) .'</td></tr>';

                    if ($dadosUsuario['tipoentrega'] == 1) {
                        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Entrega</td>';
                    } else {
                        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Retirada</td>';    
                    }
					
					$html[] = '<td cellpadding="0" cellspacing="0" style="text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'. $dadosUsuario['horaentrega'] .'</td></tr>';
					$html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Forma de Pagamento</td>';
					$html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'.$dadosUsuario['formaDePagamento']['nome'] . $troco .'</td></tr>';
					$html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Valor da compra</td>';
					$html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">R$ '.number_format($dadosUsuario['valortotal'], 2, ',', '.').'</td></tr>';
                    if ($dadosUsuario['tipoentrega'] == 1) {
    					$html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Taxa de Serviço</td>';
    					$html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">R$ '.number_format($dadosUsuario['taxacobrada'], 2, ',', '.').'</td></tr>';
                    }
					$html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px solid #eaeaea!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;font-weight:bold!important;">Valor total</td>';
					$html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px solid #eaeaea!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;font-weight:bold!important;">R$ '.number_format($dadosUsuario['taxacobrada'] + $dadosUsuario['valortotal'], 2, ',', '.').'</td></tr>';
					
					$html[] = '<tr><td bgcolor="#FFFFFF" valign="center" colspan="2">';
//					$html[] = '<h2 style="padding-top:10px!important;color:#666666!important;font-size:16px!important;text-align:center!important;">Dúvidas ou mudanças? Você pode entrar em contato diretamente com o supermercado!</h2>';
//		            $html[] = '</td></tr>';
//                                        '.$dadosUsuario['mercado']['rua'].' - '.$dadosUsuario['mercado']['numero'].' - '.$dadosUsuario['mercado']['bairro'].'  <br />
					
					$html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:center!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;font-weight:bold!important;width:120px!important;"><img src="' . $baseUrl . '/uploads/mercados/'.$dadosUsuario['mercado']['foto'].'" width="120" height="120" /></td>';
					$html[] = '<td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;width:400px!important;padding-left:20px!important;">'.$dadosUsuario['mercado']['nome'].'<br />
					WhatsApp: <a href="https://api.whatsapp.com/send?1=pt_BR&amp;phone=55'.str_replace(" ","",str_replace("-","",str_replace(")","",str_replace("(","",$dadosUsuario['mercado']['telefone'])))).'" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=pt-BR&amp;q=https://api.whatsapp.com/send?1%3Dpt_BR%26phone%3D55'.str_replace(" ","",str_replace("-","",str_replace(")","",str_replace("(","",$dadosUsuario['mercado']['telefone'])))).'&amp;source=gmail&amp;ust=1513457605322000&amp;usg=AFQjCNFOHMDU85XWet-n_YWt3xReCeqtgg">'.$dadosUsuario['mercado']['telefone'].'</a> <br /> E-mail: '.$dadosUsuario['mercado']['email'].' </td></tr>';
					
					$html[] = '</table></td></tr>';

                    $html[] = '<tr>';
					$html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
					$html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">'.$baseUrl.'</a>';
					$html[] = '</td>';
					$html[] = '</tr>';
					$html[] = '</table>';
					$html[] = '</td>';
					$html[] = '</tr>';
					$html[] = '</table>';
					$html[] = '</table></td></tr>';

                    $mail->setBodyHtml(join("\n", $html));
                    $mail->addTo($dadosUsuario['cliente']['email'], $dadosUsuario['cliente']['email']);
                    
                    $objConfigMail = new Application_Model_ConfigMail();
                    $transport = $objConfigMail->get_configuracao_mail();
                    $mail->send($transport);
                }

                $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                unset($_POST);
                unset($post);

            }

            
        }

        

    }


}

