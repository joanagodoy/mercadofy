<?php

class Default_AtributoController extends PainelBW_Painel
{

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }
    

    public function indexAction() {

    }

    public function cadastroAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objUsuario = new Application_Model_DbTable_Atributo();

        if (!empty($post)) {
            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'tipo' => array('Tipo', $post['tipo']),
                    'valor' => array('Valor', $post['valor'])
                )
            );

            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (empty($id)) {
                $id = 0;
            }

            $dadosUsuario = $objUsuario->fetchRow("tipo = {$post['tipo']} AND valor = '{$post['valor']}' AND id <> {$id}");

            if (!empty($dadosUsuario)) {
                $erros[] = 'Já existe um atributo cadastrado com esse nome.';
            }

            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {
                $arrayUsuario = array(
                    'tipo' => $post['tipo'],
                    'valor' => $post['valor']
                );

                if (empty($id)) {
                    $objUsuario->save($arrayUsuario);
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    unset($_POST);
                } else {
                    $objUsuario->save($arrayUsuario, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                }
            }
        }

        if (!empty($id)) {
            $this->view->id = $id;
            $dadosUsuario = $objUsuario->fetchRow("id = {$id}");
            if (!empty($dadosUsuario)) {
                $dadosUsuario = $dadosUsuario;
                $this->view->dadosUsuario = $dadosUsuario;
            } else {
                $this->_redirect('/atributo/');
            }
        }
    }

    public function listagemAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $action = $post['action'];
        $modelBW = new Application_Model_DbTable_ModelBW();

        $columns = array('atributos.tipo', 'atributos.valor');
        $sql = "SELECT "
                . "case "
                . "when atributos.tipo = 0 then 'Marca' "
                . "when atributos.tipo = 1 then 'Sabor' "
                . "when atributos.tipo = 2 then 'Embalagem' "
                . "when atributos.tipo = 3 then 'Tamanho' "
                . "when atributos.tipo = 4 then 'Unidade de Medida' "
                . "when atributos.tipo = 5 then 'Países' "
                . "when atributos.tipo = 6 then 'Tipo de Produto' "
                . "end as nome, "
                . "atributos.valor, "
                . "atributos.id "
                . "FROM atributos WHERE 1 = 1 ";
        $output = $modelBW->dinamicTable($sql, $columns, $post);
        echo json_encode($output);

    }

}

