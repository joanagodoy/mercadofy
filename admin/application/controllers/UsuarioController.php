<?php

class Default_UsuarioController extends PainelBW_Painel {

    public $request;

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }

    public function listagemAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $action = $post['action'];
        $modelBW = new Application_Model_DbTable_ModelBW();

        if ($action == 'nivel') {
            $columns = array('nivel');
            $sql = "SELECT nivel, id FROM usuarios_niveis WHERE mercado = {$_SESSION['logado']['usuario']['idMercado']}  ";

        } else if ($action == 'newsletter') {
            $columns = array('email');
            $sql = "SELECT email, id FROM newsletter ";

        } else {
            $columns = array('usuarios.nome', 'usuarios.cpf', 'usuarios_niveis.nivel', 'usuarios.ativo');
            $sql = "SELECT "
                    . "usuarios.nome, "
                    . "usuarios.cpf, "
                    . "usuarios_niveis.nivel, "
                    . "IF(usuarios.ativo = 1, 'Ativo', 'Inativo') AS statusUsuario, "
                    . "usuarios.id "
                    . "FROM usuarios "
                    . "INNER JOIN usuarios_niveis ON usuarios_niveis.id = usuarios.idNivel "
                    . "WHERE idMercado = {$_SESSION['logado']['usuario']['idMercado']} ";
        }
        $output = $modelBW->dinamicTable($sql, $columns, $post);
        echo json_encode($output);
    }

    public function indexAction() {
        
    }

    public function cadastroAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objUsuario = new Application_Model_DbTable_Usuario();

        $this->view->arrayListNiveis = $objUsuario->getNivel()->fetchAll("mercado = {$_SESSION['logado']['usuario']['idMercado']}", "nivel ASC");

        if (!empty($post)) {
            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'nome' => array('Nome', $post['nome']),
                    'email' => array('E-Mail', $post['email']),
                    'cpf' => array('CPF', $post['cpf']),
                    'nivel' => array('Nível', $post['nivel'])
                )
            );

            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (!empty($id)) {
                if (empty($post['senha'])) {
                    $arrayListValidacao['NotEmpty']['senha'] = array('Senha', $post['senha']);
                }
            }

            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {
                $arrayUsuario = array(
                    'nome' => $post['nome'],
                    'cpf' => $post['cpf'],
                    'email' => $post['email'],
                    'idNivel' => $post['nivel'],
                    'idMercado' => $_SESSION['logado']['usuario']['idMercado'],
                    'ativo' => $post['ativo']
                );
                if (!empty($post['senha'])) {
                    $arrayUsuario['senha'] = hash('sha512', $post['senha']);
                }
                if (empty($id)) {
                    $objUsuario->save($arrayUsuario);
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    unset($_POST);
                } else {
                    $objUsuario->save($arrayUsuario, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                }
            }
        }

        if (!empty($id)) {
            $this->view->id = $id;
            $dadosUsuario = $objUsuario->fetchRow("id = {$id}");
            if (!empty($dadosUsuario)) {
                $dadosUsuario = $dadosUsuario;
                $this->view->dadosUsuario = $dadosUsuario;
            } else {
                $this->_redirect('/usuario/');
            }
        }
    }

    public function nivelAction() {
        $objNivel = new Application_Model_DbTable_Nivel();
        $arrayListNiveis = $objNivel->fetchAll(null, "nivel ASC");
        $this->view->arrayListNiveis = $arrayListNiveis;
    }

    public function newsletterAction() {
    }

    public function nivelCadastroAction() {
        $id = (int) $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objNivel = new Application_Model_DbTable_Nivel();
        $objMenuNiveis = new Application_Model_DbTable_MenuNiveis();

        if (!empty($post)) {

            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'nivel' => array('Nível', $post['nivel'])
                )
            );

            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {
                $arrayNivel = array(
                    'nivel' => $post['nivel'],
                    'mercado' => $_SESSION['logado']['usuario']['idMercado']
                );

                if (empty($id)) {
                    $objNivel->save($arrayNivel);
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    unset($_POST);
                } else {
                    $objNivel->save($arrayNivel, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    if (!empty($post['itensMenu'])) {
                        $objMenuNiveis->delete("idNivel = {$post['idnivel']}");
                        foreach ($post['itensMenu'] as $umMenu) {
                            $objMenuNiveis->save(array('idMenu' => $umMenu, 'idNivel' => $post['idnivel']));
                        }
                    }
                }
            }
        }

        if (!empty($id)) {
            $this->view->id = $id;
            $dadosNivel = $objNivel->fetchRow("id = {$id} AND mercado = {$_SESSION['logado']['usuario']['idMercado']}");

            $tipoMenu = "0,2";

            if ($_SESSION['logado']['usuario']['idMercado'] == 0) {
                $tipoMenu = "0,1,2";
            }

            $objMenu = new Application_Model_DbTable_Menu();
            $arrayListMenuPai = $objMenu->fetchAll("idMenuPai IS NULL AND ativo = 1 AND master in ({$tipoMenu})", "ordem ASC");

            $arrayListItensMenu = array();

            if (!empty($arrayListMenuPai)) {
                foreach ($arrayListMenuPai as $umMenuPai) {
                    $arrayListItensMenuFilho = array();
                    $arrayListMenuFilho = $objMenu->fetchAll("idMenuPai = {$umMenuPai['id']} AND ativo = 1  AND master in ({$tipoMenu})", "ordem ASC");

                    if (!empty($arrayListMenuFilho)) {
                        foreach ($arrayListMenuFilho as $umMenuFilho) {
                            $arrayListItensMenuFilho[] = array(
                                'id' => $umMenuFilho['id'],
                                'idMenuPai' => $umMenuFilho['idMenuPai'],
                                'titulo' => $umMenuFilho['menu'],
                                'controller' => $umMenuFilho['controller'],
                                'action' => $umMenuFilho['action'],
                                'url' => $umMenuFilho['controller'] . '/' . (!empty($umMenuFilho['action']) && $umMenuFilho['action'] <> 'index' ? $umMenuFilho['action'] . '/' : false),
                                'current' => false
                                    //'current' => ($umMenuFilho['controller'] == $this->controller && $umMenuFilho['action'] == $this->action ? true : false)
                            );
                        }
                    }

                    $arrayListItensMenu[]['menu'] = array(
                        'id' => $umMenuPai['id'],
                        'titulo' => $umMenuPai['menu'],
                        'idMenuPai' => $umMenuPai['idMenuPai'],
                        'icone' => $umMenuPai['icone'],
                        'controller' => $umMenuPai['controller'],
                        'action' => $umMenuPai['action'],
                        'url' => (!empty($umMenuPai['action']) ? $umMenuPai['controller'] . '/' . (!empty($umMenuPai['action']) && $umMenuPai['action'] <> 'index' ? $umMenuPai['action'] . '/' : false) : '#'),
                        'current' => false,
                        //'current' => ($umMenuPai['controller'] == $this->controller ? true : false),
                        'submenu' => $arrayListItensMenuFilho
                    );
                }
            }
            $this->view->arrayListMenu = $arrayListItensMenu;

            $sql = "SELECT 
					id,
					menu,
					menus.idMenuPai
					FROM menus
					INNER JOIN menus_niveis ON menus_niveis.idMenu = menus.id
					WHERE menus_niveis.idNivel = {$id} AND menus.master in ({$tipoMenu})
					ORDER BY menus.idMenuPai, ordem ASC";
            $itensMenu = $objMenuNiveis->getAdapter()->fetchAll($sql);

            if (!empty($dadosNivel)) {
                $this->view->dadosNivel = $dadosNivel;
            } else {
                $this->_redirect('/usuario/nivel/');
            }
            if (!empty($itensMenu)) {
                $itens = array();
                foreach ($itensMenu as $umItemMenu) {
                    $itens[$umItemMenu['id']] = $umItemMenu['idMenuPai'];
                }
                $this->view->itensMenu = $itens;
            }
        }
    }

}
