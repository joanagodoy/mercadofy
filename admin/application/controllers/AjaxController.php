<?php

class Default_AjaxController extends Zend_Controller_Action {

    private $Base;

    public function init() {
        $this->_initVars();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction() {
        
    }

    public function revisarItemAction() {
        $post = $this->getRequest()->getPost();
        $objPedido = new Application_Model_DbTable_Pedido();
        $objPedidoItem = new Application_Model_DbTable_PedidoItem();
        if (!empty($post)) {

            if (!empty($post['id']) && is_numeric($post['quantidade']) && is_numeric($post['valor'])) {

                //pega os dados do item
                $qItem = $objPedidoItem->fetchRow("id = {$post['id']}");

                // adiciona no array os itens revisados
                $arrayItem = array(
                    'quantidadeRevisada' => $post['quantidade'],
                    'precoRevisado' => $post['valor'],
                    'revisado' => $post['revisado']
                );
                $objPedidoItem->save($arrayItem,"id = {$post['id']}");

                // pega todos os itens para fazer somatorio
                $qItens = $objPedidoItem->fetchAll("idPedido = {$qItem['idPedido']}");

                $totalItens = 0;
                $totalRevisado = 0;

                //soma itens revisados e o valor revisado total
                foreach ($qItens as $umItem) {
                    if ($umItem['revisado'] != 0) {
                        $totalItens++;
                        $totalRevisado += $umItem['preco'] * $umItem['quantidadeRevisada'];
                    }
                }

                //salva no cabecalho do pedido
                $arrayPedido = array(
                    'itensConferidos' => $totalItens,
                    'totalRevisado' => $totalRevisado
                );
                $objPedido->save($arrayPedido,"id = {$qItem['idPedido']}");

                //retorno json
                $qPedido = $objPedido->fetchRow("id = {$qItem['idPedido']}");
                $qPedido['itens'] = $objPedidoItem->fetchAll("idPedido = {$qItem['idPedido']}");

                echo json_encode($qPedido);
            }
        }
    }

    public function buscaProdutosAction() {
        $post = $this->getRequest()->getPost();
        if (!empty($post)) {

            $objProduto = new Application_Model_DbTable_Produto();

            $sql = "SELECT "
                        . "p.* "
                        . "FROM produtos p "
                        . "WHERE p.ean like '%{$post['q']}%' OR p.nome like '%{$post['q']}%' "
                        . "ORDER BY p.nome ";
            $dadosPedido['data'] = $objProduto->getAdapter()->fetchAll($sql);

            echo json_encode($dadosPedido);
        }
    }

    public function buscaNiveisAction() {
        $objNivel = new Application_Model_DbTable_Nivel();
        $arrayListNiveis = $objNivel->fetchAll(null, "nivel ASC");
        
        echo json_encode($arrayListNiveis);
    }

    public function buscaCepAction() {
        $post = $this->getRequest()->getPost();
        if (!empty($post)) {
            $cep = str_replace(".", "", $post['cep']);
            $cep = str_replace("-", "", $cep);
            
            $objPedido = new Application_Model_DbTable_Cidade();
            $sql = "SELECT "
                        . "l.*, e.id as idEstado "
                        . "FROM log_localidade l INNER JOIN enderecos_estados e ON l.ufe_sg = e.uf "
                        . "WHERE l.cep = '{$cep}' ";
            $dadosPedido = $objPedido->getAdapter()->fetchAll($sql);

            foreach ($dadosPedido as $keyPedidos => $umPedido) {
                $dados = array( 
                    'estado' => $umPedido['ufe_sg'],
                    'idEstado' => $umPedido['idEstado'],
                    'localidade' => strtoupper($umPedido['loc_no']),
                    'localidadecodigo' => $umPedido['loc_nu_sequencial'],
                    'bairro' => '',
                    'cep' => $umPedido['cep'],
                    'abreviado' => ''
                    );

                $dadosCep[] = $dados;
            }

            $sql = "SELECT "
                        . "logx.ufe_sg,  logx.log_nome,  loc.loc_no,  loc.loc_nu_sequencial, bai.bai_no, logx.cep, e.id as idEstado "
                        . "FROM log_logradouro logx  INNER JOIN enderecos_estados e ON logx.ufe_sg = e.uf INNER JOIN  log_localidade loc ON loc.loc_nu_sequencial = logx.loc_nu_sequencial  INNER JOIN  log_bairro bai ON bai.bai_nu_sequencial = logx.bai_nu_sequencial_ini  "
                        . "WHERE logx.cep = '{$cep}' ";
            $dadosPedido = $objPedido->getAdapter()->fetchAll($sql);

            foreach ($dadosPedido as $keyPedidos => $umPedido) {
                $dados = array( 
                    'estado' => $umPedido['ufe_sg'],
                    'idEstado' => $umPedido['idEstado'],
                    'localidade' => strtoupper($umPedido['loc_no']),
                    'localidadecodigo' => $umPedido['loc_nu_sequencial'],
                    'bairro' => $umPedido['bai_no'],
                    'cep' => $umPedido['cep'],
                    'abreviado' => $umPedido['log_nome']
                    );

                $dadosCep[] = $dados;
            }

            $sql = "SELECT "
                        . "gu.ufe_sg, gu.gru_no,  gu.gru_endereco,  loc.loc_no,  loc.loc_nu_sequencial,  bai.bai_no, gu.cep, e.id as idEstado "
                        . "FROM log_grande_usuario gu INNER JOIN enderecos_estados e ON gu.ufe_sg = e.uf INNER JOIN log_localidade loc ON loc.loc_nu_sequencial = gu.loc_nu_sequencial INNER JOIN  log_bairro bai ON bai.bai_nu_sequencial = gu.bai_nu_sequencial "
                        . "WHERE gu.cep = '{$cep}' ";
            $dadosPedido = $objPedido->getAdapter()->fetchAll($sql);

            foreach ($dadosPedido as $keyPedidos => $umPedido) {
                $dados = array( 
                    'estado' => $umPedido['ufe_sg'],
                    'idEstado' => $umPedido['idEstado'],
                    'localidade' => strtoupper($umPedido['loc_no']),
                    'localidadecodigo' => $umPedido['loc_nu_sequencial'],
                    'bairro' => $umPedido['bai_no'],
                    'cep' => $umPedido['cep'],
                    'abreviado' => $umPedido['gru_endereco']
                    );

                $dadosCep[] = $dados;
            }

            $sql = "SELECT "
                        . "uo.ufe_sg, uo.uop_no,  uo.uop_endereco,  loc.loc_no,  loc.loc_nu_sequencial, bai.bai_no, uo.cep, e.id as idEstado "
                        . "FROM log_unid_oper uo INNER JOIN enderecos_estados e ON uo.ufe_sg = e.uf  INNER JOIN  log_localidade loc ON loc.loc_nu_sequencial = uo.loc_nu_sequencial  INNER JOIN  log_bairro bai ON bai.bai_nu_sequencial = uo.bai_nu_sequencial "
                        . "WHERE uo.cep = '{$cep}' ";
            $dadosPedido = $objPedido->getAdapter()->fetchAll($sql);

            foreach ($dadosPedido as $keyPedidos => $umPedido) {
                $dados = array( 
                    'estado' => $umPedido['ufe_sg'],
                    'idEstado' => $umPedido['idEstado'],
                    'localidade' => strtoupper($umPedido['loc_no']),
                    'localidadecodigo' => $umPedido['loc_nu_sequencial'],
                    'bairro' => $umPedido['bai_no'],
                    'cep' => $umPedido['cep'],
                    'abreviado' => $umPedido['uop_endereco']
                    );

                $dadosCep[] = $dados;
            }

            $sql = "SELECT "
                        . " cp.ufe_sg, cp.cpc_no,  cp.cpc_endereco,  loc.loc_no, loc.loc_nu_sequencial, cp.cep, e.id as idEstado "
                        . "FROM log_cpc cp INNER JOIN enderecos_estados e ON cp.ufe_sg = e.uf   INNER JOIN  log_localidade loc ON loc.loc_nu_sequencial = cp.loc_nu_sequencial "
                        . "WHERE cp.cep = '{$cep}' ";
            $dadosPedido = $objPedido->getAdapter()->fetchAll($sql);

            foreach ($dadosPedido as $keyPedidos => $umPedido) {
                $dados = array( 
                    'estado' => $umPedido['ufe_sg'],
                    'idEstado' => $umPedido['idEstado'],
                    'localidade' => strtoupper($umPedido['loc_no']),
                    'localidadecodigo' => $umPedido['loc_nu_sequencial'],
                    'bairro' => '',
                    'cep' => $umPedido['cep'],
                    'abreviado' => $umPedido['cpc_endereco']
                    );

                $dadosCep[] = $dados;
            }

            echo json_encode($dadosCep);

        }
    }

    public function buscaCidadesAction() {
        $post = $this->getRequest()->getPost();
        if (!empty($post)) {
            $idEstado = $post['idEstado'];
            $idCidade = "";
            if (isset($post['idCidade'])) {
                $idCidade = $post['idCidade'];
            }

            $objCidades = new Application_Model_DbTable_Cidade();
            //$arrayListCidades = $objCidades->fetchAll("", "cidade ASC");
            $arrayListCidades = $objCidades->getAdapter()->fetchAll("SELECT * FROM enderecos_cidades WHERE idEstado = {$idEstado} GROUP BY cidade ORDER BY cidade ASC");
            if (!empty($arrayListCidades)) {
                $html[] = '<option value="">Selecione</option>';
                foreach ($arrayListCidades as $umaCidade) {
                    $selected = "";

                    if (strlen($idCidade) && $idCidade == $umaCidade['id']) {
                        $selected = "selected";
                    }

                    $html[] = '<option value="' . $umaCidade['id'] . '" '.$selected.' >' . $umaCidade['cidade'] . '</option>';
                }
                echo join("\n", $html);
            }
        }
    }

    private function _initVars() {
        $this->Base = $this->getFrontController()->getBaseUrl();
    }

}
