<?php

class Default_ProdutoController extends PainelBW_Painel
{

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }

    public function cosmosProdutosAction($codigoAtual = "") {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $objLog = new Application_Model_DbTable_Log();
        $objChaves = new Application_Model_DbTable_Chaves();
        $objChavesUso = new Application_Model_DbTable_ChavesUso();

        if (!empty($codigoAtual)) {

            $chave = $objChaves->getChave(false);

            if (empty($chave)) {
                $objLog->save(array('data' => Date('y-m-d H:i:s'), 'usuario' => $_SESSION['logado']['usuario']['idUsuario'], 'observacao' => "Não foi possível importar o produto {$codigoAtual} pois não tem chaves disponíveis."));

                $_SESSION['logado']['usuario']['msgImportacao'] = false;
                $_SESSION['logado']['usuario']['msgChaves'] = true;
                
                if ($_SESSION['logado']['usuario']['idMercado'] != 0) {
                    $this->_redirect('/produto/importacao-mercado/');
                } else {
                    $this->_redirect('/produto/importacao/');
                }
            } else {

                $url = 'https://api.cosmos.bluesoft.com.br/gtins/' . $codigoAtual;
                $headers = array(
                    "Content-Type: application/json",
                    "X-Cosmos-Token: " . $chave['chave']
                );

                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
                $dados = curl_exec($curl);
                curl_close($curl);

                $objLog->save(array('data' => Date('y-m-d H:i:s'), 'usuario' => $_SESSION['logado']['usuario']['idUsuario'], 'observacao' => "Retorno cosmos produto {$codigoAtual}: " . $dados));
                $objChavesUso->save(array('data' => Date('y-m-d H:i:s'), 'chave' => $chave['id']));

                if ($dados === false || $dados == NULL) {
                    return null;
                } else {
                    $object = json_decode($dados, true);
                    return $object;
                }
            }
            

        }

    }

    //altera produto pela listagem de produtos no campo nome
    public function alteracaoProdutoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $objProduto = new Application_Model_DbTable_Produto();
        $objAtributo = new Application_Model_DbTable_Atributo();
        $validacao = new Application_Model_Validacao();

        if (!empty($post['id']) && !empty($post['coluna']) && !empty($post['valor'])) {

            $alteracao = array();
            $alteracao[$post['coluna']] = $post['valor'];

            //se a coluna for unidade medida, preciso do id do item baseado no nome
            if ($post['coluna'] == 'unidademedida') {
                $qAtributo = $objAtributo->fetchRow("valor = '{$post['valor']}'");
                $alteracao[$post['coluna']] = $qAtributo['id'];
            }

            //ajusta o link/url do produto
            if ($post['coluna'] == 'nome') {
                $codigofiltro = $validacao->tirarAcentos($alteracao[$post['coluna']]);
                $alteracao['link'] = $codigofiltro;    
            }

            $objProduto->save($alteracao, "id = {$post['id']}");

            echo json_encode($alteracao);
        }
    }

    public function alteracaoAction() {
        $post = $this->getRequest()->getPost();
        $objProduto = new Application_Model_DbTable_Produto();
        $objCategoria = new Application_Model_DbTable_Categoria();
        $objAtributo = new Application_Model_DbTable_Atributo();

        
        if (isset($post['marqueiTodos'])) {
            $post['idProdutos'] = explode("," , $post['idProdutos']);
        }

        if (empty($post['idProdutos'])) {
            $this->_redirect('/produto/');
        }

        if (!empty($post['salvar'])) {

            if (is_array($post['idProdutos'])) {
                $idsProdutos = implode(",",$post['idProdutos']);    
            } else {
                $idsProdutos = $post['idProdutos'];    
            }

            if (empty($idsProdutos)) {
                $this->_redirect('/produto/');
            }

            //prepara o array de produtos
            $alteracao = array();

            if (!empty($post['categoria1'])) {
                $alteracao['categoria'] = $post['categoria1'];
            }

            if (!empty($post['categoria2'])) {
                $alteracao['categoria'] = $post['categoria2'];
            }

            if (!empty($post['categoria3'])) {
                $alteracao['categoria'] = $post['categoria3'];
            }

            if (!empty($post['marca'])) {
                $alteracao['marca'] = $post['marca'];
            }

            if (!empty($post['unidademedida'])) {
                $alteracao['unidademedida'] = $post['unidademedida'];
            }

            if (!empty($post['pesovolume'])) {
                $alteracao['pesovolume'] = $post['pesovolume'];
            }

            if (!empty($post['embalagem'])) {
                $alteracao['embalagem'] = $post['embalagem'];
            }

            if (!empty($post['pais'])) {
                $alteracao['pais'] = $post['pais'];
            }

            if (!empty($post['sabor'])) {
                $alteracao['sabor'] = $post['sabor'];
            }

            if (!empty($post['tamanho'])) {
                $alteracao['tamanho'] = $post['tamanho'];
            }

            if (!empty($post['tipo-atributo'])) {
                $alteracao['tipoAt'] = $post['tipo-atributo'];
            }

            if (isset($post['ativo'])) {
                $alteracao['ativo'] = $post['ativo'];
            }

            if (empty($alteracao)) {
                $this->_redirect('/produto/');
            }

            //atualiza produtos
            $objProduto->save($alteracao, "id in ({$idsProdutos})");

            $_SESSION['logado']['usuario']['msgAlteracao'] = true;
            $this->_redirect('/produto/');

        }

        $this->view->idProdutos = implode(",",$post['idProdutos']);

        $this->view->arrayListMarcas = $objAtributo->fetchAll("tipo = 0", "valor ASC");
        $this->view->arrayListSabores = $objAtributo->fetchAll("tipo = 1", "valor ASC");
        $this->view->arrayListEmbalagens = $objAtributo->fetchAll("tipo = 2", "valor ASC");
        $this->view->arrayListTamanhos = $objAtributo->fetchAll("tipo = 3", "valor ASC");
        $this->view->arrayListUnidades = $objAtributo->fetchAll("tipo = 4", "valor ASC");
        $this->view->arrayListPaises = $objAtributo->fetchAll("tipo = 5", "valor ASC");
        $this->view->arrayListTipos = $objAtributo->fetchAll("tipo = 6", "valor ASC");
        $this->view->arrayListCategoria = $objCategoria->fetchAll("categoria = 0 OR categoria is null", "nome ASC");

    }
    

    public function indexAction() {
        $objCategoria = new Application_Model_DbTable_Categoria();
        $objAtributo = new Application_Model_DbTable_Atributo();

        if (isset($_SESSION['logado']['usuario']['msgAlteracao'])) {

            if ($_SESSION['logado']['usuario']['msgAlteracao'] == false) {
                $erros[] = 'Não foi possível atualizar os produtos.';
                $this->view->importacao = array('erros' => $erros, 'sucesso' => false);
            } else {
                $this->view->importacao = array('erros' => false, 'sucesso' => true);    
            }

            unset($_SESSION['logado']['usuario']['msgAlteracao']);
            
        }


        // traz dados para a view para filtragem
        $this->view->arrayListMarcas = $objAtributo->fetchAll("tipo = 0", "valor ASC");
        $this->view->arrayListSabores = $objAtributo->fetchAll("tipo = 1", "valor ASC");
        $this->view->arrayListEmbalagens = $objAtributo->fetchAll("tipo = 2", "valor ASC");
        $this->view->arrayListTamanhos = $objAtributo->fetchAll("tipo = 3", "valor ASC");
        $this->view->arrayListUnidades = $objAtributo->fetchAll("tipo = 4", "valor ASC");
        $this->view->arrayListPaises = $objAtributo->fetchAll("tipo = 5", "valor ASC");
        $this->view->arrayListTipos = $objAtributo->fetchAll("tipo = 6", "valor ASC");
        $this->view->arrayListCategoria = $objCategoria->fetchAll("categoria = 0 OR categoria is null", "nome ASC");

    }

    public function listagemAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $modelBW = new Application_Model_DbTable_ModelBW();

        $action = $post['action'];

        


        if ($action == 'listagem-mercado') {
            $columns = array('produtos.ean', 'produtos.nome', 'produtos_mercados.preco', 'produtos_mercados.ativo');
            $sql = "SELECT "
                    . "produtos.ean, "
                    . "produtos.nome, "
                    . "produtos_mercados.preco, "
                    . "IF(produtos_mercados.ativo = 1, 'ATIVO', 'INATIVO') AS ativo, "
                    . "produtos_mercados.id "
                    . "FROM produtos, produtos_mercados "
                    . "WHERE produtos.id = produtos_mercados.idProduto AND produtos_mercados.idMercado = {$_SESSION['logado']['usuario']['idMercado']} ";

        } else {
            $columns = array(
                    "produtos.id",
                    "produtos.foto",
                    "produtos.nome",
                    "produtos.ean",
                    "c.nome",
                    "c2.nome",
                    "c3.nome",
                    "a.valor",
                    "un.valor",
                    "produtos.pesovolume",
                    "s.valor",
                    "t.valor",
                    "tp.valor",
                    "pa.valor",
                    "produtos.ativo",
                    "produtos.id ");

            $sql = "SELECT "
                    . "produtos.id, "
                    . "produtos.foto, "
                    . "produtos.nome, "
                    . "produtos.ean, "
                    . "c.nome as categoria, "
                    . "c2.nome as subcategoria, "
                    . "c3.nome as subcategoria2, "
                    

                    . "a.valor as marca, "
                    . "un.valor as unidadeDeMedida, "
                    . "produtos.pesovolume, "
                    . "s.valor as sabor, "
                    . "t.valor as tamanho, "
                    . "tp.valor as tipoProduto, "
                    . "pa.valor as pais, "

                    . "IF(produtos.tipo = 1, 'PADRÃO', 'GRANEL') AS tipoP, "
                    . "IF(produtos.ativo = 1, 'ATIVO', 'INATIVO') AS ativo, "

                    . "produtos.id "
                    . "FROM produtos "
                    . "LEFT JOIN categorias c ON (c.id = produtos.categoria) "
                    . "LEFT JOIN categorias c2 ON (c2.id = c.categoria) "
                    . "LEFT JOIN categorias c3 ON (c3.id = c2.categoria) "

                    . "LEFT JOIN atributos a ON (produtos.marca = a.id) "
                    . "LEFT JOIN atributos e ON (produtos.embalagem = e.id) "
                    . "LEFT JOIN atributos p ON (produtos.pais = p.id) "
                    . "LEFT JOIN atributos s ON (produtos.sabor = s.id) "
                    . "LEFT JOIN atributos t ON (produtos.tamanho = t.id) "
                    . "LEFT JOIN atributos tp ON (produtos.tipoAt = tp.id) "
                    . "LEFT JOIN atributos un ON (produtos.unidademedida = un.id) "
                    . "LEFT JOIN atributos pa ON (produtos.pais = pa.id) "

                    . "WHERE 1 = 1 ";

            $sql2 = "SELECT GROUP_CONCAT(produtos.id SEPARATOR ',') as ids "
                    . "FROM produtos "
                    . "LEFT JOIN categorias c ON (c.id = produtos.categoria) "
                    . "LEFT JOIN categorias c2 ON (c2.id = c.categoria) "
                    . "LEFT JOIN categorias c3 ON (c3.id = c2.categoria) "

                    . "LEFT JOIN atributos a ON (produtos.marca = a.id) "
                    . "LEFT JOIN atributos e ON (produtos.embalagem = e.id) "
                    . "LEFT JOIN atributos p ON (produtos.pais = p.id) "
                    . "LEFT JOIN atributos s ON (produtos.sabor = s.id) "
                    . "LEFT JOIN atributos t ON (produtos.tamanho = t.id) "
                    . "LEFT JOIN atributos tp ON (produtos.tipoAt = tp.id) "
                    . "LEFT JOIN atributos un ON (produtos.unidademedida = un.id) "
                    . "LEFT JOIN atributos pa ON (produtos.pais = pa.id) "

                    . "WHERE 1 = 1 ";


            //select da segunda coluna
            if (!empty($post['columns'][4]['search']['value'])) {

                if ($post['columns'][4]['search']['value'] == '\-') {
                    $sql .= " AND (produtos.categoria = 0 OR produtos.categoria is null) ";
                    $sql2 .= " AND (produtos.categoria = 0 OR produtos.categoria is null) ";
                } else {
                    $sql .= " AND c.id = '{$post['columns'][4]['search']['value']}' ";    
                    $sql2 .= " AND c.id = '{$post['columns'][4]['search']['value']}' ";    
                }
                
            }
            //select da terceira coluna
            if (!empty($post['columns'][5]['search']['value'])) {

                if ($post['columns'][5]['search']['value'] == '0') {
                    $sql .= " AND (c.categoria = 0 OR c.categoria is null) ";
                    $sql2 .= " AND (c.categoria = 0 OR c.categoria is null) ";
                } else {
                    $sql .= " AND c2.id = '{$post['columns'][5]['search']['value']}' ";    
                    $sql2 .= " AND c2.id = '{$post['columns'][5]['search']['value']}' ";    
                }
            }
            //select da quarta coluna
            if (!empty($post['columns'][6]['search']['value'])) {

                if ($post['columns'][6]['search']['value'] == '0') {
                    $sql .= " AND (c2.categoria = 0 OR c2.categoria is null) ";
                    $sql2 .= " AND (c2.categoria = 0 OR c2.categoria is null) ";
                } else {
                    $sql .= " AND c3.id = '{$post['columns'][6]['search']['value']}' ";    
                    $sql2 .= " AND c3.id = '{$post['columns'][6]['search']['value']}' ";    
                }
            }
            //marca
            if (!empty($post['columns'][7]['search']['value'])) {
                if (!empty($post['columns'][7]['search']['value'])) {
                    $sql .= " AND a.id = '{$post['columns'][7]['search']['value']}' ";    
                    $sql2 .= " AND a.id = '{$post['columns'][7]['search']['value']}' ";    
                }
            }
            //unidade e medida
            if (!empty($post['columns'][8]['search']['value'])) {
                if (!empty($post['columns'][8]['search']['value'])) {
                    $sql .= " AND produtos.unidademedida like '{$post['columns'][8]['search']['value']}' ";    
                    $sql2 .= " AND produtos.unidademedida like '{$post['columns'][8]['search']['value']}' ";    
                }
            }
            //peso volume
            if (!empty($post['columns'][9]['search']['value'])) {
                if (!empty($post['columns'][9]['search']['value'])) {
                    $sql .= " AND produtos.pesovolume like '%{$post['columns'][9]['search']['value']}%' ";    
                    $sql2 .= " AND produtos.pesovolume like '%{$post['columns'][9]['search']['value']}%' ";    
                }
            }
            //sabor
            if (!empty($post['columns'][10]['search']['value'])) {
                if (!empty($post['columns'][10]['search']['value'])) {
                    $sql .= " AND s.id = '{$post['columns'][10]['search']['value']}' ";    
                    $sql2 .= " AND s.id = '{$post['columns'][10]['search']['value']}' ";    
                }
            }
            //tamanho
            if (!empty($post['columns'][11]['search']['value'])) {
                if (!empty($post['columns'][11]['search']['value'])) {
                    $sql .= " AND t.id = '{$post['columns'][11]['search']['value']}' ";    
                    $sql2 .= " AND t.id = '{$post['columns'][11]['search']['value']}' ";    
                }
            }
            //tipo
            if (!empty($post['columns'][12]['search']['value'])) {
                if (!empty($post['columns'][12]['search']['value'])) {
                    $sql .= " AND tp.id = '{$post['columns'][12]['search']['value']}' ";    
                    $sql2 .= " AND tp.id = '{$post['columns'][12]['search']['value']}' ";    
                }
            }
            //pais
            if (!empty($post['columns'][13]['search']['value'])) {
                if (!empty($post['columns'][13]['search']['value'])) {
                    $sql .= " AND pa.id = '{$post['columns'][13]['search']['value']}' ";    
                    $sql2 .= " AND pa.id = '{$post['columns'][13]['search']['value']}' ";    
                }
            }

            //select do tipo
            if (isset($post['columns'][14]['search']['value']) && $post['columns'][14]['search']['value'] != '') {

                if ($post['columns'][14]['search']['value'] == '1') {
                    $sql .= " AND produtos.tipo = 1 ";    
                    $sql2 .= " AND produtos.tipo = 1 ";    
                } else {
                    $sql .= " AND produtos.tipo = 0 ";    
                    $sql2 .= " AND produtos.tipo = 0 ";    
                }
            }

            //select do status
            if (isset($post['columns'][15]['search']['value']) && $post['columns'][15]['search']['value'] != '') {

                if ($post['columns'][15]['search']['value'] == '1') {
                    $sql .= " AND produtos.ativo = 1 ";    
                    $sql2 .= " AND produtos.ativo = 1 ";    
                } else {
                    $sql .= " AND produtos.ativo = 0 ";    
                    $sql2 .= " AND produtos.ativo = 0 ";    
                }
            }

            if (!empty($post['search']['value'])) {
                $search = "AND (";
                foreach ($columns as $column) {
                    $search .= $column . " LIKE '%{$post['search']['value']}%' OR ";
                }
                $search = substr_replace($search, "", -3);
                $search .= ')';

                $sql2 .= $search;
            }
            
            $listaIdsTodosProdutosQuery = $modelBW->getAdapter()->fetchAll($sql2);
        }

        

        $output = $modelBW->dinamicTable($sql, $columns, $post);

        if (!empty($listaIdsTodosProdutosQuery)) {
            $output['listaIds'] = $listaIdsTodosProdutosQuery[0]['ids'];    
        }
        
        echo json_encode($output);
    }

    public function cadastroMercadoAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objProdutoMercado = new Application_Model_DbTable_ProdutoMercado();
        $objProduto = new Application_Model_DbTable_Produto();

        if (!empty($post)) {
            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'idProduto' => array('Produto', $post['idProduto']),
                    'preco' => array('Preço', $post['preco']),
                    'ativo' => array('Ativo', $post['ativo'])
                )
            );

            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (empty($id)) {
                $id = 0;
            }

            $dadosUsuario = $objProdutoMercado->fetchRow("idProduto = {$post['idProduto']} AND idMercado = {$_SESSION['logado']['usuario']['idMercado']} AND id <> {$id}");

            if (!empty($dadosUsuario)) {
                $erros[] = 'Já existe esse mesmo produto vinculado.';
            }


            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {

                if (!is_numeric($post['estoque'])) {
                    $post['estoque'] = 0;
                }

                $post['preco'] = str_replace(".", "", $post['preco']);
                $post['preco'] = str_replace(",", ".", $post['preco']);

                $arrayUsuario = array(
                    'idMercado' => $_SESSION['logado']['usuario']['idMercado'],
                    'idProduto' => $post['idProduto'],
                    'preco' => $post['preco'],
                    'estoque' => $post['estoque'],
                    'ativo' => $post['ativo'],
                    'codigoproduto' => $post['codigoproduto']
                );

                if (empty($id)) {
                    $objProdutoMercado->save($arrayUsuario);
                    $idInsert = $objProdutoMercado->getAdapter()->lastInsertId();
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    unset($_POST);
                } else {
                    $objProdutoMercado->save($arrayUsuario, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                }
            }
        }

        if (!empty($id)) {
            $this->view->id = $id;
            $dadosUsuario = $objProdutoMercado->fetchRow("id = {$id} AND idMercado = {$_SESSION['logado']['usuario']['idMercado']}");

            if (!empty($dadosUsuario)) {
                $this->view->dadosUsuario = $dadosUsuario;

            } else {
                $this->_redirect('/produto/listagem-mercado/');
            }
        }


        if (!empty($post['idProduto']) || !empty($dadosUsuario['idProduto'])) {

            if (!empty($dadosUsuario['idProduto'])) {
                $id = $dadosUsuario['idProduto'];
            } else {
                $id = $post['idProduto'];
            }
            
            $this->view->dadosProduto = $objProduto->fetchRow("id = {$id}");
        }
    }

    public function cadastroAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objProduto = new Application_Model_DbTable_Produto();
        $objTinify = new Application_Model_Tinify();
        $objCategoria = new Application_Model_DbTable_Categoria();
        $objAtributo = new Application_Model_DbTable_Atributo();

        $this->view->arrayListMarcas = $objAtributo->fetchAll("tipo = 0", "valor ASC");
        $this->view->arrayListSabores = $objAtributo->fetchAll("tipo = 1", "valor ASC");
        $this->view->arrayListEmbalagens = $objAtributo->fetchAll("tipo = 2", "valor ASC");
        $this->view->arrayListTamanhos = $objAtributo->fetchAll("tipo = 3", "valor ASC");
        $this->view->arrayListUnidades = $objAtributo->fetchAll("tipo = 4", "valor ASC");
        $this->view->arrayListPaises = $objAtributo->fetchAll("tipo = 5", "valor ASC");
        $this->view->arrayListTipos = $objAtributo->fetchAll("tipo = 6", "valor ASC");
        $this->view->arrayListCategoria = $objCategoria->fetchAll("categoria = 0 OR categoria is null", "nome ASC");

        if (!empty($post)) {

            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'tipo' => array('Tipo', $post['tipo']),
                    'ean' => array('EAN', $post['ean']),
                    'categoria' => array('Categoria', $post['categoria']),
                    'unidademedida' => array('Unidade', $post['unidademedida']),
                    'pesovolume' => array('Peso ou Volume', $post['pesovolume']),
                    'ativo' => array('Ativo', $post['ativo'])
                )
            );



            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (!empty($_FILES['foto']['size'])) {
                if ($_FILES['foto']['type'] <> 'image/jpeg' && $_FILES['foto']['type'] <> 'image/jpg') {
                    $erros[] = 'Envie uma imagem com a extensão .jpg';
                } else {
                    if ($_FILES['foto']['size'] > 2097152) {
                        $erros[] = 'Envie uma imagem até 2Mb.';
                    }
                }
            }

            if (empty($id)) {
                $idVerify = 0;
            } else {
                $idVerify = $id;
            }

            $dadosUsuario = $objProduto->fetchRow("ean = '{$post['ean']}' AND id <> {$idVerify}");

            if (!empty($dadosUsuario)) {
                $erros[] = 'Já existe um produto com esse código EAN.';
            }

            $idCategoria = 0;

            if (!empty($post['categoria']) && $post['categoria'] != '0') {
                $idCategoria = $post['categoria'];
            }

            if (!empty($post['categoria2']) && $post['categoria2'] != '0') {
                $idCategoria = $post['categoria2'];
            }

            if (!empty($post['categoria3']) && $post['categoria3'] != '0') {
                $idCategoria = $post['categoria3'];
            }

            if ($post['ativo'] == 1 && $idCategoria == 0) {
                $erros[] = 'Para ativar o produto atribuir uma categoria.';
            }

            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {

                $codigofiltro = $validacao->tirarAcentos($post['nome']);

                $arrayUsuario = array(
                    'tipo' => $post['tipo'],
                    'ean' => $post['ean'],
                    'nome' => $post['nome'],
                    'categoria' => $idCategoria,
                    'marca' => $post['marca'],
                    'unidademedida' => $post['unidademedida'],
                    'pesovolume' => $post['pesovolume'],
                    'embalagem' => $post['embalagem'],
                    'pais' => $post['pais'],
                    'tipoAt' => $post['tipo-atributo'],
                    'sabor' => $post['sabor'],
                    'tamanho' => $post['tamanho'],
                    'ativo' => $post['ativo'],
                    'descricao' => $post['descricao'],
                    'link' => $codigofiltro
                );


                if (empty($id)) {
                    $objProduto->save($arrayUsuario);
                    $idInsert = $objProduto->getAdapter()->lastInsertId();
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    unset($_POST);
                } else {

                    //verifica se estou alterando a foto
                    if (!empty($_FILES['foto']['size'])) {
                        $dadosUsuario = $objProduto->fetchRow("id = {$id}");

                        if (!empty($dadosUsuario['foto'])) {
                            $path = ROOT_PATH . '/public/uploads/produtos/';

                            if (file_exists($path . strtolower($dadosUsuario['foto']))) {
                                unlink($path . strtolower($dadosUsuario['foto']));    
                            }

                            if (file_exists($path . 'thumbs/' . strtolower($dadosUsuario['foto']))) {
                                unlink($path . 'thumbs/' . strtolower($dadosUsuario['foto']));
                            }
                        }
                    }

                    $objProduto->save($arrayUsuario, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                }

                if (!empty($_FILES['foto']['size'])) {
                    $dadosUsuario = $objProduto->fetchRow("id = {$id}");

                    $arquivo = $_FILES['foto'];
                    $path = ROOT_PATH . '/public/uploads/produtos/';
                    $Original = new SW_ImageVerify();
                    $Original->createDir($path);
                    $Original->nameGenerator($arquivo['type']);
                    $ImagemOriginal = SW_WideImage::load($arquivo['tmp_name']);
                    $nomeArquivo = $dadosUsuario['ean'];

                    if ($arquivo['type'] == 'image/jpeg' || $arquivo['type'] == 'image/jpg') {
                        
                        //gera nomes
                        $fotoweb = $nomeArquivo . '.webp';
                        $nomeArquivo = $nomeArquivo . '.jpg';

                        //salva arquivo original
                        $ImagemOriginal->saveToFile($path . $nomeArquivo);

                        // coloca fundo branco de 1200x1200 conforme Hans solicitou
                        $img = SW_WideImage::load($path . 'branco.jpg');
                        $thumb = SW_WideImage::load($path . $nomeArquivo);
                        
                        // Salva a imagem do produto no centro
                        $new = $img->merge($thumb, 'center', 'center', 100)->saveToFile($path . $nomeArquivo);

                        // gera a miniatura pela imagem grande
                        $thumb = SW_WideImage::load($path . $nomeArquivo)->resize(175, 175, 'outside', 'down')->saveToFile($path . 'thumbs/' . $nomeArquivo);

                       if (function_exists('imagewebp')) {
                            // converte em webp
                            imagewebp(imagecreatefromjpeg($path . $nomeArquivo), $path . $fotoweb, 100);
                            
                            //webp da thumb
                            imagewebp(imagecreatefromjpeg($path . 'thumbs/' . $nomeArquivo), $path . 'thumbs/' . $fotoweb, 100);
                            
                        }

                        $objTinify->compressFile($path . $nomeArquivo);
                        $objTinify->compressFile($path . 'thumbs/' . $nomeArquivo);


                    }
                    $idProduto = (!empty($idInsert) ? $idInsert : $id);
                    $arrayDataProdutoArquivo = array('foto' => $nomeArquivo, 'webp' => $fotoweb);
                    $objProduto->save($arrayDataProdutoArquivo, "id = {$idProduto}");
                }

            }

        }


        if (!empty($id)) {
            $this->view->id = $id;
            $dadosUsuario = $objProduto->fetchRow("id = {$id}");
            if (!empty($dadosUsuario)) {

                if (!empty($dadosUsuario['categoria'])) {
                    $arrayListCategoria1 = $objCategoria->fetchRow("id = {$dadosUsuario['categoria']}");    
                }

                if (!empty($arrayListCategoria1['categoria'])) {
                    $arrayListCategoria2 = $objCategoria->fetchRow("id = {$arrayListCategoria1['categoria']}");
                }

                if (!empty($arrayListCategoria2['categoria'])) {
                    $arrayListCategoria3 = $objCategoria->fetchRow("id = {$arrayListCategoria2['categoria']}");    
                }

                if (!empty($arrayListCategoria3)) {
                    $dadosUsuario['categoria'] = $arrayListCategoria3['id'];
                    $dadosUsuario['categoria2'] = $arrayListCategoria2['id'];
                    $dadosUsuario['categoria3'] = $arrayListCategoria1['id'];
                } else {

                    if (!empty($arrayListCategoria2)) {
                        $dadosUsuario['categoria'] = $arrayListCategoria2['id'];
                        $dadosUsuario['categoria2'] = $arrayListCategoria1['id'];
                        $dadosUsuario['categoria3'] = 0;
                    } else if (!empty($arrayListCategoria1)) {
                        $dadosUsuario['categoria'] = $arrayListCategoria1['id'];
                        $dadosUsuario['categoria2'] = 0;
                        $dadosUsuario['categoria3'] = 0;
                    } else {
                        $dadosUsuario['categoria'] = 0;
                        $dadosUsuario['categoria2'] = 0;
                        $dadosUsuario['categoria3'] = 0;
                    }
                }

                $this->view->dadosUsuario = $dadosUsuario;
            } else {
                $this->_redirect('/produto/');
            }
        }


        if ((!empty($this->view->dadosUsuario['categoria']) && $this->view->dadosUsuario['categoria']) || (!empty($_POST['categoria']) && $_POST['categoria'])) {

            if (!empty($this->view->dadosUsuario['categoria'])) {
                $idFiltro = $this->view->dadosUsuario['categoria'];
            } else {
                $idFiltro = $_POST['categoria'];
            }

            $this->view->arrayListCategoria2 = $objCategoria->fetchAll("categoria = {$idFiltro}");
        }

        if ((!empty($this->view->dadosUsuario['categoria2']) && $this->view->dadosUsuario['categoria2']) || (!empty($_POST['categoria2']) && $_POST['categoria2'])) {

            if (!empty($this->view->dadosUsuario['categoria2'])) {
                $idFiltro = $this->view->dadosUsuario['categoria2'];
            } else {
                $idFiltro = $_POST['categoria2'];
            }

            $this->view->arrayListCategoria3 = $objCategoria->fetchAll("categoria = {$idFiltro}");
        }
    }

    public function importacaoMercadoAction() {
        $post = $this->getRequest()->getPost();
        $objProdutoMercado = new Application_Model_DbTable_ProdutoMercado();

        $this->view->passo = 1;

        if (!empty($post)) {
            if ($post['passo'] == 1) {

                if (empty($_FILES['arquivoCSV']['name'])) {
                    $erros[] = 'Selecione o arquivo para importar.';
                }

                
                if (!empty($_FILES['arquivoCSV']['name']) && $_FILES['arquivoCSV']['type'] <> 'application/vnd.ms-excel' && $_FILES['arquivoCSV']['type'] <> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    $erros[] = 'Envie um arquivo no formato XLS ou XLSX.';
                }

                if (!empty($erros)) {
                    $this->view->importacao = array('erros' => $erros, 'sucesso' => false);
                } else {

                    $objProduto = new Application_Model_DbTable_Produto();

                    $codigos = array();

                    if (isset($post['arquivo'])) {

                        if (!empty($_FILES['arquivoCSV']['name'])) {

                            $path = ROOT_PATH . '/public/uploads/arquivos/';
                            $nomeArquivo = md5(uniqid(time())) . ".xls";

                            move_uploaded_file($_FILES["arquivoCSV"]["tmp_name"], $path . $nomeArquivo);

                            $objExcel = new Application_Model_Excel();
                            $dados = $objExcel->load($path . $nomeArquivo);
                            $dados = $dados->getActiveSheet();

                            // percorre o excel para alimentar os eans para busca
                            foreach ($dados->getRowIterator() as $row) {
                                $cellIterator = $row->getCellIterator();
                                $cellIterator->setIterateOnlyExistingCells(false);
                                
                                $codigoAtual = array();

                                if (empty($cellIterator)) {
                                    break;
                                }

                                $contador2 = 0;
                                foreach ($cellIterator as $cell) {

                                    if (empty($cell->getValue())) {
                                        break;
                                    }

                                    $codigoAtual[$contador2] = $cell->getValue();
                                    $contador2++;
                                }

                                if (empty($codigoAtual)){
                                    break;
                                }

                                $codigos[] = $codigoAtual;
                            }

                            //remove o arquivo temporario excel
                            unlink($path . $nomeArquivo);
                        }
                    }



                    $retDados = array();

                    foreach ($codigos as $codigoAtual) { 

                        $codigoAtualOriginal = $codigoAtual;

                        if (is_array($codigoAtual)) {
                            $codigoAtual = $codigoAtual[0];
                        }

//                        $dadosUsuario = $objProduto->fetchRow("ean = '{$codigoAtual}'");

//                        if (empty($dadosUsuario)) {
//                            $dadosUsuario = $this->cosmosProdutosAction($codigoAtual);
//
//                            // procura pelo codigo ean
//                            if (!empty($dadosUsuario) && empty($dadosUsuario['message'])) {
//                                $this->saveProdutosImportacao($dadosUsuario);    
//
//                            // se nao achar, pode ser um codigo interno do produto do mercado
//                            } else {
//
//                                $qProdutoImportado = $objProdutoMercado->fetchRow("codigoproduto = '{$codigoAtual}' AND idMercado = {$_SESSION['logado']['usuario']['idMercado']}");
//
//                                if (!empty($qProdutoImportado)) {
//
//                                    $codigoInternoAchado = $codigoAtual;
//
//                                    $dadosUsuario = $objProduto->fetchRow("id = '{$qProdutoImportado['idProduto']}'");
//                                    $codigoAtual = $dadosUsuario['ean'];
//
//                                }
//
//                            }
//                            
//                        }

                        $dadosUsuario = $objProduto->fetchRow("ean = '{$codigoAtual}'");

                        if (!empty($dadosUsuario)) {

                             if (is_array($codigoAtualOriginal)) {
                                $key = array_search($codigoAtual, array_column($codigos, 0));    
                                $dadosUsuario['estoqueAtual'] = $codigos[$key][1];
                                $dadosUsuario['valorAtual'] = number_format($codigos[$key][2], 2, ',', '.');
                                if (!empty($codigos[$key][3])) {
                                    $dadosUsuario['codigoInterno'] = $codigos[$key][3];    
                                } else {
                                    $dadosUsuario['codigoInterno'] = '';
                                }                                
                            } else {
                                $dadosUsuario['estoqueAtual'] = 0;
                                $dadosUsuario['codigoInterno'] = "";
                                $dadosUsuario['valorAtual'] = "0,00";
                            }

                            if (isset($codigoInternoAchado) && !empty($codigoInternoAchado)) {
                                $dadosUsuario['codigoInterno'] = $codigoInternoAchado;
                            }

                            $retDados[] = $dadosUsuario;
                        }
                    }

                    if (empty($retDados)) {

                        $_SESSION['logado']['usuario']['msgImportacao'] = false;
                        $this->_redirect('/produto/importacao-mercado/');

                    } else {
                        $this->view->passo = 2;
                        $this->view->produtos = $retDados;

                        $_SESSION['logado']['usuario']['produtosMercado'] = $retDados;
                        $this->_redirect('/produto/importacao-mercado/');    
                    }

                    

                }
            } else if ($post['passo'] == 2) {

                $objProdutoMercado = new Application_Model_DbTable_ProdutoMercado();
                $objProduto = new Application_Model_DbTable_Produto();

                foreach ($post['codigosImportacao'] as $umProduto) {

                    $dadosAtuais = $objProdutoMercado->fetchRow("idProduto = {$umProduto} AND idMercado = {$_SESSION['logado']['usuario']['idMercado']}");    

//                    $post['valor_'.$umProduto] = str_replace(".", "", $post['valor_'.$umProduto]);
                    $post['valor_'.$umProduto] = str_replace(",", ".", $post['valor_'.$umProduto]);

                    if (!is_numeric($post['estoque_'.$umProduto])) {
                        $post['estoque_'.$umProduto] = 0;
                    }

                    $arrayUsuario = array(
                        'idMercado' => $_SESSION['logado']['usuario']['idMercado'],
                        'idProduto' => $umProduto,
                        'preco' => $post['valor_'.$umProduto],
                        'ativo' => $post['ativo_'.$umProduto],
                        'estoque' => $post['estoque_'.$umProduto],
                        'codigoproduto' => $post['codigoproduto_'.$umProduto]
                    );

                    //salva novo registro
                    if (empty($dadosAtuais)) {
                        $objProdutoMercado->save($arrayUsuario);
                    } else {
                        //atualiza valor e status pois já tem o produto vinculado ao mercado
                        //atualiza se preço é maior
                        error_log("PREÇO ANTIGO ".$dadosAtuais['preco']." PREÇO ATUALIZADO ".$arrayUsuario['preco']);
                        if($dadosAtuais['preco'] < $arrayUsuario['preco']){
                            error_log("ATUALIZOU");
                            $objProdutoMercado->save($arrayUsuario, "id = {$dadosAtuais['id']}");
                        }
                        

                    }

                    $this->view->importacao = array('erros' => false, 'sucesso' => true);
                    unset($_POST);

                    
                }



            }
        }

        if (!empty($_SESSION['logado']['usuario']['produtosMercado'])) {
            $this->view->passo = 2;
            $this->view->produtos = $_SESSION['logado']['usuario']['produtosMercado'];
            unset($_SESSION['logado']['usuario']['produtosMercado']);
        }

        
    }
    
    public function importacaoAction() {
        $post = $this->getRequest()->getPost();
        if (!empty($post)) {
                if (empty($_FILES['arquivoCSV']['name'])) {
                    $erros[] = 'Selecione o arquivo para importar.';
                }

                
                if (!empty($_FILES['arquivoCSV']['name']) && $_FILES['arquivoCSV']['type'] <> 'application/vnd.ms-excel' && $_FILES['arquivoCSV']['type'] <> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    $erros[] = 'Envie um arquivo no formato XLS ou XLSX.';
                }

                if (!empty($erros)) {
                    $this->view->importacao = array('erros' => $erros, 'sucesso' => false);
                } else {

                    $objProduto = new Application_Model_DbTable_Produto();

                    $codigos = array();

                    if (isset($post['arquivo'])) {

                        if (!empty($_FILES['arquivoCSV']['name'])) {

                            $path = ROOT_PATH . '/public/uploads/arquivos/';
                            $nomeArquivo = md5(uniqid(time())) . ".xls";

                            move_uploaded_file($_FILES["arquivoCSV"]["tmp_name"], $path . $nomeArquivo);

                            $objExcel = new Application_Model_Excel();
                            $dados = $objExcel->load($path . $nomeArquivo);
                            $dados = $dados->getActiveSheet();

                            // percorre o excel para alimentar os eans para busca
                            foreach ($dados->getRowIterator() as $row) {
                                $cellIterator = $row->getCellIterator();
                                $cellIterator->setIterateOnlyExistingCells(false);
                                
                                $codigoAtual = array();

                                if (empty($cellIterator)) {
                                    break;
                                }

                                $contador2 = 0;
                                foreach ($cellIterator as $cell) {

                                    if (empty($cell->getValue())) {
                                        break;
                                    }
                                    
                                    $codigoAtual[$contador2] = $cell->getValue();
                                    $contador2++;
                                }

                                if (empty($codigoAtual)){
                                    break;
                                }
                                
                                $this->saveProdutosEanImportados($codigoAtual[0], 1, null);
//                                $codigos[] = $codigoAtual;
                            }

                            //remove o arquivo temporario excel
                            unlink($path . $nomeArquivo);
                        }
                    }
                }
        }        
    }
    
    public function importarProdutosEan() {
        $objProdutoEan = new Application_Model_DbTable_ProdutosEanImportados();
        $objProduto = new Application_Model_DbTable_Produto();
        
        $produtosImportar = $objProdutoEan->fetchRow("status = 1");
        
        foreach ($produtosImportar as $umProdutoImportar) {
            $novoStatus = 1;
            $novoNome = null;
            $codigoAtual  = $umProdutoImportar['ean'];
            $dadosProduto = $objProduto->fetchRow("ean = '{$codigoAtual}'");
            if (empty($dadosProduto)) {
                $dadosCosmos = $this->cosmosProdutosAction($codigoAtual);
                if(empty($dadosCosmos['gtin'])){
                    $novoStatus = 3;
                }else{
                    $this->saveProdutosImportacao($dadosCosmos);
                    $novoStatus = 2;
//                    $novoNome = 
                }
            }else{
                $novoStatus = 4;
            }
            
            $this->saveProdutosEanImportados($codigoAtual, $novoStatus, null);
        }
    }
    
    public function listagemMercadoAction() {

    }

//    public function importacaoAction() {
//        $post = $this->getRequest()->getPost();
//
//        if (!empty($post)) {
//
//            if (empty($_FILES['arquivoCSV']['name']) && empty($post['descricao']) ) {
//                $erros[] = 'Selecione o arquivo ou digite os códigos para importar.';
//            }
//
//
//            if (!empty($_FILES['arquivoCSV']['name']) && $_FILES['arquivoCSV']['type'] <> 'application/vnd.ms-excel' && $_FILES['arquivoCSV']['type'] <> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
//                $erros[] = 'Envie um arquivo no formato XLS ou XLSX.';
//            }
//
//
//            if (!empty($erros)) {
//                $this->view->importacao = array('erros' => $erros, 'sucesso' => false);
//            } else {
//
//                $objProduto = new Application_Model_DbTable_Produto();
//                $objCategoria = new Application_Model_DbTable_Categoria();
//                $objAtributo = new Application_Model_DbTable_Atributo();
//
//                $codigos = array();
//
//                if (isset($post['salvar'])) {
//                    if (!empty($post['descricao'])) {
//                        // lista de eans para busca
//                        $codigos = explode(";", $post['descricao']);
//                    }
//                }
//
//
//
//                if (isset($post['arquivo'])) {
//                    if (!empty($_FILES['arquivoCSV']['name'])) {
//                        $path = ROOT_PATH . '/public/uploads/arquivos/';
//                        $nomeArquivo = md5(uniqid(time())) . ".xls";
//
//                        move_uploaded_file($_FILES["arquivoCSV"]["tmp_name"], $path . $nomeArquivo);
//
//                        $objExcel = new Application_Model_Excel();
//                        $dados = $objExcel->load($path . $nomeArquivo);
//                        $dados = $dados->getActiveSheet();
//
//                        // percorre o excel para alimentar os eans para busca
//                        foreach ($dados->getRowIterator() as $row) {
//                            $cellIterator = $row->getCellIterator();
//                            $cellIterator->setIterateOnlyExistingCells(false);
//
//                            if (empty($cellIterator)) {
//                                break;
//                            }
//                           
//                            foreach ($cellIterator as $cell) {
//
//                                if (empty($cell->getValue())) {
//                                    break;
//                                }
//
//                                $codigos[] = $cell->getValue();
//                            }
//                        }
//
//                        //remove o arquivo temporario excel
//                        unlink($path . $nomeArquivo);
//                    }
//                }
//
//                foreach ($codigos as $codigoAtual) { 
//
//                    $dadosUsuario = $objProduto->fetchRow("ean = '{$codigoAtual}'");
////                    error_log($dadosUsuario." DADOS USUARIO");
//                    if (empty($dadosUsuario)) {
//                        $dadosCosmos = $this->cosmosProdutosAction($codigoAtual);
//                        if(empty($dadosCosmos['gtin'])){
//                            error_log($codigoAtual." N ENCONTRADO");
//                        }else{
//                            $this->saveProdutosImportacao($dadosCosmos);
//                        }
//                    }else{
////                        error_log($codigoAtual." Ja estava importada");
//                    }
//                }
//
//                $_SESSION['logado']['usuario']['msgImportacao'] = true;
//                
//                $this->_redirect('/produto/importacao/');
//
//            }
//
//        }
//
//        if (isset($_SESSION['logado']['usuario']['msgImportacao'])) {
//
//            if ($_SESSION['logado']['usuario']['msgImportacao'] == false) {
//
//                if (isset($_SESSION['logado']['usuario']['msgChaves'])) {
//                    unset($_SESSION['logado']['usuario']['msgChaves']);
//                    
//                    $erros[] = 'Não existe mais créditos para importar produtos usando as chaves atuais.';    
//                } else {
//                    $erros[] = 'Não foi possível carregar os dados da API de importação de produtos.';
//                }
//
//                
//                $this->view->importacao = array('erros' => $erros, 'sucesso' => false);
//            } else {
//                $this->view->importacao = array('erros' => false, 'sucesso' => true);    
//            }
//
//            unset($_SESSION['logado']['usuario']['msgImportacao']);
//            
//        }
//    }
    
    public function saveProdutosEanImportados($ean, $status, $nome) {
        $objProdutoEan = new Application_Model_DbTable_ProdutosEanImportados();
        
        $arrayProdutoEan = array(
                    'ean' => $ean,
                    'status' => $status,
                    'nome' => $nome                
                );
        try{
            $objProdutoEan->save($arrayProdutoEan);
        } catch (Exception $ex) {
            error_log("ERRO AO IMPORTAR EAN ".$ex);
        }
        
    }

    public function saveProdutosImportacao($retorno) {

        $objProduto = new Application_Model_DbTable_Produto();
        $objCategoria = new Application_Model_DbTable_Categoria();
        $objAtributo = new Application_Model_DbTable_Atributo();
        $validacao = new Application_Model_Validacao();
        $objTinify = new Application_Model_Tinify();
        $dadosUsuario = array();

        if (!empty($retorno['gtin'])) {
            //verifica se o ean já existe na base
            $dadosUsuario = $objProduto->fetchRow("ean = '{$retorno['gtin']}'");

            if (empty($dadosUsuario)) {

                $idMarca = 0;
                $idTamanho = 0;
                $foto = "";
                $path = ROOT_PATH . '/public/uploads/produtos/';

                if (!empty($retorno['brand'])) {
                    $brand = $retorno['brand']['name'];
                    $brand = str_replace("'","", $brand);    
                } else {
                    $brand = "";
                }

                $tamaho = $retorno['height'];

                //adiciona a marca como atributo
                if (!empty($brand)) {
                    $marcaExiste = $objAtributo->fetchRow("valor = '{$brand}' and tipo = 0");

                    if (empty($marcaExiste)) {
                        $arrayUsuario = array(
                            'tipo' => 0,
                            'valor' => $brand
                        );
                        $objAtributo->save($arrayUsuario);
                        $idMarca = $objAtributo->getAdapter()->lastInsertId();
                    } else {
                        $idMarca = $marcaExiste['id'];
                    }
                }

                //adiciona o tamanho
                if (!empty($tamanho)) {
                    $tamanhoExiste = $objAtributo->fetchRow("valor = '{$tamanho}' and tipo = 3");

                    if (empty($tamanhoExiste)) {
                        $arrayUsuario = array(
                            'tipo' => 3,
                            'valor' => $tamanho
                        );
                        $objAtributo->save($arrayUsuario);
                        $idTamanho = $objAtributo->getAdapter()->lastInsertId();
                    } else {
                        $idTamanho = $tamanhoExiste['id'];
                    }
                }

                //pega a imagem
                if (!empty($retorno['thumbnail'])) {

                    $foto = "";

                    //verifica se ja nao existe a foto fisicamente
                    if (!file_exists($path . $retorno['gtin'])) {

                        $foto = $retorno['gtin'] . ".jpg";
                        $fotoweb = $retorno['gtin'] . ".webp";
                        $ch = curl_init($retorno['thumbnail']);
                        $fp = fopen($path . $foto, 'wb');
                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_exec($ch);
                        curl_close($ch);
                        fclose($fp);

                        if (file_exists($path . $foto)) {
                            // coloca fundo branco de 1200x1200 conforme Hans solicitou
                            $img = SW_WideImage::load($path . 'branco.jpg');
                            $thumb = SW_WideImage::load($path . $foto);
                            
                            // Salva a imagem do produto no centro
                            $new = $img->merge($thumb, 'center', 'center', 100)->saveToFile($path . $foto);

                            // gera a miniatura pela imagem grande
                            $thumb = SW_WideImage::load($path . $foto)->resize(175, 175, 'outside', 'down')->saveToFile($path . 'thumbs/' . $foto);

                            if (function_exists('imagewebp')) {
                                // converte em webp
                                imagewebp(imagecreatefromjpeg($path . $foto), $path . $fotoweb, 100);
                                //webp da thumb
                                imagewebp(imagecreatefromjpeg($path . 'thumbs/' . $foto), $path . 'thumbs/' . $fotoweb, 100);
                            } 

                            $objTinify->compressFile($path . $foto);
                            $objTinify->compressFile($path . 'thumbs/' . $foto);
                            
                        }

                    }
                }

                $nome = str_replace("'","", $retorno['description']);

                $codigofiltro = $validacao->tirarAcentos($nome);

                //adiciona o produto
                $arrayUsuario = array(
                    'tipo' => 0,
                    'categoria' => 0,
                    'foto' => $foto,
                    'webp' => $fotoweb,
                    'ean' => $retorno['gtin'],
                    'nome' => $nome,
                    'marca' => $idMarca,
                    'unidademedida' => 0,
                    'pesovolume' => $retorno['net_weight'],
                    'embalagem' => 0,
                    'pais' => 0,
                    'sabor' => 0,
                    'tamanho' => $idTamanho,
                    'ativo' => 0,
                    'link' => $codigofiltro,
                    'descricao' => $retorno['ncm']['full_description']
                );

                $objProduto->save($arrayUsuario);   

            }

        }
    }

}

