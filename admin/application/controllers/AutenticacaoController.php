<?php

class Default_AutenticacaoController extends PainelBW_Painel {

    public function init() {
        parent::init();
        $this->_helper->layout->disableLayout();
    }

    public function recuperarSenhaAction() {
        $post = $this->getRequest()->getPost();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        
        if (!empty($post['email'])) {
            $objMercado = new Application_Model_DbTable_Mercado();
            $objUsuario = new Application_Model_DbTable_Usuario();

            $senhaNova = time();

            $arrayUsuario['senha'] = hash('sha512', $senhaNova);

            $dadosMercado = $objMercado->fetchRow("email = '{$post['email']}'");

            if ($dadosMercado) {
                $objMercado->save($arrayUsuario, "email = '{$post['email']}'");
            }

            $dadosUsuario = $objUsuario->fetchRow("email = '{$post['email']}'");

            if ($dadosUsuario) {
                $objUsuario->save($arrayUsuario,"email = '{$post['email']}'");
            }

            //$tr = new Zend_Mail_Transport_Smtp('localhost');
            $front = Zend_Controller_Front::getInstance()->getBaseUrl();
            $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;
            $mail = new Zend_Mail('utf-8');
            $mail->setSubject("Recuperar Senha");
             $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");
            $html = array();
            $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
            $html[] = '<tr>';
            $html[] = '<td width="600" align="center">';
            $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
            $html[] = '<tr>';
            $html[] = '<td height="120" bgcolor="#022e6d" align="center" valign="center">';
            $html[] = '<p><img src="' . $baseUrl . '/images/logo.png" /></p>';    
            $html[] = '</td>';
            $html[] = '</tr>';

            $html[] = '<tr>';
            $html[] = '<td bgcolor="#FFFFFF" valign="center">';
            $html[] = '<p><font face="verdana" size="3" color="#000000">';
            $html[] = 'Olá, <br><br> Sua senha nova no Mercado Fique Em Casa é: '.$senhaNova.'.<br />';
            $html[] = '</p>';
            $html[] = '</td>';
            $html[] = '</tr>';

            $html[] = '<tr>';
            $html[] = '<td height="30" bgcolor="#022e6d" align="center" valign="center">';
            $html[] = '<p>&nbsp;</p>';
            $html[] = '</td>';
            $html[] = '</tr>';
            $html[] = '</table>';
            $html[] = '</td>';
            $html[] = '</tr>';
            $html[] = '</table>';

            $mail->setBodyHtml(join("\n", $html));
            $mail->addTo($post['email'], $post['email']);
            $objConfigMail = new Application_Model_ConfigMail();
            $transport = $objConfigMail->get_configuracao_mail();
            $mail->send($transport);
    
        }
    }

    public function indexAction() {
        $post = $this->getRequest()->getPost();
        $ip = $_SERVER['REMOTE_ADDR'];
        $front = Zend_Controller_Front::getInstance()->getBaseUrl();
        $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;

        if (!empty($post)) {

            if (!empty($post['usuario'])) {

                $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

                //valida primeiro base de usuarios
                $authAdapter->setTableName('usuarios')
                        ->setIdentityColumn('email')
                        ->setCredentialColumn('senha');
                
                if (empty($post['automate'])) {
                    $authAdapter->setIdentity($post['usuario'])->setCredential(hash('sha512', $post['senha']));
                } else {
                    $authAdapter->setIdentity($post['usuario'])->setCredential($post['senha']);    
                }

                $this->view->erro = 'Usuário e/ou senha incorretos.';
                
                $auth = Zend_Auth::getInstance();
                $autenticado = $auth->authenticate($authAdapter)->isValid();
                

                if (!empty($autenticado)) {
                    $objUsuarioAutenticado = $authAdapter->getResultRowObject(null, 'senha');
                    if (!empty($objUsuarioAutenticado->ativo)) {
                        $storage = $auth->getStorage();
                        $storage->write($storage);

                        $sessaoUser = new Zend_Session_Namespace('logado');
                        $sessaoUser->usuario = array(
                            'idUsuario' => $objUsuarioAutenticado->id,
                            'idNivel' => $objUsuarioAutenticado->idNivel,
                            'idMercado' => $objUsuarioAutenticado->idMercado,
                            'nomeUsuario' => $objUsuarioAutenticado->nome
                        );

                        $objSessionNamespace = new Zend_Session_Namespace('autenticado');
                        $objSessionNamespace->setExpirationSeconds(86400);

                        $_SESSION['Default']['usuario']['admin'] = true;

                        if (isset($post['remember'])) {
                            Zend_Session::rememberMe(60*60*24*7);
                        }

                        $this->_redirect('/');
                    } else {
                        $this->view->erro = 'Usuário inativo.';
                    }
                }

                //valida base do mercado
                $authAdapter->setTableName('mercados')
                        ->setIdentityColumn('email')
                        ->setCredentialColumn('senha');
                
                if (empty($post['automate'])) {
                    $authAdapter->setIdentity($post['usuario'])->setCredential(hash('sha512', $post['senha']));
                } else {
                    $authAdapter->setIdentity($post['usuario'])->setCredential($post['senha']);    
                }

                $auth = Zend_Auth::getInstance();
                $autenticado = $auth->authenticate($authAdapter)->isValid();

                

                if (!empty($autenticado)) {
                    $objUsuarioAutenticado = $authAdapter->getResultRowObject(null, 'senha');
                    
                    if (!empty($objUsuarioAutenticado->ativo)) {
                        $storage = $auth->getStorage();
                        $storage->write($storage);

                        $sessaoUser = new Zend_Session_Namespace('logado');
                        $sessaoUser->usuario = array(
                            'idUsuario' => $objUsuarioAutenticado->id,
                            'idNivel' => $objUsuarioAutenticado->idNivel,
                            'idMercado' => $objUsuarioAutenticado->id,
                            'nomeUsuario' => $objUsuarioAutenticado->nome
                        );

                        $objSessionNamespace = new Zend_Session_Namespace('autenticado');
                        $objSessionNamespace->setExpirationSeconds(86400);

                        $_SESSION['Default']['usuario']['admin'] = true;

                        if (isset($post['remember'])) {
                            Zend_Session::rememberMe(60*60*24*7);
                        }

                        $this->_redirect('/');
                    } else {
                        $this->view->erro = 'Usuário inativo.';
                    }
                }

                
            } else {
                $this->view->erro = 'Preencha o usuário e senha.';                
            }
        }
    }

    public function logoutAction() {

        if (Zend_Auth::getInstance()->hasIdentity()) {
            Zend_Auth::getInstance()->clearIdentity();
            Zend_Session::forgetMe();
        }

        $this->_redirect('/');
    }

}
