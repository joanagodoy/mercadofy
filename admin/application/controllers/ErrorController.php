<?php
class Default_ErrorController extends Zend_Controller_Action
{
    public function init() {
        parent::init();

    }
    
    public function errorAction()
    {    
        $errors = $this->_getParam('error_handler');

        if (!$errors) {
            $this->view->message = 'You have reached the error page';
            return;
        }

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Zend_Log::CRIT;
                $this->view->message = 'Application error';
                break;
        }

        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->log($this->view->message, $priority, $errors->exception);
            $log->log('Request Parameters', $priority, $request->getParams());
        }

        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

        $this->view->request   = $errors->request;
        
        error_log($this->view->message);
        error_log($errors->exception->getMessage());
        error_log($errors->exception->getTraceAsString());
        
        $mail = new Zend_Mail('utf-8');
        $mail->setSubject("Erro SCF");
         $mail->setFrom('mercadofiqueemcasa@gmail.com', "Admin Application");
        $html = array();
        $html[] = $this->view->message;
        $html[] = $errors->exception->getMessage();
        $html[] = $errors->exception->getTraceAsString();
        $mail->setBodyHtml(join("\n", $html));
        $mail->addTo('sistema.mercadofy@gmail.com', 'sistema.mercadofy@gmail.com');
       
        $objConfigMail = new Application_Model_ConfigMail();
        $transport = $objConfigMail->get_configuracao_mail();
//        $mail->send($transport);
        
//        $arrayMail = array(
//                'from' => "Joana",
//                'to' => 'sistema.mercadofy@gmail.com',
//                'subject' => "Erro SCF",
//                'bodyHtml' => join("\n", $html)
//            );
//
//        $objMail = new Application_Model_Mail();
//        $objMail->enviarEmail($arrayMail);
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }
}
    