<?php

class Default_OpcoesPagamentoController extends PainelBW_Painel
{

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }

    public function indexAction() {

    }

    public function listagemAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $action = $post['action'];
        $modelBW = new Application_Model_DbTable_ModelBW();

        $columns = array('nome', 'status');
        $sql = "SELECT "
                . "c.nome, "
                . "IF(c.status = 1, 'Ativo', 'Inativo') AS status, "
                . "c.id "
                . "FROM formaspagamentos c WHERE mercado = {$_SESSION['logado']['usuario']['idMercado']} ";
        $output = $modelBW->dinamicTable($sql, $columns, $post);
        echo json_encode($output);
    }

    public function cadastroAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objOpcoes = new Application_Model_DbTable_OpcoesPagamento();

        if (!empty($post)) {
            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'nome' => array('Nome', $post['nome'])
                )
            );

            
            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {
                $arrayUsuario = array(
                    'nome' => $post['nome'],
                    'status' => $post['ativo'],
                    'mercado' => $_SESSION['logado']['usuario']['idMercado']
                );

                if (empty($id)) {
                    $objOpcoes->save($arrayUsuario);
                    $idInsert = $objOpcoes->getAdapter()->lastInsertId();
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    unset($_POST);
                } else {

                    $objOpcoes->save($arrayUsuario, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                }
                
            }
        }

        if (!empty($id)) {
            $this->view->id = $id;
            $dadosUsuario = $objOpcoes->fetchRow("id = {$id} AND mercado = {$_SESSION['logado']['usuario']['idMercado']}");
            if (!empty($dadosUsuario)) {


                $this->view->dadosUsuario = $dadosUsuario;
            } else {
                $this->_redirect('/opcoespagamento/');
            }
        }
    }

}

