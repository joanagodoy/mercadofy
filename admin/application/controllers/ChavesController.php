<?php

class Default_ChavesController extends PainelBW_Painel {

    public $request;

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }

    

    public function listagemAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $modelBW = new Application_Model_DbTable_ModelBW();

        $columns = array('chaves.chave', 'chaves.limite');
        $sql = "SELECT "
                . "chaves.chave, "
                . "chaves.limite, "
                . "chaves.id "
                . "FROM chaves "
                . "WHERE 1 = 1 ";
        $output = $modelBW->dinamicTable($sql, $columns, $post);
        echo json_encode($output);
    }

    public function indexAction() {
        
    }

    public function cadastroAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objChave = new Application_Model_DbTable_Chaves();

      

        if (!empty($post)) {

            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'chave' => array('Chave', $post['chave']),
                    'limite' => array('Limite', $post['limite'])
                )
            );

            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (empty($id)) {
                $id = 0;
            }

            $dadosUsuario = $objChave->fetchRow("chave = '{$post['chave']}' AND id <> {$id}");

            if (!empty($dadosUsuario)) {
                $erros[] = 'Já existe uma chave cadastrada com esse código.';
            }

            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {
                $arrayUsuario = array(
                    'chave' => $post['chave'],
                    'limite' => $post['limite']
                );

                if (empty($id)) {
                    $objChave->save($arrayUsuario);
                    $idInsert = $objChave->getAdapter()->lastInsertId();
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    unset($_POST);
                } else {

                    $objChave->save($arrayUsuario, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                }

                unset($_POST);
            }
        }

        if (!empty($id)) {
            

            $this->view->id = $id;
            $dadosUsuario = $objChave->fetchRow("id = {$id}");    
            
            if (!empty($dadosUsuario)) {
                $this->view->dadosUsuario = $dadosUsuario;

            } else {
                $this->_redirect('/chaves/');
            }
        }

    }


}
