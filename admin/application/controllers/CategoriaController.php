<?php

class Default_CategoriaController extends PainelBW_Painel
{

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }

    public function categoriasAjaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();

        $objCategoria = new Application_Model_DbTable_Categoria();
        $retorno = array();

        if (!empty($post['parent']) && !empty($post['operation']) && $post['operation'] == 'create_node') {
            $arrayUsuario = array(
                'nome' => $post['title'],
                'codigofiltro' => str_replace(" ", "-", $post['title']),
                'categoria' => $post['parent']
            );
            $objCategoria->save($arrayUsuario);
        }

        if (!empty($post['id_change']) && !empty($post['operation']) && $post['operation'] == 'rename_node') {
            $arrayUsuario = array(
                'nome' => $post['title'],
                'codigofiltro' => str_replace(" ", "-", $post['title']),
            );
            $objCategoria->save($arrayUsuario, "id = {$post['id_change']}");
        }

        if (!empty($post['id_change']) && !empty($post['operation']) && $post['operation'] == 'remove_node') {
            $objCategoria->delete("id = {$post['id_change']}");
        }


        if (is_numeric($post['id'])) {
            $arrayList = $objCategoria->getAdapter()->fetchAll("SELECT * FROM categorias WHERE categoria = {$post['id']}  ORDER BY nome ASC");
        } else {
            $arrayList = $objCategoria->getAdapter()->fetchAll("SELECT * FROM categorias WHERE (categoria = 0 OR categoria is null) ORDER BY nome ASC");
        }

        foreach($arrayList as $item) {

            $children = false;

            $qVerifyChildrens = $objCategoria->fetchOne("COUNT(*)", "categoria = {$item['id']}");

            if ($qVerifyChildrens) {
                $children = true;                    
            }

            $retorno[] = array(
                'id' => $item['id'],
                'text' => $item['nome'],
                'categoria' => $item['categoria'],
                'children' => $children
            );
        }


        echo json_encode($retorno);

    }

    public function buscaCategoriasAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $post = $this->getRequest()->getPost();
        if (!empty($post)) {
            $valor = $post['valor'];

            $objCategoria = new Application_Model_DbTable_Categoria();
            $arrayList = $objCategoria->getAdapter()->fetchAll("SELECT * FROM categorias WHERE categoria = {$valor} ORDER BY nome ASC");

            $html[] = '<option value="0">Nenhuma</option>';
            foreach ($arrayList as $umaCidade) {
                $html[] = '<option value="' . $umaCidade['id'] . '" '.$selected.' >' . $umaCidade['nome'] . '</option>';
            }
            echo join("\n", $html);
        }
    }
    

    public function indexAction() {

    }

    public function listagemAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $action = $post['action'];
        $modelBW = new Application_Model_DbTable_ModelBW();

        $columns = array('c.nome', 'c2.nome', 'c3.nome');
        $sql = "SELECT "
                . "c.nome, "
                . "c2.nome as nome2, "
                . "c3.nome as nome3, "
                . "c.id "
                . "FROM categorias c "
                . "LEFT JOIN categorias c2 ON (c2.id = c.categoria) "
                . "LEFT JOIN categorias c3 ON (c3.id = c2.categoria) "
                . "WHERE 1 = 1 ";

        //select da segunda coluna
        if (!empty($post['columns'][1]['search']['value'])) {

            if ($post['columns'][1]['search']['value'] == '\-') {
                $sql .= " AND (c.categoria = 0 OR c.categoria is null) ";
            } else {
                $sql .= " AND c2.nome like '{$post['columns'][1]['search']['value']}' ";    
            }
            
        }
        //select da terceira coluna
        if (!empty($post['columns'][2]['search']['value'])) {

            if ($post['columns'][2]['search']['value'] == '\-') {
                $sql .= " AND (c2.categoria = 0 OR c2.categoria is null) ";
            } else {
                $sql .= " AND c3.nome like '{$post['columns'][2]['search']['value']}' ";    
            }
        }


        $output = $modelBW->dinamicTable($sql, $columns, $post);
        echo json_encode($output);
    }

    public function cadastroAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objCategoria = new Application_Model_DbTable_Categoria();
        $objProduto = new Application_Model_DbTable_Produto();

        $str = "";

        if (!empty($id)) {
            $str = " AND id <> {$id}";
        }

        $this->view->showIcone = true;

        $this->view->arrayListCategoria = $objCategoria->fetchAll("(categoria = 0 OR categoria is null) {$str}", "nome ASC");

        if (!empty($post)) {

            if (!empty($post['excluir'])) {

                $objCategoria->delete('id = '. $id);
                $this->_redirect('/categoria/');
            }


            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'nome' => array('Nome', $post['nome'])
                )
            );
            $arrayListExtPermitidas = array('image/jpeg', 'image/jpg');
            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (!empty($_FILES['icone']['size'])) {
                if ($_FILES['icone']['type'] <> 'image/jpeg' && $_FILES['icone']['type'] <> 'image/jpg') {
                    $erros[] = 'Envie uma thumb com a extensão .jpg';
                }
            }
            if (!empty($_FILES['imagem']['size'])) {
                if ($_FILES['imagem']['type'] <> 'image/jpeg' && $_FILES['imagem']['type'] <> 'image/jpg') {
                    $erros[] = 'Envie uma imagem com a extensão .jpg';
                }
            }

            if (empty($post['categoria']) || $post['categoria'] == 0) {
                $post['categoria'] = $post['categoria2'];
            }

            if (empty($id)) {
                $idVerify = 0;
            } else {
                $idVerify = $id;
            }

            $dadosUsuario = $objCategoria->fetchRow("nome = '{$post['nome']}' AND id <> {$idVerify} AND categoria = '{$post['categoria']}'");

            if (!empty($dadosUsuario)) {
                $erros[] = 'Já existe uma categoria com esse nome.';
            }

            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {

                $codigofiltro = $validacao->tirarAcentos($post['nome']);

                $arrayUsuario = array(
                    'nome' => $post['nome'],
                    'categoria' => $post['categoria'],
                    'descricao' => $post['descricao'],
                    'icone' => $post['icone'],
                    'meta' => $post['meta'],
                    'description' => $post['description'],
                    'codigofiltro' => $codigofiltro
                );

                if (empty($id)) {
                    $objCategoria->save($arrayUsuario);
                    $idInsert = $objCategoria->getAdapter()->lastInsertId();
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    unset($_POST);
                } else {

                    //verifica se estou alterando a foto
                    if (!empty($_FILES['icone']['size'])) {
                        $dadosUsuario = $objCategoria->fetchRow("id = {$id}");

                        if (!empty($dadosUsuario['thumb'])) {
                            $path = ROOT_PATH . '/public/uploads/categorias/';
                            unlink($path . 'thumbs/' . $dadosUsuario['thumb']);
                        }
                    }

                    if (!empty($_FILES['imagem']['size'])) {
                        $dadosUsuario = $objCategoria->fetchRow("id = {$id}");

                        if (!empty($dadosUsuario['foto'])) {
                            $path = ROOT_PATH . '/public/uploads/categorias/';
                            unlink($path . $dadosUsuario['foto']);
                        }
                    }

                    $objCategoria->save($arrayUsuario, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                }

                


                if (!empty($_FILES['imagem']['size'])) {
                    $nomeArquivo = md5(uniqid(time()));
                    $arquivo = $_FILES['imagem'];
                    $path = ROOT_PATH . '/public/uploads/categorias/thumbs/';
                    $Original = new SW_ImageVerify();
                    $Original->createDir($path);
                    $Original->nameGenerator($arquivo['type']);
                    $ImagemOriginal = SW_WideImage::load($arquivo['tmp_name'])->resize(110, 110, 'outside', 'down');
                    if ($arquivo['type'] == 'image/jpeg' || $arquivo['type'] == 'image/jpg') {
                        $nomeArquivo = $nomeArquivo . '.jpg';
                        $ImagemOriginal->saveToFile($path . $nomeArquivo);
                    }
                    $idProduto = (!empty($idInsert) ? $idInsert : $id);
                    $arrayDataProdutoArquivo = array('thumb' => $nomeArquivo);
                    $objCategoria->save($arrayDataProdutoArquivo, "id = {$idProduto}");
                }

                if (!empty($_FILES['imagem']['size'])) {
                    $nomeArquivo = md5(uniqid(time()));
                    $arquivo = $_FILES['imagem'];
                    $path = ROOT_PATH . '/public/uploads/categorias/';
                    $Original = new SW_ImageVerify();
                    $Original->createDir($path);
                    $Original->nameGenerator($arquivo['type']);
                    $ImagemOriginal = SW_WideImage::load($arquivo['tmp_name']);
                    if ($arquivo['type'] == 'image/jpeg' || $arquivo['type'] == 'image/jpg') {
                        $nomeArquivo = $nomeArquivo . '.jpg';
                        $ImagemOriginal->saveToFile($path . $nomeArquivo);
                    }
                    $idProduto = (!empty($idInsert) ? $idInsert : $id);
                    $arrayDataProdutoArquivo = array('foto' => $nomeArquivo);
                    $objCategoria->save($arrayDataProdutoArquivo, "id = {$idProduto}");
                }

                
            }
        }

        if (!empty($id)) {
            $this->view->id = $id;
            $dadosUsuario = $objCategoria->fetchRow("id = {$id}");
            if (!empty($dadosUsuario)) {

                if (!empty($dadosUsuario['categoria']) && $dadosUsuario['categoria'] != 0) {
                    
                    $arrayListCategoria2 = $objCategoria->fetchRow("id = {$dadosUsuario['categoria']}");

                    if (empty($arrayListCategoria2['categoria']) || $arrayListCategoria2['categoria'] == 0) {
                        $dadosUsuario['categoria'] = 0;    
                        $dadosUsuario['categoria2'] = $arrayListCategoria2['id'];    

                        $this->view->arrayListCategoria2 = $objCategoria->fetchAll("categoria = {$arrayListCategoria2['id']} AND id <> {$id}", "nome ASC");

                    } else {
                        $dadosUsuario['categoria2'] = $arrayListCategoria2['categoria'];    
                        $dadosUsuario['categoria'] = $arrayListCategoria2['id'];    
                        $this->view->arrayListCategoria2 = $objCategoria->fetchAll("categoria = {$arrayListCategoria2['categoria']} AND id <> {$id}", "nome ASC");
                    }

                }
                $dadosUsuario['icone'] = strtolower($dadosUsuario['icone']);

                if (!empty($dadosUsuario['categoria']) || !empty($dadosUsuario['categoria2'])) {
                    $this->view->showIcone = false;
                } else {
                    $this->view->showIcone = true;
                }

                //verifica se posso excluir
                $qCategorias = $objCategoria->fetchAll("categoria = {$id}");

                //verifica se posso excluir
                $qProdutos = $objProduto->fetchAll("categoria = {$id}");

                if (empty($qCategorias) && empty($qProdutos)) {
                    $this->view->isPossivelExcluir = true;
                } else {
                    $this->view->isPossivelExcluir = false;
                }

                $this->view->dadosUsuario = $dadosUsuario;

            } else {
                $this->_redirect('/categoria/');
            }
        }
    }

}

