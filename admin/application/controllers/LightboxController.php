<?php

class Default_LightboxController extends PainelBW_Painel {

    public $request;

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }

    public function listagemAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $modelBW = new Application_Model_DbTable_ModelBW();

        $columns = array('lightbox.nome', 'lightbox.foto', 'lightbox.tipo');
        $sql = "SELECT "
                . "lightbox.nome, "
                . "lightbox.foto, "
                . "IF(lightbox.tipo = 1, 'Desktop', 'Mobile') AS tipoTexto, "
                . "lightbox.id "
                . "FROM lightbox "
                . "WHERE 1 = 1 ";

        if ($_SESSION['logado']['usuario']['idMercado'] != 0) {
            $sql .= " AND mercado = {$_SESSION['logado']['usuario']['idMercado']} ";
        }  else {
            $sql .= " AND mercado is null ";
        }

        $output = $modelBW->dinamicTable($sql, $columns, $post);
        echo json_encode($output);
    }

    public function indexAction() {
        $post = $this->getRequest()->getPost();

        $objCookies = new Application_Model_DbTable_MercadoCookie();
        
        if (!empty($post)) {
            if (isset($post['alterarTempo'])) {

                if (!is_numeric($post['tempo'])) {
                    $erros[] = "As horas precisam ser informadas corretamente.";
                }

                if (!empty($erros)) {
                    $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
                } else {
                    if ($_SESSION['logado']['usuario']['idMercado'] != 0) {
                        $dados = $objCookies->fetchRow("mercado = {$_SESSION['logado']['usuario']['idMercado']}");    
                    } else {
                        $dados = $objCookies->fetchRow("mercado is null");    
                    }

                    $arrayUsuario = array(
                        'horas' => $post['tempo']
                    );

                    if ($_SESSION['logado']['usuario']['idMercado'] != 0) {
                        $arrayUsuario['mercado'] = $_SESSION['logado']['usuario']['idMercado'];
                    }

                    if (empty($dados)) {
                        $objCookies->save($arrayUsuario);
                    } else {
                        $objCookies->save($arrayUsuario, "id = {$dados['id']}");
                    }

                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);

                    unset($_POST);

                }
            }
        }

        if ($_SESSION['logado']['usuario']['idMercado'] != 0) {
            $dadosUsuario = $objCookies->fetchRow("mercado = {$_SESSION['logado']['usuario']['idMercado']}");
        } else {
            $dadosUsuario = $objCookies->fetchRow("mercado is null");    
        }

        $this->view->dadosUsuario = $dadosUsuario;
    }

    public function cadastroAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objLightbox = new Application_Model_DbTable_Lightbox();


        if (!empty($post)) {

            if (isset($post['excluir'])) {
                $objLightbox->delete("id = {$id}");
                $this->_redirect('/lightbox/');
            }

            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'nome' => array('Nome', $post['nome']),
                    'tipo' => array('Tipo', $post['tipo'])
                )
            );

            if (empty($id)) {
                if (empty($_FILES['foto']['type'])) {
                    $arrayListValidacao['NotEmpty']['foto'] = array('Imagem', $post['foto']);
                }
            }

            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (!empty($_FILES['foto']['size'])) {

                if ($_FILES['foto']['type'] <> 'image/jpeg' && $_FILES['foto']['type'] <> 'image/jpg' && $_FILES['foto']['type'] <> 'image/png') {
                    $erros[] = 'Envie uma imagem com a extensão .jpg ou .png';
                } else {
                    if ($_FILES['foto']['size'] > 2097152) {
                        $erros[] = 'Envie uma imagem até 2Mb.';
                    }
                }
            }

            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {

                $arrayUsuario = array(
                    'nome' => $post['nome'],
                    'tipo' => $post['tipo']
                );

                if ($_SESSION['logado']['usuario']['idMercado'] != 0) {
                    $arrayUsuario['mercado'] = $_SESSION['logado']['usuario']['idMercado'];
                }

                if (empty($id)) {
                    $objLightbox->save($arrayUsuario);
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    $idInsert = $objLightbox->getAdapter()->lastInsertId();
                } else {
                    $objLightbox->save($arrayUsuario, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                }

                if (!empty($_FILES['foto']['size'])) {
                    $arquivo = $_FILES['foto'];
                    $path = ROOT_PATH . '/public/uploads/boasvindas/';
                    $Original = new SW_ImageVerify();
                    $Original->createDir($path);
                    $Original->nameGenerator($arquivo['type']);
                    $ImagemOriginal = SW_WideImage::load($arquivo['tmp_name']);
                    $nomeArquivo = strtolower(md5(uniqid(time())));
                    
                    if ($arquivo['type'] == 'image/jpeg' || $arquivo['type'] == 'image/jpg') {
                        $nomeArquivo = $nomeArquivo . '.jpg';
                        $ImagemOriginal->saveToFile($path . $nomeArquivo);
                    } else if ($arquivo['type'] == 'image/png') {
                        $nomeArquivo = $nomeArquivo . '.png';
                        $ImagemOriginal->saveToFile($path . $nomeArquivo);
                    }

                    $idLightbox = (!empty($idInsert) ? $idInsert : $id);
                    $arrayDataProdutoArquivo = array('foto' => $nomeArquivo);
                    $objLightbox->save($arrayDataProdutoArquivo, "id = {$idLightbox}");
                }

                unset($_POST);

            }
        }

        if (!empty($id)) {
            $this->view->id = $id;
            
            if ($_SESSION['logado']['usuario']['idMercado'] != 0) {
                $dadosUsuario = $objLightbox->fetchRow("id = {$id} AND mercado = {$_SESSION['logado']['usuario']['idMercado']}");
            } else {
                $dadosUsuario = $objLightbox->fetchRow("id = {$id} AND mercado is null");    
            }
            
            
            if (!empty($dadosUsuario)) {
                $dadosUsuario = $dadosUsuario;
                $this->view->dadosUsuario = $dadosUsuario;
            } else {
                $this->_redirect('/lightbox/');
            }
        }
    }


}
