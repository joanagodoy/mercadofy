<?php

class Default_IndexController extends PainelBW_Painel
{

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }
    

    public function permissaoNegadaAction() {
        
    }

    public function indexAction() {
        $post = $this->getRequest()->getPost();
        $objMercado = new Application_Model_DbTable_Mercado();
        $objPedido = new Application_Model_DbTable_Pedido();

        if (empty($_SESSION['logado'])) {
            $this->_redirect('/autenticacao/logout');
        }

        if ($_SESSION['logado']['usuario']['idMercado'] != 0) {
            $str = " m.id = {$_SESSION['logado']['usuario']['idMercado']} ";    
        } else {
            $str = " 1 = 1 ";

            if (!empty($post['mercado'])) {
                $str = " m.id = {$post['mercado']} ";    
            }
            
        }

        if (!empty($post['devalor'])) {

            $ateValor = str_replace(".","",$post['devalor']);
            $ateValor = str_replace(",",".",$ateValor);

            $str .= " AND pedidos.valortotal >= {$ateValor} ";    
        }


        if (!empty($post['atevalor'])) {

            $ateValor = str_replace(".","",$post['atevalor']);
            $ateValor = str_replace(",",".",$ateValor);

            $str .= " AND pedidos.valortotal <= {$ateValor} ";    
        }

        if (!isset($post['from'])) {
            $dataInicial = date('Y-m-d');
            $_POST['from'] = date('d/m/Y');
        } else {
            $dataInicial = join('-', array_reverse(explode('/', $post['from'])));
            
        }

        $str .= " AND date(pedidos.data) >= '{$dataInicial}' ";

        if (!isset($post['to'])) {
            $dataFinal = date('Y-m-d');
            $_POST['to'] = date('d/m/Y');
        } else {
            $dataFinal = join('-', array_reverse(explode('/', $post['to'])));
            
        }

        $str .= " AND date(pedidos.data) <= '{$dataFinal}' ";

        if (!empty($post['mercado'])) {
            $str .= " AND m.id = {$post['mercado']} ";
        }

        $sql = "SELECT "
            . "pedidos.id, "
            . "date_format(pedidos.data,'%d/%m/%Y') as data, "
            . "pedidos.valortotal, "
            . "IF(pedidos.status = 1, 'Ativo', 'Inativo') AS status, "
            . "IF(pedidos.statuspagamento = 1, 'Ativo', 'Inativo') AS statuspagamento, "
            . "pedidos.horaentrega, "
            . "m.nome as nomeMercado, "
            . "pedidos.id "
            . "FROM pedidos "
            . "LEFT JOIN mercados m ON (m.id = pedidos.idMercado) "
            . "LEFT JOIN clientes c ON (c.id = pedidos.idCliente) "
            . "WHERE {$str} "
            . "ORDER BY pedidos.id desc ";

        $this->view->arrayPedidos = $objMercado->getAdapter()->fetchAll($sql);

        $this->view->arrayMercados = $objMercado->fetchAll("1 = 1", "nome ASC");

    }

}

