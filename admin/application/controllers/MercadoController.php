<?php

class Default_MercadoController extends PainelBW_Painel {

    public $request;

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('autenticacao');
        }
    }

    

    public function listagemAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $modelBW = new Application_Model_DbTable_ModelBW();

        if ($_SESSION['logado']['usuario']['idMercado'] != 0) {
            $str = " mercados.id = {$_SESSION['logado']['usuario']['idMercado']} ";    
        } else {
            $str = " 1 = 1 ";
        }

        $columns = array('mercados.nome', 'mercados.email', 'mercados.telefone', 'mercados.ativo');
        $sql = "SELECT "
                . "mercados.nome, "
                . "mercados.email, "
                . "mercados.telefone, "
                . "CASE  "
                . "WHEN mercados.ativo = 0 THEN 'Desativado' "
                . "WHEN mercados.ativo = 1 THEN 'Site' "
                . "WHEN mercados.ativo = 2 THEN 'Site + Mercado' "
                . "WHEN mercados.ativo = 3 THEN 'Mercado' "
                . "END AS statusUsuario, "
                . "mercados.id "
                . "FROM mercados "
                . "WHERE {$str} ";
                
        $output = $modelBW->dinamicTable($sql, $columns, $post);
        echo json_encode($output);
    }

    public function indexAction() {
        
    }

    public function cadastroAction() {
        $id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();

        $objMercado = new Application_Model_DbTable_Mercado();
        $objCidade = new Application_Model_DbTable_Cidade();
        $objEstado = new Application_Model_DbTable_Estado();
        $objFeriado = new Application_Model_DbTable_Feriado();
        $objRegrasRetirada = new Application_Model_DbTable_RegrasRetirada();
        $objRegrasEntrega = new Application_Model_DbTable_RegrasEntrega();

        $arrayListCidades = array();

        if (!empty($post)) {

            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'nome' => array('Nome', $post['nome']),
                    'razaosocial' => array('Razão Social', $post['razaosocial']),
                    'cnpj' => array('CNPJ', $post['cnpj']),
                    'email' => array('E-Mail', $post['email']),
                    'telefone' => array('Telefone', $post['telefone']),
                    'cep' => array('CEP', $post['cep']),
                    'rua' => array('Rua', $post['rua']),
                    'numero' => array('Número', $post['numero']),
                    'bairro' => array('Bairro', $post['bairro']),
                    'cidade' => array('Cidade', $post['idCidade']),
                    'estado' => array('Estado', $post['idEstado']),
                    'ativo' => array('Ativo', $post['ativo'])
                )
            );

            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();

            if (!empty($id)) {
                if (empty($post['senha'])) {
                    $arrayListValidacao['NotEmpty']['senha'] = array('Senha', $post['senha']);
                }
            }
            if (!empty($_FILES['foto']['size'])) {
                if ($_FILES['foto']['type'] <> 'image/jpeg' && $_FILES['foto']['type'] <> 'image/jpg') {
                    $erros[] = 'Envie uma imagem com a extensão .jpg';
                } else {
                    if ($_FILES['foto']['size'] > 2097152) {
                        $erros[] = 'Envie uma imagem até 2Mb.';
                    }
                }
            }

            if (!empty($_FILES['foto2']['size'])) {
                if ($_FILES['foto2']['type'] <> 'image/jpeg' && $_FILES['foto2']['type'] <> 'image/jpg') {
                    $erros[] = 'Envie uma imagem com a extensão .jpg';
                } else {
                    if ($_FILES['foto2']['size'] > 2097152) {
                        $erros[] = 'Envie uma imagem até 2Mb.';
                    }
                }
            }

            if (empty($id)) {
                $id = 0;
            }

            $dadosUsuario = $objMercado->fetchRow("email = '{$post['email']}' AND id <> {$id}");

            if (!empty($dadosUsuario)) {
                $erros[] = 'Já existe um mercado cadastrado com esse e-mail.';
            }

            if (!empty($erros)) {
                $this->view->cadastro = array('erros' => $erros, 'sucesso' => false);
            } else {
                $arrayUsuario = array(
                    'nome' => $post['nome'],
                    'razaosocial' => $post['razaosocial'],
                    'cnpj' => $post['cnpj'],
                    'responsavel' => $post['responsavel'],
                    'email' => $post['email'],
                    'telefone' => $post['telefone'],
                    'telefone2' => $post['telefone2'],
                    'cep' => $post['cep'],
                    'rua' => $post['rua'],
                    'numero' => $post['numero'],
                    'complemento' => $post['complemento'],
                    'bairro' => $post['bairro'],
                    'cidade' => $post['idCidade'],
                    'ativo' => $post['ativo'],
                    'configuracaocores' => $post['configuracaocores'],
                    'configuracaocss' => $post['configuracaocss'],
                    'subdomain' => $post['subdomain'],
                    'horariodeatendimento' => $post['editor1'],

                    'idNivel' => 2
                );
                if (!empty($post['senha'])) {
                    $arrayUsuario['senha'] = hash('sha512', $post['senha']);
                }

                if (empty($id)) {
                    $objMercado->save($arrayUsuario);
                    $idInsert = $objMercado->getAdapter()->lastInsertId();
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);
                    unset($_POST);
                } else {

                    //verifica se estou alterando a foto
                    if (!empty($_FILES['foto']['size'])) {
                        $dadosUsuario = $objMercado->fetchRow("id = {$id}");

                        if (!empty($dadosUsuario['foto'])) {
                            $path = ROOT_PATH . '/public/uploads/mercados/';
                            @unlink($path . strtolower($dadosUsuario['foto']));
                            @unlink($path . 'thumbs/' . strtolower($dadosUsuario['foto']));
                        }
                    }

                    if (!empty($_FILES['foto2']['size'])) {
                        $dadosUsuario = $objMercado->fetchRow("id = {$id}");

                        if (!empty($dadosUsuario['foto2'])) {
                            $path = ROOT_PATH . '/public/uploads/mercados/';
                            @unlink($path . strtolower($dadosUsuario['foto2']));
                            @unlink($path . 'thumbs/' . strtolower($dadosUsuario['foto2']));
                        }
                    }

                    $objMercado->save($arrayUsuario, "id = {$id}");
                    $this->view->cadastro = array('erros' => false, 'sucesso' => true);

                    $post['taxa-retirada'] = str_replace(".", "", $post['taxa-retirada']);
                    $post['taxa-retirada'] = str_replace(",", ".", $post['taxa-retirada']);

                    if (!is_numeric($post['taxa-retirada'])) {
                        $post['taxa-retirada'] = 0;
                    }

                    //cadastro de regras de retirada

                    if (!empty($post['nome-regra-retirada'])) {
                        $arrayDadosRetirada = array (
                            'mercado' => $id,
                            'nomeregra' => $post['nome-regra-retirada'],
                            'taxa' => $post['taxa-retirada'],
                            'comprasde' => $post['retirada-compras-de-1'],
                            'comprasate' => $post['retirada-compras-ate-1'],
                            'retiradaem' => $post['retirada-em-1'],

                            'comprasde2' => $post['retirada-compras-de-2'],
                            'comprasate2' => $post['retirada-compras-ate-2'],
                            'retiradaem2' => $post['retirada-em-2'],

                            'comprasde3' => $post['retirada-compras-de-3'],
                            'comprasate3' => $post['retirada-compras-ate-3'],
                            'retiradaem3' => $post['retirada-em-3'],

                            'comprasde4' => $post['retirada-compras-de-4'],
                            'comprasate4' => $post['retirada-compras-ate-4'],
                            'retiradaem4' => $post['retirada-em-4'],

                            'faixacepde' => $post['cep-de2'],
                            'faixacepate' => $post['cep-ate2']
                        );

                        //$dadosRetirada = $objRegrasRetirada->fetchAll("mercado = {$id}");

                        if (!empty($post['codigoEdicaoRetirada'])) {
                            $objRegrasRetirada->save($arrayDadosRetirada,"id = {$post['codigoEdicaoRetirada']}");

                        } else {
                            $objRegrasRetirada->save($arrayDadosRetirada);
                        }
                    }

                    
                    //cadastro regrasEntrega
                    if (!empty($post['nome-regra'])) {

                        $post['taxa'] = str_replace(".", "", $post['taxa']);
                        $post['taxa'] = str_replace(",", ".", $post['taxa']);

                        if (!is_numeric($post['taxa'])) {
                            $post['taxa'] = 0;
                        }

                        $post['valor-minimo'] = str_replace(".", "", $post['valor-minimo']);
                        $post['valor-minimo'] = str_replace(",", ".", $post['valor-minimo']);

                        if (!is_numeric($post['valor-minimo'])) {
                            $post['valor-minimo'] = 0;
                        }

                        $arrayDadosEntrega = array (
                            'mercado' => $id,
                            'nomeregra' => $post['nome-regra'],
                            'valor' => $post['valor-minimo'],

                            'faixacepde' => $post['cep-de'],
                            'faixacepate' => $post['cep-ate'],

                            'taxacobrada' => $post['taxa'],

                            'comprasde' => $post['compras-de-1'],
                            'comprasate' => $post['compras-ate-1'],
                            'entregaem' => $post['entrega-em-1'],

                            'comprasde2' => $post['compras-de-2'],
                            'comprasate2' => $post['compras-ate-2'],
                            'entregaem2' => $post['entrega-em-2'],

                            'comprasde3' => $post['compras-de-3'],
                            'comprasate3' => $post['compras-ate-3'],
                            'entregaem3' => $post['entrega-em-3']
                        );

                        if (!empty($post['codigoEdicaoEntrega'])) {
                            $objRegrasEntrega->save($arrayDadosEntrega,"id = {$post['codigoEdicaoEntrega']}");
                        } else {
                            $objRegrasEntrega->save($arrayDadosEntrega);    
                        }

                        

                    }

                }

                if (!empty($_FILES['foto']['size'])) {
                    $arquivo = $_FILES['foto'];
                    $path = ROOT_PATH . '/public/uploads/mercados/';
                    $Original = new SW_ImageVerify();
                    $Original->createDir($path);
                    $Original->nameGenerator($arquivo['type']);
                    $ImagemOriginal = SW_WideImage::load($arquivo['tmp_name']);
                    $nomeArquivo = strtolower(md5(uniqid(time())));
                    if ($arquivo['type'] == 'image/jpeg' || $arquivo['type'] == 'image/jpg') {
                        $nomeArquivo = $nomeArquivo . '.jpg';
                        $ImagemOriginal->saveToFile($path . $nomeArquivo);
                        $thumb = SW_WideImage::load($arquivo['tmp_name'])->resize(300, 300, 'outside', 'down');
                        $thumb->saveToFile($path . 'thumbs/' . $nomeArquivo);
                    }
                    $idProduto = (!empty($idInsert) ? $idInsert : $id);
                    $arrayDataProdutoArquivo = array('foto' => $nomeArquivo);
                    $objMercado->save($arrayDataProdutoArquivo, "id = {$idProduto}");
                }

                if (!empty($_FILES['foto2']['size'])) {
                    $arquivo = $_FILES['foto2'];
                    $path = ROOT_PATH . '/public/uploads/mercados/';
                    $Original = new SW_ImageVerify();
                    $Original->createDir($path);
                    $Original->nameGenerator($arquivo['type']);
                    $ImagemOriginal = SW_WideImage::load($arquivo['tmp_name']);
                    $nomeArquivo = strtolower(md5(uniqid(time())));
                    if ($arquivo['type'] == 'image/jpeg' || $arquivo['type'] == 'image/jpg') {
                        $nomeArquivo = $nomeArquivo . '.jpg';
                        $ImagemOriginal->saveToFile($path . $nomeArquivo);
                        $thumb = SW_WideImage::load($arquivo['tmp_name'])->resize(300, 300, 'outside', 'down');
                        $thumb->saveToFile($path . 'thumbs/' . $nomeArquivo);
                    }
                    $idProduto = (!empty($idInsert) ? $idInsert : $id);
                    $arrayDataProdutoArquivo = array('foto2' => $nomeArquivo);
                    $objMercado->save($arrayDataProdutoArquivo, "id = {$idProduto}");
                }

                unset($_POST);
            }
        }

        if (!empty($id)) {
            

            if ($_SESSION['logado']['usuario']['idMercado']) {

                if ($id != $_SESSION['logado']['usuario']['idMercado']) {
                    $this->_redirect('/mercado/cadastro/'.$_SESSION['logado']['usuario']['idMercado']);
                }

                $dadosUsuario = $objMercado->fetchRow("id = {$_SESSION['logado']['usuario']['idMercado']}");
                $this->view->id = $_SESSION['logado']['usuario']['idMercado'];
            } else {
                $this->view->id = $id;
                $dadosUsuario = $objMercado->fetchRow("id = {$id}");    
            }
            
            if (!empty($dadosUsuario)) {

                $dadosUsuario['cidade'] = $objCidade->fetchRow("id = {$dadosUsuario['cidade']}");
                $dadosUsuario['cidade']['estado'] = $objEstado->fetchRow("id = {$dadosUsuario['cidade']['idEstado']}");
                $arrayListCidades = $objCidade->fetchAll("idEstado = {$dadosUsuario['cidade']['idEstado']}", "cidade ASC");

                $dadosFeriado = $objFeriado->fetchAll("mercado = {$id}");
                $dadosUsuario['feriados'] = $dadosFeriado;

                $dadosRetirada = $objRegrasRetirada->fetchAll("mercado = {$id}");
                $dadosUsuario['regrasRetirada'] = $dadosRetirada;

                $dadosEntrega = $objRegrasEntrega->fetchAll("mercado = {$id}");
                $dadosUsuario['regrasEntrega'] = $dadosEntrega;
                
                $this->view->dadosUsuario = $dadosUsuario;


            } else {
                $this->_redirect('/mercado/');
            }
        } else {
            if ($_SESSION['logado']['usuario']['idMercado']) {
                $this->_redirect('/mercado/cadastro/'.$_SESSION['logado']['usuario']['idMercado']);
            }
        }

        $range = range(strtotime("00:00"),strtotime("23:45"),15*60);


        if (!empty($post['idEstado'])) {
            $arrayListCidades = $objCidade->fetchAll("idEstado = {$post['idEstado']}", "cidade ASC");
        }

        $this->view->rangeHours = $range;
        $this->view->arrayListCidades = $arrayListCidades;
        $arrayListEstados = $objEstado->fetchAll(null, "estado ASC");
        $this->view->arrayListEstados = $arrayListEstados;
    }

    public function removerEntregaAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();


        if (!empty($post)) {
            $objRegrasEntrega = new Application_Model_DbTable_RegrasEntrega();
            $objRegrasEntrega->delete("id = '{$post['codigoDelecao']}' AND mercado = {$post['mercado']}");

            $this->_redirect('/mercado/cadastro/'.$post['mercado']);
        }

        
    }

    public function removerRetiradaAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();


        if (!empty($post)) {
            $objRegrasRetirada = new Application_Model_DbTable_RegrasRetirada();
            $objRegrasRetirada->delete("id = '{$post['codigoDelecao2']}' AND mercado = {$post['mercado']}");

            $this->_redirect('/mercado/cadastro/'.$post['mercado']);
        }

        
    }

    public function cadastroFeriadoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $objFeriado = new Application_Model_DbTable_Feriado();

        if (!empty($post) && !empty($post['mercado']) && !empty($post['data'])) {

            $dadosFeriado = $objFeriado->fetchAll("data = '{$post['data']}' AND mercado = {$post['mercado']}");

            if (empty($dadosFeriado)) {
                $arrayFeriado = array('data' => $post['data'], 'mercado' => $post['mercado']);
                $objFeriado->save($arrayFeriado);

            } else {
                $objFeriado->delete("data = '{$post['data']}' AND mercado = {$post['mercado']}");
            }

            

        }

        $dadosFeriado = $objFeriado->fetchAll("mercado = {$post['mercado']}");

        echo json_encode($dadosFeriado);

    }

}
