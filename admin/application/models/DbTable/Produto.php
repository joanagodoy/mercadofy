<?php

class Application_Model_DbTable_Produto extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'produtos';

    public function getBarCode() {
        return new Application_Model_BarCode();
    }

    public function verificaCodBarras($idProduto) {

        if (!file_exists( ROOT_PATH . '/uploads/produtos/codbarras/' . $idProduto . ".png")) {

            $dadosProduto = $this->fetchRow("id = {$idProduto}");
            $barCode = $this->getBarCode(); 

            $barCode->generate( ROOT_PATH . '/uploads/produtos/codbarras/' . $idProduto . ".png", $dadosProduto['ean']);

        }
        
    }

    public function getProdutos($idCategoria = null, $idProduto = null, $limite = null, $notIn = null, $termo = null) {
    	$sql = "SELECT "
            . "p.* "
            . ", (SELECT MIN(preco) as valor FROM produtos_mercados WHERE ativo = 1 AND idProduto = p.id) as precoMin "
            . ", (SELECT MAX(preco) as valor FROM produtos_mercados WHERE ativo = 1 AND idProduto = p.id) as precoMax "
            . ", c.id as categoriaPai "
            . ", cp.id as categoriaPai2 "
            . ", cp2.id as categoriaPai3 "
            . "FROM produtos p "
            . "LEFT JOIN categorias c ON (p.categoria = c.id) "
            . "LEFT JOIN categorias cp ON (c.categoria = cp.id) "
            . "LEFT JOIN categorias cp2 ON (cp.categoria = cp2.id) "
            . "WHERE p.ativo = 1 ";

        //$sql .= " AND (SELECT SUM(preco) FROM produtos_mercados WHERE ativo = 1 AND idProduto = p.id) >0 ";

    	if (!empty($idCategoria)) {
    		$sql .= " AND p.categoria = {$idCategoria} ";
    	}

    	if (!empty($idProduto)) {
    		$sql .= " AND p.id = {$idProduto} ";
    	}

        if (!empty($termo)) {
            $sql .= " AND p.nome like '%{$termo}%' ";
        }

        if (!empty($notIn)) {
            $notIn = explode(",",$notIn);
            foreach ($notIn as $umFiltro) {
                if (is_numeric($umFiltro)) {
                    $sql .= " AND p.id <> {$umFiltro} ";
                }
            }
        }

    	$sql .= " ORDER BY p.nome ";

    	if (!empty($limite)) {
    		$sql .= " LIMIT 0,{$limite} ";
    	}

    	if (!empty($idProduto)) {
    		$dadosProdutos = $this->getAdapter()->fetchRow($sql);
    	} else {
    		$dadosProdutos = $this->getAdapter()->fetchAll($sql);	
    	}

    	return $dadosProdutos;

    }
}

