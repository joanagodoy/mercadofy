<?php

class Application_Model_DbTable_Chaves extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'chaves';
	   

	public function getChave($random = false) {
        $dataHoje = Date('Y-m-d');

        // preciso verificar se o total de chaves usados por dia
        $sql = "SELECT * FROM chaves c WHERE (select count(id) from chaves_uso where chave = c.id AND date(data) = '{$dataHoje}') < c.limite ";

        if ($random) {
        	$sql .= " ORDER BY rand()";
        }

        $chave = parent::getAdapter()->fetchRow($sql);

        return $chave;
    }

}