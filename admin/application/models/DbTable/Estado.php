<?php

class Application_Model_DbTable_Estado extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'enderecos_estados';
    
    public function getDadosEstado($id) {
        if (!empty($id)) {
            $dadosEstado = $this->fetchRow("id = {$id}");
            if (!empty($dadosEstado)) {
                return $dadosEstado;
            }
        }
        return false;
    }
}

