<?php

class Application_Model_DbTable_Cidade extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'enderecos_cidades';
    
    public function getEstado() {
        return new Application_Model_DbTable_Estado();
    }
    
    public function getDadosCidade($id) {
        if (!empty($id)) {
            $dadosCidade = $this->fetchRow("id = {$id}");
            if (!empty($dadosCidade)) {
                $listCidade = $dadosCidade;
                $listCidade['estado'] = $this->getEstado()->getDadosEstado($dadosCidade['idEstado']);
                return $listCidade;
            }
        }
        return false;
    }
    

}