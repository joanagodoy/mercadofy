<?php

class Application_Model_DbTable_Pedido extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'pedidos';
    
    public function getPedidoItem() {
        return new Application_Model_DbTable_PedidoItem();
    }

    public function getMercado() {
        return new Application_Model_DbTable_Mercado();
    }

    public function getCliente() {
        return new Application_Model_DbTable_Cliente();
    }

    public function getStatus() {
        return new Application_Model_DbTable_PedidoStatus();
    }

    public function getOpcoesPagamento() {
        return new Application_Model_DbTable_OpcoesPagamento();
    }
    
    public function getDadosPedido($id, $idMercado) {
        $dadosProdutos = array();

    	if (!empty($id)) {

            if (!empty($idMercado)) {
                $dadosProdutos = $this->fetchRow("id = {$id} AND idMercado = {$idMercado}");    
            } else {
                $dadosProdutos = $this->fetchRow("id = {$id}");        
            }
            

            if (!empty($dadosProdutos)) {
                $dadosProdutos['mercado'] = $this->getMercado()->fetchRow("id = {$dadosProdutos['idMercado']}");
                $dadosProdutos['itens'] = $this->getPedidoItem()->getDadosItem($id);
                $dadosProdutos['cliente'] = $this->getCliente()->getDadosCliente($dadosProdutos['idCliente']);
                $dadosProdutos['status'] = $this->getStatus()->fetchRow("id = {$dadosProdutos['status']}");

                if (!empty($dadosProdutos['formapagamento'])) {
                    $dadosProdutos['formaDePagamento'] = $this->getOpcoesPagamento()->fetchRow("id = {$dadosProdutos['formapagamento']}");
                } else {
                    $dadosProdutos['formaDePagamento']['nome'] = null;    
                }
            }
            
        }

    	

    	return $dadosProdutos;

    }
    
}

