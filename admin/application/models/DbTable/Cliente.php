<?php

class Application_Model_DbTable_Cliente extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'clientes';

    public function getDadosCliente($idCliente) {
    	if (!empty($idCliente)) {
            $dadosCliente = $this->fetchRow("id = {$idCliente}", "id ASC");
            return $dadosCliente;
        }
        return false;
    }

}

