<?php

require_once('../library/Fpdf/fpdf.php');

class Application_Model_Pdf extends FPDF {

    public $alturaLinha = 5;
    public $tituloRelatorio;
    public $periodoRelatorio;
    public $empresa;
    public $hideHeader;
    public $hideBar = 'relatorio';
    public $orientation;

    public function __construct($dadosCabecalhoPDF, $hideHeader = null, $orientation = 'P', $unit = 'mm', $size = 'A4', $hideBar = 'relatorio') {
        $this->tituloRelatorio = $dadosCabecalhoPDF['title'];
        $this->periodoRelatorio = $dadosCabecalhoPDF['periodo'];
        $this->empresa = $dadosCabecalhoPDF['empresa'];
        $this->hideHeader = $hideHeader;
        $this->hideBar = $hideBar;
        $this->orientation = $orientation;

        parent::FPDF($orientation, $unit, $size);
        $this->AddPage();
    }

    public function Header() {
        $this->SetXY(10, 10);

        if ($this->hideBar == 'relatorio') {
            if ($this->orientation == 'P') {
                $this->Rect(10, 10, 190, 280);
            } else {
                $this->Rect(10, 10, 277, 192);
            }
        } else if ($this->hideBar == 'boleto') {
            if ($this->orientation == 'P') {
                $this->Rect(10, 10, 190, 100);
            } else {
                $this->Rect(10, 10, 277, 92);
            }
        }

        if (empty($this->hideHeader)) {
            $this->SetFont('Arial', '', 10);
            $this->Cell(100, $this->alturaLinha, utf8_decode($this->empresa), 0, 0, '', 0); //NOME DA EMPRESA
            $this->Cell(0, $this->alturaLinha, utf8_decode('DATA: ' . date('d/m/Y - H:i:s', time())), 0, 0, 'R', 0); //DATA GERAÇÃO

            $this->SetFont('Arial', 'B', 16);
            $this->SetXY(10, 20);
            $this->Cell(0, $this->alturaLinha, utf8_decode($this->tituloRelatorio), '', 0, 'C');
            $this->Ln();
            $this->SetFont('Arial', '', 10);
            $this->Cell(0, 10, utf8_decode($this->periodoRelatorio), 'B', 1, 'C');
        }
    }

    public function Footer() { // CRIANDO UM RODAPE
        if ($this->hideBar == 'relatorio') {
            $this->SetY(-15);
            $this->SetFont('Arial', 'I', 8);
            $this->AliasNbPages();
            $this->Cell(0, 10, utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'), 0, 0, 'R');
        } 
    }

}
