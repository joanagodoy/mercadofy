<?php

class Application_Model_Validacao {

    private $erros = array();
    public $arrayListMensagens = array(
        'NotEmpty' => 'O campo <strong>%s</strong> deve ser preenchido.',
        'FileEmpty' => 'Selecione um <strong>Arquivo</strong>.',
        'FileExtensao' => '<strong>Arquivo</strong> não permitido. Verifique extensão.',
    );

    public function tirarAcentos($string){

        $codigofiltro = str_replace(" ", "-", $string);
        $codigofiltro = str_replace("~", "", $codigofiltro);
        $codigofiltro = str_replace("º", "", $codigofiltro);
        $codigofiltro = str_replace("^", "", $codigofiltro);
        $codigofiltro = str_replace("&", "E", $codigofiltro);
        $codigofiltro = str_replace("ç", "C", $codigofiltro);
        $codigofiltro = str_replace("Ç", "C", $codigofiltro);
        $codigofiltro = str_replace("*", "", $codigofiltro);
        $codigofiltro = str_replace("`", "", $codigofiltro);
        $codigofiltro = str_replace("'", "", $codigofiltro);
        $codigofiltro = str_replace('"', "", $codigofiltro);
        $codigofiltro = str_replace('|', "-", $codigofiltro);
        $codigofiltro = str_replace('{', "-", $codigofiltro);
        $codigofiltro = str_replace('}', "-", $codigofiltro);
        $codigofiltro = str_replace("\\", "-", $codigofiltro);
        $codigofiltro = str_replace(",", "-", $codigofiltro);
        $codigofiltro = str_replace(".", "-", $codigofiltro);
        $codigofiltro = str_replace(":", "-", $codigofiltro);
        $codigofiltro = str_replace(";", "-", $codigofiltro);
        $codigofiltro = str_replace("+", "-", $codigofiltro);
        $codigofiltro = str_replace("@", "-", $codigofiltro);
        $codigofiltro = str_replace("Ø", "-", $codigofiltro);
        $codigofiltro = str_replace("/", "-", $codigofiltro);
        $codigofiltro = str_replace("--", "-", $codigofiltro);
        $codigofiltro = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$codigofiltro);

        return $codigofiltro;
    }

    public function check($arrayListValidacao) {
        if (!empty($arrayListValidacao) && is_array($arrayListValidacao)) {
            foreach ($arrayListValidacao as $key => $values) {
                if ($key <> 'File') {
                    $this->valida($key, $values);
                } else {
                    $this->validaFile($values);
                }
            }
        }
    }

    public function validaFile($file) {
        if (empty($file[0]['size'])) {
            $this->setErro($this->arrayListMensagens['FileEmpty']);
        } else {
            if (!in_array($file[0]['type'], $file[1])) {
                $this->setErro($this->arrayListMensagens['FileExtensao']);
            }
        }
    }

    public function valida($key, $values, $msg = false) {
        foreach ($values as $field => $value) {
            $label = $value[0];
            $valor = $value[1];
            $msg = isset($value[2]) ? $value[2] : false;
            if (!Zend_Validate::is($valor, $key)) {
                $mensagem = (!empty($msg) ? $msg : (array_key_exists($key, $this->arrayListMensagens) ? str_replace('%s', $label, $this->arrayListMensagens[$key]) : false));
                $this->setErro($mensagem);
            }
        }
    }

    public function setErro($erro) {
        $this->erros[] = $erro;
    }

    public function getErros() {
        return $this->erros;
    }

}
