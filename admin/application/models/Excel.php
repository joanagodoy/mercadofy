<?php

require_once('../library/Excel/PHPExcel.php');

class Application_Model_Excel extends PHPExcel {

	public function load($file) {
		require_once '../library/Excel/PHPExcel/IOFactory.php';
		$objPHPExcel = PHPExcel_IOFactory::load($file);

		return $objPHPExcel;

	}
}

?>