<?php

class Application_Model_MysqlBW extends Zend_Db_Table_Abstract {
    
    public function save($data, $where = false) {
        if (empty($where)) {
            $this->insert($data);
        } else {
            $this->update($data, $where);
        }
    }
}
