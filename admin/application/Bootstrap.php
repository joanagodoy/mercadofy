<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

	protected function _initAutoload() {
        $autoloader = new Zend_Application_Module_Autoloader(array('basePath' => APPLICATION_PATH, 'namespace' => 'sua_compra_admin'));
        return $autoloader;
    }

    protected function _initAutoLoader() {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('SW');
        $autoloader->registerNamespace('Fpdf');
        $autoloader->registerNamespace('Event');

    }

	public function onBootstrap(MvcEvent $e) {
		$eventManager = $e->getApplication()->getEventManager();
		$routeMatch = $e->getRouteMatch();
		$controller = $routeMatch->getParam(controller);
		$action = $routeMatch->getParam(action);
		$namespace = $routeMatch->getParam(__NAMESPACE__);
		
	}

	protected function _initRouter() {
        $this->bootstrap('frontController');
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/routes.ini', 'routes');
        $router = $this->getResource('frontController')->getRouter()->addConfig($config, 'routes');
        return $router;
    }

}

