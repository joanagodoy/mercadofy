function detectmob() {

    if (window.innerWidth <= 800 && window.innerHeight <= 600) {
        return true;
    } else {
        return false;
    }
}

var listaIdsArray;
var listaIds = "";
var options = new Array();

$(document).ready(function() {

    function realizaContagem() {

        // se for pra marcar apenas itens da lista
        if ($("#innerTodos").html() == '') {
            $("#contador").html($(".selecionaProdutos:checked").length);
        
        //se for pra marcar todos da listagem mostra um contador geral
        } else {
            $("#contador").html(listaIdsArray.length);
        }
        
    }

    function getParametros() {
        var dataForm = new Array();
        $('.parametros').each(function () {
            key = $(this).attr('rel');
            value = $(this).val();
            dataForm.push(key + '=' + value);
        });
        data = dataForm.join(',');
        return data;
    }

    function geraListagemMobile() {
        oTable.columns().eq(0).each(function (colIdx) {
            var coluna = oTable.columns(colIdx).header().to$();
            mobile = detectmob();
            if (coluna.hasClass('hidden-xs') && mobile === true) {
                oTable.columns(colIdx).visible(false);
            }
        });

        $(".selecionaProdutos").on("click", function() {
            realizaContagem();
        });
    }

    var oTable = $('#dynamic-table').DataTable({
        processing: false,
        serverSide: true,
        orderCellsTop: true,
        order: [[0, 'asc']],
        aLengthMenu: [
            [25, 50, 100, 200],
            [25, 50, 100, 200]
        ],
        iDisplayLength: 200,
        ajax: {
            url: baseUrl + controller + '/listagem/',
            type: 'post',
            data: {
                action: action,
                parametros: getParametros()
            }
        },
        columnDefs: [
            {
                targets: 0,
                className: false,
                data: null,
                render: function (data, type, row) {

                    if ($('#dynamic-table').find('thead th:eq(-1)').hasClass('produtos')) {
                        return '<input type="checkbox" name="idProdutos[]" class="selecionaProdutos" value="'+data[0]+'" />';
                    } else {
                        return data[0];    
                    }
                    
                }
            },
            {
                targets: 1,
                className: false,
                data: null,
                render: function (data, type, row) {
                    if ($('#dynamic-table').find('thead th:eq(-1)').hasClass('nivel')) {
                        return  '<a data-original-title="Editar" data-placement="top" data-toggle="tooltip" href="'+ baseUrl + controller + '/nivel-cadastro/' + data[1] + '/" class="btn btn-primary btn-xs tooltips"><i class="fa fa-edit"></i></a>';    

                    } else if ($('#dynamic-table').find('thead th:eq(-1)').hasClass('produtos') || $('#dynamic-table').find('thead th:eq(-1)').hasClass('lightbox')) {
                        if (data[1] != '') {

                            if ($('#dynamic-table').find('thead th:eq(-1)').hasClass('produtos') ) {
                                return '<a data-fancybox="gallery" href="'+baseUrl+'uploads/produtos/thumbs/'+data[1]+'"/><img src="'+baseUrl+'uploads/produtos/thumbs/'+data[1]+'" width="40" heigth="40" /></a>';
                            } else {
                                return '<a data-fancybox="gallery" href="'+baseUrl+'uploads/boasvindas/'+data[1]+'"/><img src="'+baseUrl+'uploads/boasvindas/'+data[1]+'" width="40" heigth="40" /></a>';
                            }

                            
                        } else {
                            return '<span class="fa fa-camera-alt"></span>';
                        }
                        
                    } else {
                        return data[1];    
                    }
                    
                }
            },
             {
                targets: -1,
                searchable: false,
                orderable: false,
                className: 'text-center',
                width: '5%',
                render: function (data, type, row) {

                    if ($('#dynamic-table').find('thead th:eq(-1)').hasClass('nivel')) {
                        return  '<a data-original-title="Editar" data-placement="top" data-toggle="tooltip" href="'+ baseUrl + controller + '/nivel-cadastro/' + data + '/" class="btn btn-primary btn-xs tooltips"><i class="fa fa-edit"></i></a>';    

                    } else if ($('#dynamic-table').find('thead th:eq(-1)').hasClass('pedidos')) {
                        return  '<a data-original-title="Visualizar" data-placement="top" data-toggle="tooltip" href="'+ baseUrl + controller + '/visualizar/' + row[0] + '/" class="btn btn-primary btn-xs tooltips"><i class="fa fa-search"></i></a>';    

                    } else if ($('#dynamic-table').find('thead th:eq(-1)').hasClass('cadastro-mercado')) {
                        return  '<a data-original-title="Visualizar" data-placement="top" data-toggle="tooltip" href="'+ baseUrl + controller + '/cadastro-mercado/' + data + '/" class="btn btn-primary btn-xs tooltips"><i class="fa fa-edit"></i></a>';    

                    } else if ($('#dynamic-table').find('thead th:eq(-1)').hasClass('produtos')) {

                        return  '<a data-original-title="Visualizar" data-placement="top" data-toggle="tooltip" href="'+ baseUrl + controller + '/cadastro/' + row[0] + '/" class="btn btn-primary btn-xs tooltips"><i class="fa fa-edit"></i></a>';    

                    } else {

                        return  '<a data-original-title="Editar" data-placement="top" data-toggle="tooltip" href="'+ baseUrl + controller + '/cadastro/' + data + '/" class="btn btn-primary btn-xs tooltips"><i class="fa fa-edit"></i></a>';    
                    }


                }
            }
        ],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var categorias = $("#dynamic-table").hasClass("categorias");
                var produtos = $("#dynamic-table").hasClass("produtos");

                if (produtos) {
                    if (column[0][0] == 8) {

                        //coluna status de produtos
                        /*if (column[0][0] == 13 || column[0][0] == 12 || column[0][0] == 11 || column[0][0] == 10
                            || column[0][0] == 9 || column[0][0] == 8 || column[0][0] == 7) {
                            var select = $('<select name="categoria_'+column[0][0]+'"><option value="">TODOS</option></select>')
                                .appendTo( $(column.header()).empty() )
                                .on( 'change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );
             
                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();
                            } );
                        } else {
                            var select = $('<select name="categoria_'+column[0][0]+'"><option value="">TODAS</option><option value="-">SEM CATEGORIA</option></select>')
                                .appendTo( $(column.header()).empty() )
                                .on( 'change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );
             
                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();
                            } ); 
                        }
                        */

                        column.data().unique().sort().each( function ( d, j ) {
                            if (d != null) {
                                //select.append( '<option value="'+d+'">'+d+'</option>' ) ;

                                //aqui adiciona os OPTIONS   
                                if (column[0][0] == 8) {
                                    options.push(d);
                                }   
                                
                            }
                            
                        } );
                    }
                }

                if (categorias) {

                    if (column[0][0] == 1 || column[0][0] == 2) {
                        var select = $('<select name="categoria_'+column[0][0]+'"><option value="">TODAS</option><option value="-">SEM CATEGORIA</option></select>')
                            .appendTo( $(column.header()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
         
                                column
                                    .search( val ? val : '', true, false )
                                    .draw();
                        } );

                        column.data().unique().sort().each( function ( d, j ) {
                            if (d != null) {
                                select.append( '<option value="'+d+'">'+d+'</option>' )        
                            }
                            
                        } );
                    }
                }
            } );
        },
        drawCallback: function (settings) {

            var response = settings.json;
            
            if (response.listaIds) {
                listaIds = response.listaIds;
            }

            $('.tooltips').tooltip();
            geraListagemMobile();

            if ($('#dynamic-table').find('thead th:eq(-1)').hasClass('produtos')) {
                realizaContagem();
            };
        }
    });


    $("#dynamic-table_paginate").on("click", "a", function () {
        geraListagemMobile();
    });

    if ($('#dynamic-table').find('thead th:eq(-1)').hasClass('produtos')) {

        var obj;

        $('#dynamic-table').on( 'click', 'tbody td', function (e) {

            var texto = $(this)[0].innerText;
            
            var col = $(this).index(),
                row = $(this).parent().index();

            var obj = $(this);

            if (obj.html().indexOf("<select") < 0) {
                if (col == 2 && texto != '') {
                    obj.html("").append("<input class='form-control alteracaoProduto' type='text' data-id=\""+$(this).closest("tr").find("td:first-child")[0].firstChild.value+"\" value=\""+texto+"\">");
                    coluna = 'nome';
                    $(".alteracaoProduto").focus();
                } else if (col == 8 && texto != '') {

                    optionsToAdd = "";

                    for (i = 0; i < options.length; i++) { 
                        if (options[i] == texto) {
                            optionsToAdd += "<option value=\""+ options[i] +"\" selected> " + options[i] + "</option>"; 
                        } else {
                            optionsToAdd += "<option value=\""+ options[i] +"\"> " + options[i] + "</option>"; 
                        }
                    }

                    obj.html("").html("<select class='form-control alteracaoProduto' data-id=\""+$(this).closest("tr").find("td:first-child")[0].firstChild.value+"\">"+optionsToAdd+"</select>");
                    coluna = 'unidademedida';
                    $(".alteracaoProduto").focus();
                } else if (col == 9 && texto != '') {
                    obj.html("").append("<input class='form-control alteracaoProduto' type='text' data-id=\""+$(this).closest("tr").find("td:first-child")[0].firstChild.value+"\" value=\""+texto+"\">");
                    coluna = 'pesovolume';
                    $(".alteracaoProduto").focus();
                }
            }


            $(".alteracaoProduto").on("change", function() {
                var valorFinal = $(".alteracaoProduto option:selected").val();
                //ajax de update
                $.ajax({
                    type: "POST",
                    url: baseUrl + "produto/alteracao-produto",
                    data: {
                        id: $(this)[0].dataset.id,
                        valor: valorFinal,
                        coluna: coluna
                    },
                    dataType: "json",
                    success: function(json){
                        $(obj).html(valorFinal);
                    }
                });

            });

            $(".alteracaoProduto").on("blur", function() {
                var valorFinal = $(this).val();
                //ajax de update
                $.ajax({
                    type: "POST",
                    url: baseUrl + "produto/alteracao-produto",
                    data: {
                        id: $(this)[0].dataset.id,
                        valor: valorFinal,
                        coluna: coluna
                    },
                    dataType: "json",
                    success: function(json){
                        $(obj).html(valorFinal);
                    }
                });

            });

        });   

        $("#marcaTodos").on("click", function() {
            $(".selecionaProdutos").prop("checked", true);

            realizaContagem();
        });

        $("#desmarcaTodos").on("click", function() {
            $("#desmarcaTodosRegistros").click();
            $(".selecionaProdutos").prop("checked", false);

            realizaContagem();
        });

        $("#marcaTodosRegistros").on("click", function() {

            $("#innerTodos").html('<input type="text" name="idProdutos" value="'+listaIds+'"/><input type="text" name="marqueiTodos" value="true"/>');

            listaIdsArray = listaIds.split(",");

            $(".selecionaProdutos").attr("disabled", true);
            $(".selecionaProdutos").prop("checked", true);

            realizaContagem();
        });

        $("#desmarcaTodosRegistros").on("click", function() {
            $("#innerTodos").html('');

            $(".selecionaProdutos").prop("checked", false);
            $(".selecionaProdutos").attr("disabled", false);

            realizaContagem();
        });

        $(window).keydown(function(event){
            if( (event.keyCode == 13) ) {
                event.preventDefault();
                return false;
            }
        });



        // filtros
        $('#categoria').on( 'change', function () {
           
            var valor = $(this).val();

            $.ajax({
                url: baseUrl + '/categoria/busca-categorias',
                data: {valor: valor},
                type: 'POST',
                beforeSend: function () {
                    $('#subcategoria').html("<option selected disabled>Carregando...</option>");
                }, success: function (r) {
                    $('#subcategoria').html(r);
                    $('#subcategoria2').html("<option selected value=''>Todas</option>");

                     oTable.columns(6).search(valor).draw();
                }
            });

        });

        $('#subcategoria').on( 'change', function () {
            var valor = $(this).val();

            $.ajax({
                url: baseUrl + '/categoria/busca-categorias',
                data: {valor: valor},
                type: 'POST',
                beforeSend: function () {
                    $('#subcategoria2').html("<option selected disabled>Carregando...</option>");
                }, success: function (r) {
                    $('#subcategoria2').html(r);

                    oTable.columns(5).search(valor).draw();
                }
            });

        });

        $('#subcategoria2').on( 'change', function () {
            oTable.columns(4).search(this.value).draw();
        });

        $('#marca').on( 'change', function () {
            oTable.columns(7).search(this.value).draw();
        });

        $('#unidademedida').on( 'change', function () {
            oTable.columns(8).search(this.value).draw();
        });

        $('#pesovolume').on( 'keyup', function () {
            oTable.columns(9).search(this.value).draw();
        });

        $('#sabor').on( 'change', function () {
            oTable.columns(10).search(this.value).draw();
        });

        $('#tamanho').on( 'change', function () {
            oTable.columns(11).search(this.value).draw();
        });

        $('#tipo').on( 'change', function () {
            oTable.columns(12).search(this.value).draw();
        });

        $('#pais').on( 'change', function () {
            oTable.columns(13).search(this.value).draw();
        });

        $('#tipo2').on( 'change', function () {
            oTable.columns(14).search(this.value).draw();
        });

        $('#status').on( 'change', function () {
            oTable.columns(15).search(this.value).draw();
        });

        
    }
     

} );