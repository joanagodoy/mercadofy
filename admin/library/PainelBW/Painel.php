<?php

class PainelBW_Painel extends Zend_Controller_Action {

    private $base;
    private $module;
    private $controller;
    private $action;
    private $arrayListBreadCrumbs = array();

    public function isMobile() {
        $iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
        $palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
        $berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
        $ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $symbian = strpos($_SERVER['HTTP_USER_AGENT'], "Symbian");

        if ($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true) {
            return true;
        } else {
            return false;
        }
    }

    public function init() {
        $this->_initVars();

        if (Zend_Auth::getInstance()->hasIdentity() && $this->controller <> 'cron' && $this->action <> 'recuperar-senha' ) {
            $idNivel = $_SESSION['logado']['usuario']['idNivel'];

            if (!is_numeric($idNivel)) {
                $idNivel = 0;
            }

            $this->montaMenu($idNivel);
            $this->getNotificacoes();

            $controller = $this->controller;
            $action = $this->action;

            //correcao para ajax e categorias
            if ($action == 'categorias-ajax') {
                $action = 'listagem';
            }

            //correcao para usuario mercado poder remover regras de entrega e retirada
            if ($action == 'remover-entrega' && $controller == 'mercado') {
                $controller = $this->controller;
                $action = 'cadastro';
            }
            if ($action == 'remover-retirada' && $controller == 'mercado') {
                $controller = $this->controller;
                $action = 'cadastro';
            }

            //correcao feriados
            if ($action == 'cadastro-feriado' && $controller == 'mercado') {
                $controller = $this->controller;
                $action = 'cadastro';
            }

            if ($action == 'busca-categorias' && $controller == 'categoria') {
                $controller = $this->controller;
                $action = 'cadastro';
            }

            if (($action == 'visualizar' || $action == 'impressao') && $controller == 'pedido') {
                $controller = $this->controller;
                $action = 'index';
            }

            if (($action == 'alteracao' || $action == 'alteracao-produto') && $controller == 'produto') {
                $controller = $this->controller;
                $action = 'index';
            }

            //correcao para listagem logout e recuperar senha
            if ($controller != 'index' && $action != 'index' && $action != 'recuperar-senha') {
            
                if ($action != 'listagem' && $action != 'logout') {

                    $objMenu = new Application_Model_DbTable_Menu();
                    $idMenu = $objMenu->fetchAll("controller = '{$controller}' AND action = '{$action}'", "id asc");
                    $entrou = false;

                    if (!empty($idMenu)) {
                        foreach ($idMenu as $umMenu) {
                            $objNivelMenu = new Application_Model_DbTable_MenuNiveis();
                            $acessoOk = $objNivelMenu->fetchOne("COUNT(*)", "idMenu = {$umMenu['id']} AND idNivel = {$idNivel}");    

                            if ($acessoOk) {
                                $entrou = true;
                                break;
                            }
                        }
                    }


                    if (!$entrou) {
                        $this->_redirect('permissao-negada');
                    }
                }
            }
        }
    }

    public function getNotificacoes() {
        $objPedidos = new Application_Model_DbTable_Pedido();

        $str = " pedidos_status.id = 1 ";

        if (!empty($_SESSION['logado']['usuario']['idMercado']) && $_SESSION['logado']['usuario']['idMercado'] != "0") { 
            $str .= " AND pedidos.idMercado = {$_SESSION['logado']['usuario']['idMercado']}";
        }

        if (!isset($_SESSION['logado']['usuario']['idMercado'])) {
            $this->_redirect('/autenticacao/logout');
        }

        $sql = "SELECT "
            . "pedidos.id "
            . "FROM pedidos "
            . "LEFT JOIN pedidos_status ON (pedidos.status = pedidos_status.id) "
            . "WHERE {$str} ";

        $arrayNotificacoes = $objPedidos->getAdapter()->fetchAll($sql);

        Zend_Layout::getMvcInstance()->assign('arrayNotificacoes', $arrayNotificacoes);
    }

    public function montaMenu($idNivel) {
        $objMenu = new Application_Model_DbTable_Menu();

        $tipoMenu = "0,2";

        if ($_SESSION['logado']['usuario']['idMercado'] == 0) {
            $tipoMenu = "0,1";
        }

        $sqlMenu = "SELECT
					menus.*
					FROM menus
					INNER JOIN menus_niveis ON menus_niveis.idMenu = menus.id
					WHERE menus.idMenuPai IS NULL 
					AND menus.ativo = 1
                    AND menus.master in ({$tipoMenu})
					AND menus_niveis.idNivel = {$idNivel}
					ORDER BY menus.ordem ASC";

        $arrayListMenuPai = $objMenu->getAdapter()->fetchAll($sqlMenu);

        $arrayListItensMenu = array();

        if (!empty($arrayListMenuPai)) {
            foreach ($arrayListMenuPai as $umMenuPai) {
                $arrayListItensMenuFilho = array();
                $sqlSubmenu = "SELECT
								menus.*
								FROM menus
								INNER JOIN menus_niveis ON menus_niveis.idMenu = menus.id
								WHERE menus.idMenuPai = {$umMenuPai['id']}
								AND menus.ativo = 1
                                AND menus.master in ({$tipoMenu})
                                AND menus.ordem > 0
								AND menus_niveis.idNivel = {$idNivel}
								ORDER BY menus.ordem ASC";
                $arrayListMenuFilho = $objMenu->getAdapter()->fetchAll($sqlSubmenu);

                if (!empty($arrayListMenuFilho)) {
                    foreach ($arrayListMenuFilho as $umMenuFilho) {
                        $arrayListItensMenuFilho[] = array(
                            'id' => $umMenuFilho['id'],
                            'titulo' => $umMenuFilho['menu'],
                            'controller' => $umMenuFilho['controller'],
                            'action' => $umMenuFilho['action'],
                            'master' => $umMenuFilho['master'],
                            'url' => $umMenuFilho['controller'] . '/' . (!empty($umMenuFilho['action']) && $umMenuFilho['action'] <> 'index' ? $umMenuFilho['action'] . '/' : false),
                            'current' => ($umMenuFilho['controller'] == $this->controller && $umMenuFilho['action'] == $this->action ? true : false)
                        );
                    }
                }

                $arrayListItensMenu[]['menu'] = array(
                    'id' => $umMenuPai['id'],
                    'titulo' => $umMenuPai['menu'],
                    'icone' => $umMenuPai['icone'],
                    'controller' => $umMenuPai['controller'],
                    'action' => $umMenuPai['action'],
                    'master' => $umMenuPai['master'],
                    'url' => (!empty($umMenuPai['action']) ? $umMenuPai['controller'] . '/' . (!empty($umMenuPai['action']) && $umMenuPai['action'] <> 'index' ? $umMenuPai['action'] . '/' : false) : '#'),
                    'current' => ($umMenuPai['controller'] == $this->controller ? true : false),
                    'submenu' => $arrayListItensMenuFilho
                );
            }
        }

        Zend_Layout::getMvcInstance()->assign('arrayListMenu', $arrayListItensMenu);
    }

    private function _initVars() {
        $this->base = $this->getFrontController()->getBaseUrl();
        $this->module = $this->view->Module = $this->getRequest()->getModuleName();
        $this->controller = $this->view->Controller = $this->getRequest()->getControllerName();
        $this->action = $this->view->Action = $this->getRequest()->getActionName();
    }

}
