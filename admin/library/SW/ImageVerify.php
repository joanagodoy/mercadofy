<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SW_ImageVerify
 *
 * @author FrancisG
 */
class SW_ImageVerify {

    public $image;
    public $prefix;
    public $folder;
    public $ext;
    public $extension;
    public $newName;
    public $archives = array();
    public $condition = false;

    public function createDir($folder) {
        $this->folder = $folder;
        if (!is_dir($this->folder)) {
            $mask = umask(0);
            mkdir($this->folder, 0777);
            umask($mask);
        }
    }

    public function nameGenerator($type, $prefix = '') {
        $this->type = $type;
        $this->prefix = $prefix;

        switch ($this->type) {
            case 'image/pjpeg':
                $ext = '.jpg';
                break;
            case 'image/jpg':
                $this->extension = '.jpg';
                break;
            case 'image/jpeg':
                $this->extension = '.jpg';
                break;
            case 'image/gif':
                $this->extension = '.gif';
                break;
            case 'image/x-png':
                $this->extension = '.png';
                break;
            case 'image/png':
                $this->extension = '.png';
                break;
            default:
                $this->extension = '.jpg';
        }

        $this->newName = $this->prefix . '-'. date("dmYhis") . $this->extension;
    }

    public function returnName() {
        return $this->folder . $this->newName;
    }

}

?>
