<?php

use Cielo\API30\Merchant;
use Cielo\API30\Ecommerce\Environment;
use Cielo\API30\Ecommerce\Sale;
use Cielo\API30\Ecommerce\CieloEcommerce;
use Cielo\API30\Ecommerce\Payment;
use Cielo\API30\Ecommerce\CreditCard;
use Cielo\API30\Ecommerce\Request\CieloRequestException;

class Application_Model_Cielo {
	
	public function processarPagamentoCartaoCredito($arrayCartao) {
            // Configure seu merchant
            //merchant Sandbox
             $environment = $environment = Environment::sandbox();
             $merchant = new Merchant('ba7f3133-1ef7-4412-ba20-5810f2437168', 'LKHVIRXYLIOZIZTKXCLJUVJURFMHJIQXZVPPGGRG');

            //merchant Producao
//            $environment = $environment = Environment::production();
//            $merchant = new Merchant('6f5f4932-3708-4202-af75-4fd560703bae', 'JVKtvhTmW6eKj460bouERS9giiYSvFRkc8kwKAnn');

            // Crie uma instância de Sale informando o ID do pedido na loja
            $sale = new Sale($arrayCartao['idPedido']);

            // Crie uma instância de Customer informando o nome do cliente
            $customer = $sale->customer($arrayCartao['idPedido']);

            // Crie uma instância de Credit Card utilizando os dados de teste
            // esses dados estão disponíveis no manual de integração
            
            $bandeira = $arrayCartao['bandeira'];
            $creditCardType = CreditCard::VISA;
            if($bandeira == "MASTER"){
                    $creditCardType = CreditCard::MASTERCARD;
            }else if($bandeira == "AMEX"){
                    $creditCardType = CreditCard::AMEX;
            }elseif($bandeira == "ELO"){
                    $creditCardType = CreditCard::ELO;
            }else if($bandeira == "AURA"){
                    $creditCardType = CreditCard::AURA;
            }else if($bandeira == "JCB"){
                    $creditCardType = CreditCard::JCB;
            }else if($bandeira == "DINERS"){
                    $creditCardType = CreditCard::DINERS;
            }else if($bandeira == "DISCOVER"){
                    $creditCardType = CreditCard::DISCOVER;
            }	
            
            $valor = str_replace(".", "", $arrayCartao['valor']);
            // Crie uma instância de Payment informando o valor do pagamento
            $payment = $sale->payment($valor);
            $payment->setType(Payment::PAYMENTTYPE_CREDITCARD)
                            ->creditCard($arrayCartao['cvv'], $creditCardType)
                            ->setExpirationDate($arrayCartao['validade'])
                            ->setCardNumber($arrayCartao['numeroCartao'])
                            ->setHolder($arrayCartao['titular']);


            // Crie o pagamento na Cielo
            try {
                // Configure o SDK com seu merchant e o ambiente apropriado para criar a venda
                $sale = (new CieloEcommerce($merchant, $environment))->createSale($sale);

                // Com a venda criada na Cielo, já temos o ID do pagamento, TID e demais
                // dados retornados pela Cielo

                    $paymentId = $sale->getPayment()->getPaymentId();
                    $returnCode = $sale->getPayment()->getReturnCode();
                    //status 3 nao autorizada e 2 cancelada
                    //só captura se status 1
                    $returnMessage = $sale->getPayment()->getReturnMessage();
                    $status = $sale->getPayment()->getStatus();

                    // echo $paymentId."<br>";
                    // echo $returnCode."<br>";
                    // echo $returnMessage."<br>";	
                    // echo $status."<br>";

                    if($status == 1){//autorizado
                            // Com o ID do pagamento, podemos fazer sua captura, se ela não tiver sido capturada ainda
                            $sale = (new CieloEcommerce($merchant, $environment))->captureSale($paymentId, $valor, 0);

                            //COMPRA REALIZADA COM SUCESSO
                    }
                    error_log($status.";".$returnCode.";".$returnMessage);
                    return $status.";".$returnCode.";".$returnMessage;

                    // E também podemos fazer seu cancelamento, se for o caso
                // $sale = (new CieloEcommerce($merchant, $environment))->cancelSale($paymentId, 15700);
            } catch (CieloRequestException $e) {
                // Em caso de erros de integração, podemos tratar o erro aqui.
                // os códigos de erro estão todos disponíveis no manual de integração.
                $error = $e->getCieloError();  
//                if($error != null){
                    return $error->getCode();
//                }
            }
	}
        
        public function gerarBoleto($arrayDados) {
            // Configure seu merchant
            //merchant Sandbox
             $environment = $environment = Environment::sandbox();
             $merchant = new Merchant('ba7f3133-1ef7-4412-ba20-5810f2437168', 'LKHVIRXYLIOZIZTKXCLJUVJURFMHJIQXZVPPGGRG');

            //merchant Producao
//            $environment = $environment = Environment::production();
//            $merchant = new Merchant('6f5f4932-3708-4202-af75-4fd560703bae', 'JVKtvhTmW6eKj460bouERS9giiYSvFRkc8kwKAnn');

            // Crie uma instância de Sale informando o ID do pedido na loja
            $sale = new Sale($arrayDados['idPedido']);

            // Crie uma instância de Customer informando o nome do cliente,
            // documento e seu endereço
            $customer = $sale->customer($arrayDados['nome'])
                              ->setIdentity($arrayDados['cpf'])
                              ->setIdentityType($arrayDados['cpf'])
                              ->address()->setZipCode($arrayDados['cep'])
                                         ->setCountry('BRA')
                                         ->setState("SC")
                                         ->setCity($arrayDados['cidade'])
                                         ->setDistrict($arrayDados['bairro'])
                                         ->setStreet($arrayDados['rua'])
                                         ->setNumber($arrayDados['numero']);

            $valor = str_replace(".", "", $arrayDados['valor']);
            // Crie uma instância de Payment informando o valor do pagamento
            $payment = $sale->payment($valor)
                            ->setType(Payment::PAYMENTTYPE_BOLETO)
//                            ->setAddress($arrayDados['idPedido'])
                            ->setBoletoNumber($arrayDados['idPedido'])
                            ->setAssignor('Mercado Fique Em Casa')
//                            ->setDemonstrative($arrayDados['idPedido'])
                            ->setExpirationDate(date('d/m/Y', strtotime('+7 days')))
                            ->setIdentification('26.952.893/0001-69')
                            ->setInstructions('Esse boleto depende do registro no banco emissor. Caso não consiga pagar imediatamente, por favor, tente mais tarde.');

            // Crie o pagamento na Cielo
            try {
                // Configure o SDK com seu merchant e o ambiente apropriado para criar a venda
                $sale = (new CieloEcommerce($merchant, $environment))->createSale($sale);

                // Com a venda criada na Cielo, já temos o ID do pagamento, TID e demais
                // dados retornados pela Cielo
                $paymentId = $sale->getPayment()->getPaymentId();
                $boletoURL = $sale->getPayment()->getUrl();
                return $boletoURL;
            } catch (CieloRequestException $e) {
                // Em caso de erros de integração, podemos tratar o erro aqui.
                // os códigos de erro estão todos disponíveis no manual de integração.
                $error = $e->getCieloError();
                return $error->getCode();
                //enviar email para admin sobre erro
                error_log("error ".$error->getCode());
                error_log("error ".$error->getMessage());
            }
	}
}

?>