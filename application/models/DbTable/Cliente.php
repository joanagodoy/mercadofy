<?php

class Application_Model_DbTable_Cliente extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'clientes';

    public function getClienteEndereco() {
        return new Application_Model_DbTable_ClienteEndereco();
    }

    public function getDadosCliente($idCliente) {
    	if (!empty($idCliente)) {
            $dadosCliente = $this->fetchRow("id = {$idCliente}", "id ASC");
            $dadosCliente['enderecos'] = $this->getClienteEndereco()->getEnderecos($idCliente);

            foreach($dadosCliente['enderecos'] as $key => $umEndereco) {
            	$ret = $this->getAdapter()->fetchRow("SELECT e.uf as estado, c.cidade as nome FROM enderecos_cidades c, enderecos_estados e WHERE c.idEstado = e.id AND c.id = {$umEndereco['cidade']}");
            	$dadosCliente['enderecos'][$key]['cidade'] = $ret;
            }

            //protege estrutura da senha
            unset($dadosCliente['senha']);
            unset($dadosCliente['tokenSenha']);

            return $dadosCliente;
        }
        return false;
    }
    
}

