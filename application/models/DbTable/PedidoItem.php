<?php

class Application_Model_DbTable_PedidoItem extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'pedidos_itens';

    public function getProduto() {
        return new Application_Model_DbTable_Produto();
    }

    public function getAtributos() {
        return new Application_Model_DbTable_Atributo();
    }

    public function getDadosItem($idPedido) {
        $objAtributo = $this->getAtributos();

        if (!empty($idPedido)) {
            $arrayListItens = $this->fetchAll("idPedido = {$idPedido}", "id ASC");
            if (!empty($arrayListItens)) {
                foreach ($arrayListItens as $key => $umItem) {
                    $dadosProduto = $this->getProduto()->getProdutos(null, $umItem['idProduto']);

                    $dados[$key] = $umItem;
                    $dados[$key]['produto'] = $dadosProduto;

                    if (!empty($dadosProduto['marca'])) {
                        $dados[$key]['produto']['marca'] = $objAtributo->fetchRow("id = {$dadosProduto['marca']}");    
                    }
                    if (!empty($dadosProduto['embalagem'])) {
                        $dados[$key]['produto']['embalagem'] = $objAtributo->fetchRow("id = {$dadosProduto['embalagem']}");
                    }
                    if (!empty($dadosProduto['pais'])) {
                        $dados[$key]['produto']['pais'] = $objAtributo->fetchRow("id = {$dadosProduto['pais']}");
                    }
                    if (!empty($dadosProduto['unidademedida'])) {
                        $dados[$key]['produto']['unidademedida'] = $objAtributo->fetchRow("id = {$dadosProduto['unidademedida']}");
                    }
                    if (!empty($dadosProduto['sabor'])) {
                        $dados[$key]['produto']['sabor'] = $objAtributo->fetchRow("id = {$dadosProduto['sabor']}");
                    }
                    if (!empty($dadosProduto['tamanho'])) {
                        $dados[$key]['produto']['tamanho'] = $objAtributo->fetchRow("id = {$dadosProduto['tamanho']}");
                    }
                    if (!empty($dadosProduto['tipoAt'])) {
                        $dados[$key]['produto']['tipoAt'] = $objAtributo->fetchRow("id = {$dadosProduto['tipoAt']}");
                    }

                }
                return $dados;
            }
        }
        return false;
    }
    
}