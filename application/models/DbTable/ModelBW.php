<?php

class Application_Model_DbTable_ModelBW extends Zend_Db_Table_Abstract {

    public function fetchRow($where = null, $order = null, $offset = null) {
        $data = parent::fetchRow($where, $order, $offset);
        return $this->arrayMap($data);
    }

    public function fetchAll($where = null, $order = null, $count = null, $offset = null) {
        $data = parent::fetchAll($where, $order, $count, $offset);
        return $this->arrayMap($data, true);
    }

    public function fetchOne($field, $where = null, $order = null) {
        $table = $this->_name;
        $clausulaWhere = (!empty($where) ? "WHERE {$where}" : false);
        $clausulaOrder = (!empty($order) ? "ORDER BY {$order}" : false);
        $sql = "SELECT {$field} FROM {$table} {$clausulaWhere} {$clausulaOrder}";
        return parent::getAdapter()->fetchOne($sql);
    }

    public function save($data, $where = null, $converterMaiusculo = null, $notNull = null) {
        if (empty($converterMaiusculo)) {
            $dataToUpper = array_map(array($this, 'converteMinMai'), $data);
        } else {
            $dataToUpper = $data;
        }
        
        if (empty($notNull)) {
            $dataToUpper = array_map(array($this, 'emptyToNull'), $dataToUpper);
        } else {
            $dataToUpper = $dataToUpper;
        }
        
        if (empty($where)) {
            parent::insert($data);
        } else {
            parent::update($data, $where);
        }
    }

    public function emptyToNull($string) {

        if (empty($string) && $string != '0') {
            return new Zend_Db_Expr('NULL');
        }
        return $string;
    }

    public function converteMinMai($string) {
        return strtr(strtoupper($string), "àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ", "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß");
    }

    public function arrayMap($data, $arrayMult = null) {
        if (!empty($data)) {
            if (!empty($arrayMult)) {
                $dataMap = array();
                foreach ($data->toArray() as $umaData) {
                    $dataMap[] = array_map('stripslashes', $umaData);
                }
            } else {
                $dataMap = array_map('stripslashes', $data->toArray());
            }
            return $dataMap;
        }
        return false;
    }

    public function dinamicTable($sql, $columns, $post, $total = null) {
        $recordsTotal = count($this->getAdapter()->fetchAll($sql));

        if (!empty($post['search']['value'])) {
            $search = "AND (";
            foreach ($columns as $column) {
                $search .= $column . " LIKE '%{$post['search']['value']}%' OR ";
            }
            $search = substr_replace($search, "", -3);
            $search .= ')';
            $sql .= $search;
            $recordsTotal = count($this->getAdapter()->fetchAll($sql));
        }

        if (empty($total)) {
            $sql .= "ORDER BY {$columns[$post['order'][0]['column']]} {$post['order'][0]['dir']} ";
            $sql .= "LIMIT {$post['start']}, {$post['length']}";
            $dados = $this->getAdapter()->fetchAll($sql);

            $output = array(
                'draw' => intval($post['draw']),
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsTotal,
                'data' => array()
            );

            $rows = array();
            foreach ($dados as $umDado) {
                $dadosValues = array_values($umDado);
                if (!empty($umDado['id'])) {
                    $dadosValues['DT_RowId'] = $umDado['id'];
                }
                $rows[] = $dadosValues;
            }

            $output['data'] = $rows;
            return $output;
        } else {
            $dados = $this->getAdapter()->fetchOne($sql);
            return (!empty($dados) ? $dados : 0);
        }
    }

}
