<?php

class Application_Model_DbTable_Cidade extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'enderecos_cidades';
    
    public function getEstado() {
        return new Application_Model_DbTable_Estado();
    }

    public function getDadosCEP($cep) {

        if (!empty($cep)) {

            $sql = "SELECT "
                        . "l.*, e.id as idEstado "
                        . "FROM log_localidade l INNER JOIN enderecos_estados e ON l.ufe_sg = e.uf "
                        . "WHERE l.cep = '{$cep}' ";
            $dadosPedido = $this->getAdapter()->fetchAll($sql);

            foreach ($dadosPedido as $keyPedidos => $umPedido) {
                $dados = array( 
                    'estado' => $umPedido['ufe_sg'],
                    'idEstado' => $umPedido['idEstado'],
                    'localidade' => strtoupper($umPedido['loc_no']),
                    'localidadecodigo' => $umPedido['loc_nu_sequencial'],
                    'bairro' => '',
                    'cep' => $umPedido['cep'],
                    'abreviado' => ''
                    );

                $dadosCep[] = $dados;
            }

            $sql = "SELECT "
                        . "logx.ufe_sg,  logx.log_nome,  loc.loc_no,  loc.loc_nu_sequencial, bai.bai_no, logx.cep, e.id as idEstado "
                        . "FROM log_logradouro logx  INNER JOIN enderecos_estados e ON logx.ufe_sg = e.uf INNER JOIN  log_localidade loc ON loc.loc_nu_sequencial = logx.loc_nu_sequencial  INNER JOIN  log_bairro bai ON bai.bai_nu_sequencial = logx.bai_nu_sequencial_ini  "
                        . "WHERE logx.cep = '{$cep}' ";
            $dadosPedido = $this->getAdapter()->fetchAll($sql);

            foreach ($dadosPedido as $keyPedidos => $umPedido) {
                $dados = array( 
                    'estado' => $umPedido['ufe_sg'],
                    'idEstado' => $umPedido['idEstado'],
                    'localidade' => strtoupper($umPedido['loc_no']),
                    'localidadecodigo' => $umPedido['loc_nu_sequencial'],
                    'bairro' => $umPedido['bai_no'],
                    'cep' => $umPedido['cep'],
                    'abreviado' => $umPedido['log_nome']
                    );

                $dadosCep[] = $dados;
            }

            $sql = "SELECT "
                        . "gu.ufe_sg, gu.gru_no,  gu.gru_endereco,  loc.loc_no,  loc.loc_nu_sequencial,  bai.bai_no, gu.cep, e.id as idEstado "
                        . "FROM log_grande_usuario gu INNER JOIN enderecos_estados e ON gu.ufe_sg = e.uf INNER JOIN log_localidade loc ON loc.loc_nu_sequencial = gu.loc_nu_sequencial INNER JOIN  log_bairro bai ON bai.bai_nu_sequencial = gu.bai_nu_sequencial "
                        . "WHERE gu.cep = '{$cep}' ";
            $dadosPedido = $this->getAdapter()->fetchAll($sql);

            foreach ($dadosPedido as $keyPedidos => $umPedido) {
                $dados = array( 
                    'estado' => $umPedido['ufe_sg'],
                    'idEstado' => $umPedido['idEstado'],
                    'localidade' => strtoupper($umPedido['loc_no']),
                    'localidadecodigo' => $umPedido['loc_nu_sequencial'],
                    'bairro' => $umPedido['bai_no'],
                    'cep' => $umPedido['cep'],
                    'abreviado' => $umPedido['gru_endereco']
                    );

                $dadosCep[] = $dados;
            }

            $sql = "SELECT "
                        . "uo.ufe_sg, uo.uop_no,  uo.uop_endereco,  loc.loc_no,  loc.loc_nu_sequencial, bai.bai_no, uo.cep, e.id as idEstado "
                        . "FROM log_unid_oper uo INNER JOIN enderecos_estados e ON uo.ufe_sg = e.uf  INNER JOIN  log_localidade loc ON loc.loc_nu_sequencial = uo.loc_nu_sequencial  INNER JOIN  log_bairro bai ON bai.bai_nu_sequencial = uo.bai_nu_sequencial "
                        . "WHERE uo.cep = '{$cep}' ";
            $dadosPedido = $this->getAdapter()->fetchAll($sql);

            foreach ($dadosPedido as $keyPedidos => $umPedido) {
                $dados = array( 
                    'estado' => $umPedido['ufe_sg'],
                    'idEstado' => $umPedido['idEstado'],
                    'localidade' => strtoupper($umPedido['loc_no']),
                    'localidadecodigo' => $umPedido['loc_nu_sequencial'],
                    'bairro' => $umPedido['bai_no'],
                    'cep' => $umPedido['cep'],
                    'abreviado' => $umPedido['uop_endereco']
                    );

                $dadosCep[] = $dados;
            }

            $sql = "SELECT "
                        . " cp.ufe_sg, cp.cpc_no,  cp.cpc_endereco,  loc.loc_no, loc.loc_nu_sequencial, cp.cep, e.id as idEstado "
                        . "FROM log_cpc cp INNER JOIN enderecos_estados e ON cp.ufe_sg = e.uf   INNER JOIN  log_localidade loc ON loc.loc_nu_sequencial = cp.loc_nu_sequencial "
                        . "WHERE cp.cep = '{$cep}' ";
            $dadosPedido = $this->getAdapter()->fetchAll($sql);

            foreach ($dadosPedido as $keyPedidos => $umPedido) {
                $dados = array( 
                    'estado' => $umPedido['ufe_sg'],
                    'idEstado' => $umPedido['idEstado'],
                    'localidade' => strtoupper($umPedido['loc_no']),
                    'localidadecodigo' => $umPedido['loc_nu_sequencial'],
                    'bairro' => '',
                    'cep' => $umPedido['cep'],
                    'abreviado' => $umPedido['cpc_endereco']
                    );

                $dadosCep[] = $dados;
            }

            if (!empty($dadosCep)) {
                return $dadosCep;    
            } else {
                return false;
            }
            
        }

        return false;
    }
    
    public function getDadosCidade($id) {
        if (!empty($id)) {
            $dadosCidade = $this->fetchRow("id = {$id}");
            if (!empty($dadosCidade)) {
                $listCidade = $dadosCidade;
                $listCidade['estado'] = $this->getEstado()->getDadosEstado($dadosCidade['idEstado']);
                return $listCidade;
            }
        }
        return false;
    }
    

}