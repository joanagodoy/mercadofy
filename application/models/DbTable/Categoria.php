<?php

class Application_Model_DbTable_Categoria extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'categorias';
    protected $_arrayBreadcrumbs = array();
    protected $_arrayCategorias = array();

    public function getProduto() {
        return new Application_Model_DbTable_Produto();
    }

    public function montaBreadCrumbs($idCategoria) {

    	$this->base = Zend_Controller_Front::getInstance()->getBaseUrl();

    	if (empty($this->_arrayBreadcrumbs)) {
    		$umArray = array(
    			'nome' => "Sua Compra Fácil",
    			'link' => $this->base . "/"
     		);
     		$this->_arrayBreadcrumbs[] = $umArray;
    	}

    	if (!empty($idCategoria)) {
    		$dadosRegistro = $this->fetchRow("id = {$idCategoria}");	

    		if (!empty($dadosRegistro)) {

    			if (!empty($dadosRegistro['categoria']) && $dadosRegistro['categoria'] > 0) {
    				$this->montaBreadCrumbs($dadosRegistro['categoria']);
    			}

    			$umArray = array(
	    			'nome' => $dadosRegistro['nome'],
	    			'link' => $this->base . "/categoria/" . strtolower($dadosRegistro['codigofiltro'])
	     		);
		        $this->_arrayBreadcrumbs[] = $umArray;
    			

    		}
    	}
    }

    public function getBreadCrumbs() {
        return $this->_arrayBreadcrumbs;
    }


}