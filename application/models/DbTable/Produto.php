<?php

class Application_Model_DbTable_Produto extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'produtos';

    public function getProdutos($idCategoria = null, $idProduto = null, $limite = null, $notIn = null, $termo = null, $ordem = null, $marcas = null, $tipos = null, $subCategorias = null, $termoBusca = null, $idMercadoMaster = null) {
    	
        $sql = "SELECT "
            . "p.* "
            . ", (SELECT MIN(preco) as valor FROM produtos_mercados WHERE ativo = 1 AND idProduto = p.id) as precoMin "
            . ", (SELECT MAX(preco) as valor FROM produtos_mercados WHERE ativo = 1 AND idProduto = p.id) as precoMax "
            . ", c.id as categoriaPai "
            . ", c.nome as nomeCategoriaPai "
            . ", c.codigofiltro as linkCategoriaPai "
            . ", cp.id as categoriaPai2 "
            . ", cp.nome as nomeCategoriaPai2 "
            . ", cp.codigofiltro as linkCategoriaPai2 "
            . ", cp2.id as categoriaPai3 "
            . ", cp2.nome as nomeCategoriaPai3 "
            . ", cp2.codigofiltro as linkCategoriaPai3 "

            . ", c.meta "
            . ", c.description "
            . ", cp.meta as meta2 "
            . ", cp.description description2 "
            . ", cp2.meta as meta3 "
            . ", cp2.description description3 "

            . ", GROUP_CONCAT(me.nome) as mercados "
            . ", sum(pm.estoque) as totalestoques "
            
            . "FROM produtos p "
            . "LEFT JOIN categorias c ON (p.categoria = c.id) "
            . "LEFT JOIN categorias cp ON (c.categoria = cp.id) "
            . "LEFT JOIN categorias cp2 ON (cp.categoria = cp2.id) "
            // marca do produto
            . "LEFT JOIN atributos m ON (m.id = p.marca) "
            // tipo do produto
            . "LEFT JOIN atributos a ON (a.id = p.tipoAt) "
            // sabor para busca de produtos
            . "LEFT JOIN atributos s ON (s.id = p.sabor) "

            . "LEFT JOIN produtos_mercados pm ON (pm.idProduto = p.id) "

            . "LEFT JOIN mercados me ON (pm.idMercado = me.id) "

            . "WHERE p.ativo = 1 AND pm.ativo = 1 AND pm.estoque > 0 ";


            if (!empty($_SESSION['idMercadoMaster'])) {
                $sql .= "AND me.ativo in (2,3)";
            } else {
                $sql .= "AND me.ativo in (1,2)";
            }

        //$sql .= " AND (SELECT SUM(preco) FROM produtos_mercados WHERE ativo = 1 AND idProduto = p.id) > 0 ";



    	if (!empty($idCategoria) && empty($subCategorias)) {
    		$sql .= " AND p.categoria = {$idCategoria} ";
    	}

        if (!empty($termoBusca)) {
            $sql .= " AND (p.nome like '%{$termoBusca}%' 
                        OR 
                    m.valor like '%{$termoBusca}%'
                        OR 
                    s.valor like '%{$termoBusca}%') ";
        }

    	if (!empty($idProduto)) {

            if (is_numeric($idProduto)) {
                $sql .= " AND p.id = {$idProduto} ";
            } else {

                $sql .= " AND p.link = '{$idProduto}' ";
            }
    		
    	}

        if (!empty($termo)) {
            $sql .= " AND p.nome like '%{$termo}%' ";
        }

        if (!empty($marcas)) {

            $sql .= "
                AND 
                    m.valor in ({$marcas})
            ";

        }

        if (!empty($tipos)) {

            $sql .= "
                AND 
                    a.valor in ({$tipos})
            ";

        }

        if (!empty($idMercadoMaster)) {
            $sql .= " AND me.id = '{$idMercadoMaster}' ";
        }

        if (!empty($notIn)) {
            $notIn = explode(",",$notIn);
            foreach ($notIn as $umFiltro) {
                if (is_numeric($umFiltro)) {
                    $sql .= " AND p.id <> {$umFiltro} ";
                }
            }
        }

        if (!empty($subCategorias)) {
            $sql .= " AND ( 1 = 0 ";
            foreach ($subCategorias as $umCt) {

                if (is_array($umCt)) {
                    $sql .= " OR p.categoria = {$umCt['id']} ";
                } else {
                    $sql .= " OR p.categoria = {$umCt} ";
                }
                
            }

            $sql .= " ) ";

        }

        $sql .= " GROUP BY p.id ";    

        if (!empty($ordem)) {
            $sql .= " ORDER BY {$ordem} ";
        } else {
            $sql .= " ORDER BY p.nome ";    
        }

    	if (!empty($limite)) {
    		$sql .= " LIMIT 0,{$limite} ";
    	}

    	if (!empty($idProduto)) {
    		$dadosProdutos = $this->getAdapter()->fetchRow($sql);
    	} else {
    		$dadosProdutos = $this->getAdapter()->fetchAll($sql);	
    	}

    	return $dadosProdutos;

    }
}