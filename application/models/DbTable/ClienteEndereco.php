<?php

class Application_Model_DbTable_ClienteEndereco extends Application_Model_DbTable_ModelBW
{
    protected $_name = 'clientes_enderecos';

    public function getEnderecos($idCliente, $idEndereco = null) {
    	$dados = array();

    	$sql = " AND end.cliente = {$idCliente}";

    	if (!empty($idEndereco)) {
			$sql .= " AND end.id = {$idEndereco}";    		
    	}

    	$dados = $this->getAdapter()->fetchAll("SELECT end.*, e.uf as estado, e.id as idEstado, c.cidade as nome FROM  clientes_enderecos end, enderecos_cidades c, enderecos_estados e WHERE c.idEstado = e.id AND c.id = end.cidade {$sql}");

    	return $dados;
    }
}

