<?php

class Application_Model_BoletoFacil {
	
//	public function gerarBoleto($arrayDados) {
//           
//            // API URL
//            $url = 'https://sandbox.boletobancario.com/boletofacil/integration/api/v1';
////            $url = 'https://www.boletobancario.com/boletofacil/integration/api/v1';
//
//            // Create a new cURL resource
//            
//
//            $valor = str_replace(".", "", $arrayDados['valor']);
//            
//           $arrayBoleto = array(
//                'token' => "A40982DE6FC39081B34BC9845126A14100781991FD004AFFC7679FA892A5C1C7", //sandbox
////                'token' => "8F5649EFCA91C16953E45F326B14D6D2755EFDC837F379DFBDB6BDFF3B05A91E",
//                'description' => "Boleto Referente Compra no Mercado Fique Em Casa",
//                'amount' => $valor,
//                'payerName' => $arrayDados['nome'],
//                'payerCpfCnpj' => $arrayDados['cpf']
//            );
//            error_log("ARRAY BF ".$arrayBoleto);
//            $jsonDados = json_encode($arrayBoleto);
////            $payload = json_encode(array("user" => $data));
//            error_log("JSON BF ".$jsonDados);
//            try {
//                $ch = curl_init($url);
//                
//                // Attach encoded JSON string to the POST fields
//                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDados);
//
//                // Set the content type to application/json
//                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
////                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//
//                // Return response instead of outputting
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
//                // Execute the POST request
//                $result = curl_exec($ch);
//                error_log("RESULTADO BF ".$result);
//                // Close cURL resource
//                curl_close($ch);
//            } catch (Exception $ex) {
//                error_log($ex);
//            }
//            
//	}
    public $transferAmount;

    public $description;
    public $reference;
    public $amount;
    public $dueDate;
    public $installments;
    public $maxOverdueDays;
    public $fine;
    public $interest;
    public $discountAmount;
    public $discountDays;

    public $payerName;
    public $payerCpfCnpj;
    public $payerEmail;
    public $payerSecondaryEmail;
    public $payerPhone;
    public $payerBirthDate;

    public $billingAddressStreet;
    public $billingAddressNumber;
    public $billingAddressComplement;
    public $billingAddressCity;
    public $billingAddressState;
    public $billingAddressPostcode;

    public $notifyPayer;
    public $notificationUrl;
    
    private $token;
    private $sandbox;

    const PROD_URL = "https://www.boletobancario.com/boletofacil/integration/api/v1/";
    const SANDBOX_URL = "https://sandbox.boletobancario.com/boletofacil/integration/api/v1/";
    const RESPONSE_TYPE = "JSON";

    function __construct($token, $sandbox = false) {
        $this->token = $token;
        $this->sandbox = $sandbox;
    }

//    public function createCharge($payerName, $payerCpfCnpj, $description, $amount, $payerEmail, $payerPhone, $billingAddressStreet, $billingAddressNumber, $billingAddressCity, $billingAddressState, $billingAddressPostcode) {
    public function createCharge($arrayBoleto) {
        error_log("VALOR2 ".$arrayBoleto['valor']);
        $this->payerName = $arrayBoleto['nome'];
        $this->payerCpfCnpj = $arrayBoleto['cpf'];
        $this->description = $arrayBoleto['descricao'];
        $this->amount = $arrayBoleto['valor'];
        $this->payerEmail = $arrayBoleto['email'];
        $this->payerPhone = $arrayBoleto['telefone'];
        $this->billingAddressStreet = $arrayBoleto['rua'];
        $this->billingAddressNumber = $arrayBoleto['numero'];
        $this->billingAddressCity = $arrayBoleto['cidade'];
        $this->billingAddressState = "SC";
        $this->billingAddressPostcode = $arrayBoleto['cep'];   
//        $this->payerName = $payerName;
//        $this->payerCpfCnpj = $payerCpfCnpj;
//        $this->description = $description;
//        $this->amount = $amount;
////        $this->dueDate = $dueDate;
//        $this->payerEmail = $payerEmail;
//        $this->payerPhone = $payerPhone;
//        $this->billingAddressStreet = $billingAddressStreet;
//        $this->billingAddressNumber = $billingAddressNumber;
////        $this->billingAddressComplement = $billingAddressComplement;
//        $this->billingAddressCity = $billingAddressCity;
//        $this->billingAddressState = $billingAddressState;
//        $this->billingAddressPostcode = $billingAddressPostcode;        
        return $this;
    }

    public function issueCharge() {
        $requestData = array(
            'token'                     =>  $this->token,
            'description'               =>  $this->description,
            'reference'                 =>  $this->reference,
            'amount'                    =>  $this->amount,
            'dueDate'                   =>  $this->dueDate,
            'installments'              =>  $this->installments,
            'maxOverdueDays'            =>  $this->maxOverdueDays,
            'fine'                      =>  $this->fine,
            'interest'                  =>  $this->interest,
            'discountAmount'            =>  $this->discountAmount,
            'discountDays'              =>  $this->discountDays,
            'payerName'                 =>  $this->payerName,
            'payerCpfCnpj'              =>  $this->payerCpfCnpj,
            'payerEmail'                =>  $this->payerEmail,
            'payerSecondaryEmail'       =>  $this->payerSecondaryEmail,
            'payerPhone'                =>  $this->payerPhone,
            'payerBirthDate'            =>  $this->payerBirthDate,
            'billingAddressStreet'      =>  $this->billingAddressStreet,
            'billingAddressNumber'      =>  $this->billingAddressNumber,
            'billingAddressComplement'  =>  $this->billingAddressComplement,
            'billingAddressCity'        =>  $this->billingAddressCity,
            'billingAddressState'       =>  $this->billingAddressState,
            'billingAddressPostcode'    =>  $this->billingAddressPostcode,
            'notifyPayer'               =>  $this->notifyPayer,
            'notificationUrl'           =>  $this->notificationUrl,
            'responseType'              =>  Application_Model_BoletoFacil::RESPONSE_TYPE
        );
        
        $object = json_decode($this->request("issue-charge", $requestData), true); 
        $success = $object['success'];
        if($success){
            $retorno = $object['data']['charges'][0]['checkoutUrl'];
        }else{
            $retorno = "erro";
        }
        return $retorno;
    }

    public function fetchPaymentDetails($paymentToken) {
        $requestData = array(
            'paymentToken'   => $paymentToken,
            'responseType'   => Application_Model_BoletoFacil::RESPONSE_TYPE
        );

        return $this->request("fetch-payment-details", $requestData);
    }


    public function fetchBalance() {
        $requestData = array(
            'token'         => $this->token,
            'responseType'  => Application_Model_BoletoFacil::RESPONSE_TYPE
        );

        return $this->request("fetch-balance", $requestData);
    }


    public function requestTransfer() {
        $requestData = array(
            'token'         => $this->token,
            'responseType'  => Application_Model_BoletoFacil::RESPONSE_TYPE,
            'amount'        => $this->transferAmount
        );

        return $this->request("request-transfer", $requestData);
    }


    public function cancelCharge($code) {
        $requestData = array(
            'token'         => $this->token,
            'code'          => $code,
            'responseType'  => Application_Model_BoletoFacil::RESPONSE_TYPE
        );

        return $this->request("cancel-charge", $requestData);
    }


    private function request($urlSufix, $data) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => ($this->sandbox ? Application_Model_BoletoFacil::SANDBOX_URL : Application_Model_BoletoFacil::PROD_URL).$urlSufix,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "UTF-8",
            CURLOPT_MAXREDIRS => 2,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query($data) . "\n",
            CURLOPT_HTTPHEADER => $data
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        error_log("RESPONSE BF".$response);
        return $response;
    }
}

?>