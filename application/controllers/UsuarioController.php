<?php

class Default_UsuarioController extends PainelBW_Painel {

    public function init() {
    	Zend_Auth::getInstance()->hasIdentity();
        parent::init();
    }

    public function enderecoAction() {
        $id = $this->getRequest()->getParam('id');
        $request = $this->getRequest()->getPathInfo();
        $post = $this->getRequest()->getPost();
        $objClienteEndereco = new Application_Model_DbTable_ClienteEndereco();
        $objEstado = new Application_Model_DbTable_Estado();
        $objCidade = new Application_Model_DbTable_Cidade();
        $objCliente = new Application_Model_DbTable_Cliente();
        $arrayListCidades = array();

        if (empty($_SESSION['Default']['usuario']['id'])) {
            $this->_redirect('/');
        }

        if (!empty($post)) {


            $arrayEndereco = array(
                'cep' => $post['cep'],
                'rua' => $post['rua'],
                'cliente' => $_SESSION['Default']['usuario']['id'],
                'numero' => $post['numero'],
                'complemento' => $post['complemento'],
                'bairro' => $post['bairro'],
                'cidade' => $post['cidade']
            );

            if (empty($id)) {
                $objClienteEndereco->save($arrayEndereco);
            } else {
                $objClienteEndereco->save($arrayEndereco, "id = {$id} AND cliente = {$_SESSION['Default']['usuario']['id']}");                
            }

            $_SESSION['Default']['usuario']['dados'] = $objCliente->getDadosCliente($_SESSION['Default']['usuario']['id']);

            $this->_redirect('/usuario/#meus-enderecos');

        }

        if (!empty($id)) { 
            $dadosEndereco = $objClienteEndereco->getEnderecos($_SESSION['Default']['usuario']['id'], $id)[0];

            if (empty($dadosEndereco)) {
                $this->_redirect('/');
            } else {
                $this->view->dadosEndereco = $dadosEndereco;
            }

            $arrayListCidades = $objCidade->fetchAll("idEstado = {$dadosEndereco['idEstado']}", "cidade ASC");

        } else {
            if (!empty($post['estado'])) {
                $arrayListCidades = $objCidade->fetchAll("idEstado = {$post['estado']}", "cidade ASC");
            }
        }

        $arrayListEstados = $objEstado->fetchAll(null, "estado ASC");

        $this->view->id = $id;
        $this->view->arrayListEstados = $arrayListEstados;
        $this->view->arrayListCidades = $arrayListCidades;
    }

    public function indexAction() {
    	$request = $this->getRequest()->getPathInfo();
    	$objPedido = new Application_Model_DbTable_Pedido();
        $objCliente = new Application_Model_DbTable_Cliente();
        $objClienteEndereco = new Application_Model_DbTable_ClienteEndereco();
        $post = $this->getRequest()->getPost();


        if (empty($_SESSION['Default']['usuario']['id'])) {
            $this->_redirect('/');
        }

        if (!empty($post)) {


            //remove endereco
            if (!empty($post['idRemoverEndereco'])) {
                $objClienteEndereco->delete("id = {$post['idRemoverEndereco']} AND cliente = {$_SESSION['Default']['usuario']['id']}");
                $_SESSION['Default']['usuario']['dados'] = $objCliente->getDadosCliente($_SESSION['Default']['usuario']['id']);
            }

            //salva dados
            if (isset($post['salvarDados'])) {

                if (!empty($post['alterar_senha'])) {

                    //verifica senha atual
                    $dadosUsuario = $objCliente->fetchRow("id = {$_SESSION['Default']['usuario']['id']}");


                    if (strtolower($dadosUsuario['senha']) != hash('sha512', $post['pass_confirmation'])) {
                        $erros[] = "Senha atual não confere.";
                    } else if ($post['new_pass'] != $post['pass']) {
                        $erros[] = "Senha nova e confirmação não confere.";
                    }

                }

                if (!empty($erros)) {

                    $_SESSION['Default']['usuario']['cadastro']['erros'] = $erros;
                    $this->_redirect('/usuario/#dados-pessoais');
                } else {

                    $arrayCliente = array(
                        'nome' => $post['nome'],
                        'email' => $post['email'],
                        'cpf' => $post['cpf'],
                        'telefone' => $post['telefone'],
                        'celular' => $post['celular']
                    );

                    if (!empty($post['alterar_senha'])) {
                        $arrayCliente['senha'] = hash('sha512', $post['new_pass']);
                    }

                    $objCliente->save($arrayCliente,"id = {$_SESSION['Default']['usuario']['id']}");
                    unset($_POST);

                    $this->_redirect('/usuario/#dados-pessoais');

                }

            }
        }

    	//pedidos do cliente
    	$arrayListPedidos = $objPedido->getDadosPedido(null, $_SESSION['Default']['usuario']['id']);

    	$this->view->arrayListPedidos = $arrayListPedidos;

        $dadosUsuario = $objCliente->fetchRow("id = {$_SESSION['Default']['usuario']['id']}");

        $this->view->dadosUsuario = $dadosUsuario;

        $arrayListEnderecos = $objCliente->getDadosCliente("{$_SESSION['Default']['usuario']['id']}")['enderecos'];

        $this->view->arrayListEnderecos = $arrayListEnderecos;

    }


}
