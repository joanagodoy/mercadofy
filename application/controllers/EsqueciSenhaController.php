<?php
class Default_EsqueciSenhaController extends PainelBW_Painel
{
    public function init() {
        parent::init();

    }
	
	public function indexAction() {
		$id = $this->getRequest()->getParam('id');
	 	$post = $this->getRequest()->getPost();
		$objCliente = new Application_Model_DbTable_Cliente();

	    $dadosUsuario = $objCliente->fetchRow("tokenSenha = '{$id}'");

	    if (empty($dadosUsuario)) {
	    	$this->_redirect('/');
	    }

	    $this->view->id = $id;

	    if (!empty($post['botao_recuperar_senha'])) {
	    	$arrayUsuario['tokenSenha'] = null;
	    	$arrayUsuario['senha'] = hash('sha512', $post['confirmar_nova_senha']);
			$objCliente->save($arrayUsuario, "id = {$dadosUsuario['id']}");

			$_SESSION['msgErroLogin'] = "Sua senha foi alterada com sucesso.";
			$this->_redirect('/');
	    }

	}
}
    