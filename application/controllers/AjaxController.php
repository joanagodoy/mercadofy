<?php

class Default_AjaxController extends Zend_Controller_Action {

    public function init() {
        Zend_Auth::getInstance()->hasIdentity();
        $this->_initVars();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        // verifica se tenho um mercado com o subdominio validado
        // mesma funcao do painel.php
        $objMercado = new Application_Model_DbTable_Mercado();
//        $qMercado = $objMercado->fetchRow("subdomain = '".URL."'");
        $qMercado = $objMercado->fetchRow("id = 19");

        $this->mercadoMaster = $this->view->mercadoMaster = $qMercado;

        if (!empty($_SESSION['idMercadoMaster'])) {
            if ($_SESSION['idMercadoMaster'] != $qMercado['id']) {
                
                $_SESSION['idMercadoMaster'] = $qMercado['id'];
                // zera o carrinho também
                unset($_SESSION['Default']['usuario']['carrinho']);
            }
        } else {
            $_SESSION['idMercadoMaster'] = $qMercado['id'];
        }

    }
    
    private function _initVars() {
        $this->Base = $this->getFrontController()->getBaseUrl();
    }

    public function buscaHorariosMercadoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $objRetirada = new Application_Model_DbTable_RegrasRetirada();
        $post = $this->getRequest()->getPost();

        if (!empty($post['data']) && !empty($post['tipo'])) {

            $idMercado = $_SESSION['Default']['usuario']['mercado']['id'];
            $cepAtual = str_replace("-", "", $_SESSION['Default']['usuario']['endereco']['cep']);

            $sql = "SELECT * FROM ";

            $arrayFinal = array();
            $horaAtual = (double) Date("H.i");
            $post['diaHoje'] =  Date("Y-m-d");
            $post['dataComparacao'] = join('-', array_reverse(explode('/', $post['data'])));

            if ($post['tipo'] == 'dia_entrega') {
                $sql .= "regrasentrega WHERE mercado = {$idMercado} AND {$cepAtual} BETWEEN CAST(REPLACE(faixacepde, '-', '') AS UNSIGNED) AND CAST(REPLACE(faixacepate, '-', '') AS UNSIGNED) ";

                $arrayDados = $objRetirada->getAdapter()->fetchRow($sql);

                //se nao for o dia de hoje, mostro todas opcoes de entrega dos horários
                if ($post['diaHoje'] != $post['dataComparacao']) {

                    if (!empty($arrayDados['entregaem'])) {
                        $arrayFinal[] = $arrayDados['entregaem'];
                    }
                    if (!empty($arrayDados['entregaem2'])) {
                        $arrayFinal[] = $arrayDados['entregaem2'];
                    }
                    if (!empty($arrayDados['entregaem3'])) {
                        $arrayFinal[] = $arrayDados['entregaem3'];
                    }

                // se o dia de hoje é o dia que quero que entregue, eu verifico conforme meu horário
                } else {

                    $comprasDe = (double) str_replace(":", ".", $arrayDados['comprasde']);
                    $comprasAte = (double) str_replace(":", ".", $arrayDados['comprasate']);

                    $comprasDe2 = (double) str_replace(":", ".", $arrayDados['comprasde2']);
                    $comprasAte2 = (double) str_replace(":", ".", $arrayDados['comprasate2']);

                    $comprasDe3 = (double) str_replace(":", ".", $arrayDados['comprasde3']);
                    $comprasAte3 = (double) str_replace(":", ".", $arrayDados['comprasate3']);


                    if ($horaAtual >= $comprasDe && $horaAtual <= $comprasAte) {
                        $arrayFinal[] = $arrayDados['entregaem'];
                    } else {
                        if ($horaAtual <= $comprasDe) {
                            $arrayFinal[] = $arrayDados['entregaem'];
                        }
                    }



                    if ($horaAtual >= $comprasDe2 && $horaAtual <= $comprasAte2) {
                        $arrayFinal[] = $arrayDados['entregaem2'];
                    } else {
                        if ($horaAtual <= $comprasDe2) {
                            $arrayFinal[] = $arrayDados['entregaem2'];
                        }
                    }

                    if ($horaAtual >= $comprasDe3 && $horaAtual <= $comprasAte3) {
                        $arrayFinal[] = $arrayDados['entregaem3'];
                    } else {
                        if ($horaAtual <= $comprasDe3) {
                            $arrayFinal[] = $arrayDados['entregaem3'];
                        }
                    }
                }

                


            } else {
                $sql .= "regrasretiradas WHERE mercado = {$idMercado} AND {$cepAtual} BETWEEN CAST(REPLACE(faixacepde, '-', '') AS UNSIGNED) AND CAST(REPLACE(faixacepate, '-', '') AS UNSIGNED) ";

                $arrayDados = $objRetirada->getAdapter()->fetchRow($sql);

                //se nao for o dia de hoje, mostro todas opcoes de retirada dos horários
                if ($post['diaHoje'] != $post['dataComparacao']) {

                    if (!empty($arrayDados['retiradaem'])) {
                        $arrayFinal[] = $arrayDados['retiradaem'];
                    }
                    if (!empty($arrayDados['retiradaem2'])) {
                        $arrayFinal[] = $arrayDados['retiradaem2'];
                    }
                    if (!empty($arrayDados['retiradaem3'])) {
                        $arrayFinal[] = $arrayDados['retiradaem3'];
                    }
                    if (!empty($arrayDados['retiradaem4'])) {
                        $arrayFinal[] = $arrayDados['retiradaem4'];
                    }

                // se o dia de hoje é o dia que quero retirada, eu verifico conforme meu horário
                } else {

                    $comprasDe = (double) str_replace(":", ".", $arrayDados['comprasde']);
                    $comprasAte = (double) str_replace(":", ".", $arrayDados['comprasate']);

                    $comprasDe2 = (double) str_replace(":", ".", $arrayDados['comprasde2']);
                    $comprasAte2 = (double) str_replace(":", ".", $arrayDados['comprasate2']);

                    $comprasDe3 = (double) str_replace(":", ".", $arrayDados['comprasde3']);
                    $comprasAte3 = (double) str_replace(":", ".", $arrayDados['comprasate3']);

                    $comprasDe4 = (double) str_replace(":", ".", $arrayDados['comprasde4']);
                    $comprasAte4 = (double) str_replace(":", ".", $arrayDados['comprasate4']);

                    //verifico qual a proxima possivel retirada
                    $mercados[$key]['proximaRetirada'] = "";

                    if ($horaAtual >= $comprasDe && $horaAtual <= $comprasAte) {
                        $arrayFinal[] = $arrayDados['retiradaem'];
                    } else {
                        if ($horaAtual <= $comprasDe) {
                            $arrayFinal[] = $arrayDados['retiradaem'];
                        }
                    }

                    if ($horaAtual >= $comprasDe2 && $horaAtual <= $comprasAte2) {
                        $arrayFinal[] = $arrayDados['retiradaem2'];
                    } else {
                        if ($horaAtual <= $comprasDe2) {
                            $arrayFinal[] = $arrayDados['retiradaem2'];
                        }
                    }

                    if ($horaAtual >= $comprasDe3 && $horaAtual <= $comprasAte3) {
                        $arrayFinal[] = $arrayDados['retiradaem3'];
                    } else {
                        if ($horaAtual <= $comprasDe3) {
                            $arrayFinal[] = $arrayDados['retiradaem3'];
                        }
                    }

                    if ($horaAtual >= $comprasDe4 && $horaAtual <= $comprasAte4) {
                        $arrayFinal[] = $arrayDados['retiradaem4'];
                    } else {
                        if ($horaAtual <= $comprasDe4) {
                            $arrayFinal[] = $arrayDados['retiradaem4'];
                        }
                    }
                }

            }


            if (!empty($arrayFinal)) {
                echo json_encode($arrayFinal);
            } else {
                echo false;
            }

            
        }
    }

    public function feriadosMercadoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $objFeriado = new Application_Model_DbTable_Feriado();
        $post = $this->getRequest()->getPost();

        if (!empty($post['ano']) && !empty($post['mes'])) {
            $arrayListFeriados = $objFeriado->fetchAll("mercado = {$_SESSION['Default']['usuario']['mercado']['id']} AND month(data) = {$post['mes']} AND year(data) = {$post['ano']}");

            $cepAtual = intval(str_replace("-", "", $_SESSION['Default']['usuario']['endereco']['cep']));

            $sql = "SELECT"
            . " m.id "
            . " ,m.nome  "
            . " , LOWER(m.foto) as foto "
            . " ,m.horariodeatendimento "
            
            . " ,r.valor"
            . " ,r.taxacobrada "
            . " ,r.comprasde "
            . " ,r.comprasate "
            . " ,r.entregaem "
            . " ,r.comprasde2 "
            . " ,r.comprasate2 "
            . " ,r.entregaem2 "
            . " ,r.comprasde3 "
            . " ,r.comprasate3 "
            . " ,r.entregaem3 "

            . " ,rr.taxa "
            . " ,rr.comprasde as retcomprasde "
            . " ,rr.comprasate as retcomprasate "
            . " ,rr.retiradaem "
            . " ,rr.comprasde2 as retcomprasde2 "
            . " ,rr.comprasate2 as retcomprasate2 "
            . " ,rr.retiradaem2 "
            . " ,rr.comprasde3 as retcomprasde3 "
            . " ,rr.comprasate3 as retcomprasate3 "
            . " ,rr.retiradaem3 "
            . " ,rr.comprasde4 as retcomprasde4 "
            . " ,rr.comprasate4 as retcomprasate4 "
            . " ,rr.retiradaem4 "
            . " FROM mercados m "
            . " LEFT JOIN regrasentrega r ON (m.id = r.mercado AND {$cepAtual} BETWEEN CAST(REPLACE(r.faixacepde, '-', '') AS UNSIGNED) AND CAST(REPLACE(r.faixacepate, '-', '') AS UNSIGNED) )"
            . " LEFT JOIN regrasretiradas rr ON (m.id = rr.mercado AND {$cepAtual} BETWEEN CAST(REPLACE(rr.faixacepde, '-', '') AS UNSIGNED) AND CAST(REPLACE(rr.faixacepate, '-', '') AS UNSIGNED) )"
            . " WHERE ";

            if (!empty($_SESSION['idMercadoMaster'])) {
                $sql .= " m.ativo in (2,3) ";
            } else {
                $sql .= " m.ativo in (1,2) ";
            }

            $sql .= " AND m.id = {$_SESSION['Default']['usuario']['mercado']['id']} "
            . " AND (r.comprasde is not null OR rr.comprasde is not null) "
            . " GROUP BY m.id ";

            $mercado = $objFeriado->getAdapter()->fetchRow($sql);

            $horaAtual = (double) Date("H.i");

            $comprasDe = (double) str_replace(":", ".", $mercado['comprasde']);
            $comprasAte = (double) str_replace(":", ".", $mercado['comprasate']);

            $comprasDe2 = (double) str_replace(":", ".", $mercado['comprasde2']);
            $comprasAte2 = (double) str_replace(":", ".", $mercado['comprasate2']);

            $comprasDe3 = (double) str_replace(":", ".", $mercado['comprasde3']);
            $comprasAte3 = (double) str_replace(":", ".", $mercado['comprasate3']);

            $mercado['proximaEntrega'] = "";

            //verifico qual a proxima possivel entrega
            if ($horaAtual >= $comprasDe && $horaAtual <= $comprasAte) {
                $mercado['proximaEntrega'] = $mercado['entregaem'];
            }

            if ($horaAtual >= $comprasDe2 && $horaAtual <= $comprasAte2) {
                $mercado['proximaEntrega'] = $mercado['entregaem2'];
            }

            if ($horaAtual >= $comprasDe3 && $horaAtual <= $comprasAte3) {
                $mercado['proximaEntrega'] = $mercado['entregaem3'];
            }

            $comprasDe = (double) str_replace(":", ".", $mercado['retcomprasde']);
            $comprasAte = (double) str_replace(":", ".", $mercado['retcomprasate']);

            $comprasDe2 = (double) str_replace(":", ".", $mercado['retcomprasde2']);
            $comprasAte2 = (double) str_replace(":", ".", $mercado['retcomprasate2']);

            $comprasDe3 = (double) str_replace(":", ".", $mercado['retcomprasde3']);
            $comprasAte3 = (double) str_replace(":", ".", $mercado['retcomprasate3']);

            $comprasDe4 = (double) str_replace(":", ".", $mercado['retcomprasde4']);
            $comprasAte4 = (double) str_replace(":", ".", $mercado['retcomprasate4']);

            $mercado['proximaRetirada'] = "";

            //verifico qual a proxima possivel retirada
            if ($horaAtual >= $comprasDe && $horaAtual <= $comprasAte) {
                $mercado['proximaRetirada'] = $mercado['retiradaem'];
            }

            if ($horaAtual >= $comprasDe2 && $horaAtual <= $comprasAte2) {
                $mercado['proximaRetirada'] = $mercado['retiradaem2'];
            }

            if ($horaAtual >= $comprasDe3 && $horaAtual <= $comprasAte3) {
                $mercado['proximaRetirada'] = $mercado['retiradaem3'];
            }

            if ($horaAtual >= $comprasDe4 && $horaAtual <= $comprasAte4) {
                $mercado['proximaRetirada'] = $mercado['retiradaem4'];
            }

            if (empty($mercado['proximaRetirada']) && empty($mercado['proximaEntrega'])) {
                $arrayListFeriados[] = array(
                    'data' => Date("Y-m-d")
                );
            }

            echo json_encode($arrayListFeriados);
        }
    }

    public function alterarCepAction() {
        $post = $this->getRequest()->getPost();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $objMercado = new Application_Model_DbTable_Mercado();
        $objClienteEndereco = new Application_Model_DbTable_ClienteEndereco();

        if (!empty($post)) {

            $cep = str_replace(".", "", $post['cep']);
            $cep = str_replace("-", "", $cep);
            
            $objCidade = new Application_Model_DbTable_Cidade();
            $dadosCep = $objCidade->getDadosCEP($cep);

            $cepAtual = intval(str_replace("-", "", $post['cep']));

            $sql2 = "";

            if (!empty($this->mercadoMaster['id'])) {
                $sql2 = " AND m.id = {$this->mercadoMaster['id']} ";                
            }

            //verifica se tem mercados que atendem
            $sql = "SELECT"
            . " m.id "
            . " ,m.nome  "
            . " FROM mercados m "
            . " LEFT JOIN regrasentrega r ON (m.id = r.mercado AND {$cepAtual} BETWEEN CAST(REPLACE(r.faixacepde, '-', '') AS UNSIGNED) AND CAST(REPLACE(r.faixacepate, '-', '') AS UNSIGNED) )"
            . " LEFT JOIN regrasretiradas rr ON (m.id = rr.mercado AND {$cepAtual} BETWEEN CAST(REPLACE(rr.faixacepde, '-', '') AS UNSIGNED) AND CAST(REPLACE(rr.faixacepate, '-', '') AS UNSIGNED) )"
            . " WHERE ";

            if (!empty($_SESSION['idMercadoMaster'])) {
                $sql .= " m.ativo in (2,3) ";
            } else {
                $sql .= " m.ativo in (1,2) ";
            }

            $sql .= $sql2
            . " AND (r.comprasde is not null OR rr.comprasde is not null) "
            . " GROUP BY m.id ";

            $mercados = $objMercado->getAdapter()->fetchAll($sql);

            if (!empty($_SESSION['Default']['usuario']['endereco'])) {
                unset($_SESSION['Default']['usuario']['endereco']); 
            }

            if (!empty($mercados) && !empty($dadosCep)) {
                $_SESSION['Default']['usuario']['endereco']['atendimento'] = true;     
            } else {
                $_SESSION['Default']['usuario']['endereco']['atendimento'] = false;     
            }

            if (!empty($_SESSION['Default']['usuario']['carrinho']['idProdutoCep'])) {
                unset($_SESSION['Default']['usuario']['carrinho']['idProdutoCep']); 
            }

            if (!empty($_SESSION['Default']['usuario']['carrinho']['qtProdutoCep'])) {
                unset($_SESSION['Default']['usuario']['carrinho']['qtProdutoCep']); 
            }

            if (!empty($mercados)) {
                $_SESSION['Default']['usuario']['endereco']['cidade'] = $dadosCep[0]['localidade'] . " - " . $dadosCep[0]['estado'];
                $_SESSION['Default']['usuario']['endereco']['rua'] = $dadosCep[0]['abreviado'];
                $_SESSION['Default']['usuario']['endereco']['bairro'] = $dadosCep[0]['bairro'];
                $_SESSION['Default']['usuario']['endereco']['cep'] = $post['cep'];    
                $_SESSION['Default']['usuario']['endereco']['numero'] = "";
                $_SESSION['Default']['usuario']['endereco']['complemento'] = "";

                // verifica se eh um endereco de cliente logado
                if (!empty($_SESSION['Default']['usuario']['id'])) {

                    // pega os dados do endereco do cep que foi pesquisado
                    $qEndereco = $objClienteEndereco->fetchRow("cliente = {$_SESSION['Default']['usuario']['id']} AND cep = '{$post['cep']}'");

                    $_SESSION['Default']['usuario']['endereco']['numero'] = $qEndereco['numero'];
                   $_SESSION['Default']['usuario']['endereco']['complemento'] = $qEndereco['complemento'];
                }
                
            }

            echo json_encode($_SESSION['Default']['usuario']['endereco']);

        }
        
    }

    public function buscaProdutosAction() {
        $post = $this->getRequest()->getPost();
        $dadosPedido['data'] = array();
        $dadosPedido['sugestoes'] = array();

        if (!isset($post['filtro'])) {
            $post['filtro'] = "";
        }

        if (!empty($post['termo'])) {
            $objProduto = new Application_Model_DbTable_Produto();
            $dados = $objProduto->getProdutos(null, null, 20, null, $post['termo'], null, $post['filtro'], null, null, null, $this->mercadoMaster['id']);
            $retorno = array();

            $produtos = "";

            $categorias = array();

            foreach ($dados as $key => $umProduto) {

                $produtos .= "," . $umProduto['id'];

                $umProduto['pathFoto'] = strtolower($umProduto['foto']);
                $umProduto['pathWebp'] = strtolower($umProduto['webp']);
                $umProduto['precoMin'] = number_format($umProduto['precoMin'], 2, ',', '.');
                $umProduto['precoMax'] = number_format($umProduto['precoMax'], 2, ',', '.');

                $retorno[$key] = $umProduto;

                if (!empty($umProduto['nomeCategoriaPai3'])) {
                    //$categoriaDoProduto['nome'] = $umProduto['nomeCategoriaPai3'];
                    //$categoriaDoProduto['link'] = $umProduto['linkCategoriaPai3'];
                    //$categorias[] = $categoriaDoProduto;
                }
                
                if (!empty($umProduto['nomeCategoriaPai2'])) {
                    //$categoriaDoProduto['nome'] = $umProduto['nomeCategoriaPai2'];
                    //$categoriaDoProduto['link'] = $umProduto['linkCategoriaPai2'];
                    //$categorias[] = $categoriaDoProduto;
                }

                if (!empty($umProduto['nomeCategoriaPai'])) {
                    $categoriaDoProduto['nome'] = $umProduto['nomeCategoriaPai'];
                    $categoriaDoProduto['link'] = $umProduto['linkCategoriaPai'];
                    $categorias[] = $categoriaDoProduto;
                }


            }

            $dadosPedido['data'] = $categorias;
            $dadosPedido['sugestoes'] = $retorno;
        }

        echo json_encode($dadosPedido);

    }

    public function carregaProdutosAction() {
        $post = $this->getRequest()->getPost();
        $dadosPedido['data'] = array();

        $possoVerTotal = 20;

        if (!empty($post)) {

            $objProduto = new Application_Model_DbTable_Produto();
            $objCategoria = new Application_Model_DbTable_Categoria();
            $retorno = array();

            if ($post['tipo'] == 1) {

                $categorias = $objCategoria->fetchAll("id = {$post['categoria']}");

                foreach ($categorias as $umaCategoria) {
                    $key = 0;
                    $categorias[$key]['produtos'] = array();
                    $totalProdutos = 0;
                    

                    $qProdutos = $objProduto->getProdutos($umaCategoria['id'], null, $possoVerTotal - $totalProdutos, $post['produtosLista'], null, $post['filtro'], null, null, null, null, $this->mercadoMaster['id']);

                    if (!empty($qProdutos)) {
                        foreach ($qProdutos as $umProduto) {
                            $umProduto['pathFoto'] = strtolower($umProduto['foto']);
                            $umProduto['pathWebp'] = strtolower($umProduto['webp']);
                            $umProduto['precoMin'] = number_format($umProduto['precoMin'], 2, ',', '.');
                            $umProduto['precoMax'] = number_format($umProduto['precoMax'], 2, ',', '.');
                            $retorno[$key] = $umProduto;
                            $key++;
                        }
                    }

                    $totalProdutos = count($retorno);

                    if ($key >= $possoVerTotal) {
                        continue;
                    }

                    $sql = "SELECT 
                                c.id as idFilha, 
                                c.nome as nomeFilha,
                                c2.id as idSubFilha,
                                c2.nome as nomeSubFilha
                            FROM 
                                categorias c 
                            LEFT JOIN
                                categorias c2 ON (c2.categoria = c.id)
                            WHERE 
                                c.categoria = {$umaCategoria['id']}";

                    $qSubCategorias = $objCategoria->getAdapter()->fetchAll($sql);

                    foreach ($qSubCategorias as $umaSub) {

                        $qProdutos = $objProduto->getProdutos($umaSub['idFilha'], null, $possoVerTotal - $totalProdutos, $post['produtosLista'], null, $post['filtro'], null, null, null, null, $this->mercadoMaster['id']);

                        if (!empty($qProdutos)) {
                            foreach ($qProdutos as $umProduto) {
                                $umProduto['pathFoto'] = strtolower($umProduto['foto']);
                                $umProduto['pathWebp'] = strtolower($umProduto['webp']);
                                $umProduto['precoMin'] = number_format($umProduto['precoMin'], 2, ',', '.');
                                $umProduto['precoMax'] = number_format($umProduto['precoMax'], 2, ',', '.');
                                $retorno[$key] = $umProduto;
                                $key++;
                            }
                        }

                        $totalProdutos = count($retorno);

                        if ($key >= $possoVerTotal) {
                            break;
                        }

                        if (!empty($umaSub['idSubFilha'])) {
                            $qProdutos = $objProduto->getProdutos($umaSub['idSubFilha'],null,  $possoVerTotal - $totalProdutos, $post['produtosLista'], null, $post['filtro'], null, null, null, null, $this->mercadoMaster['id']);                    

                            if (!empty($qProdutos)) {
                                foreach ($qProdutos as $umProduto) {
                                    $umProduto['pathFoto'] = strtolower($umProduto['foto']);
                                    $umProduto['pathWebp'] = strtolower($umProduto['webp']);
                                    $umProduto['precoMin'] = number_format($umProduto['precoMin'], 2, ',', '.');
                                    $umProduto['precoMax'] = number_format($umProduto['precoMax'], 2, ',', '.');
                                    $retorno[$key] = $umProduto;
                                    $key++;
                                }
                            }

                            $totalProdutos = count($retorno);

                            if ($key >= $possoVerTotal) {
                                break;
                            }
                        }
                    }

                }


            } else {

                if (empty($post['marcas'])) {
                    $post['marcas'] = "";
                }

                if (empty($post['filtros'])) {
                    $post['filtros'] = "";
                }

                if (empty($post['termoBusca'])) {
                    $post['termoBusca'] = "";
                }

                if (!isset($post['limite'])) {
                    $post['limite'] = $possoVerTotal;
                }

                if (empty($post['subcategorias'])) {
                    $post['subcategorias'] = [];
                } else {
                    $post['subcategorias'] = explode(",",$post['subcategorias']);
                }

                $dados = $objProduto->getProdutos($post['categoria'], null, $post['limite'], $post['produtosLista'], null, $post['filtro'], $post['marcas'], $post['filtros'], $post['subcategorias'], $post['termoBusca'], $this->mercadoMaster['id']);
                foreach ($dados as $key => $umProduto) {
                    $umProduto['pathFoto'] = strtolower($umProduto['foto']);
                    $umProduto['pathWebp'] = strtolower($umProduto['webp']);
                    $umProduto['precoMin'] = number_format($umProduto['precoMin'], 2, ',', '.');
                    $umProduto['precoMax'] = number_format($umProduto['precoMax'], 2, ',', '.');
                    $retorno[$key] = $umProduto;
                }

            }

            $dadosPedido['data'] = $retorno;
            
        }

        echo json_encode($dadosPedido);
    }

    public function buscaCepAction() {
        $post = $this->getRequest()->getPost();    
        
        if (!empty($post)) {
            $cep = str_replace(".", "", $post['cep']);
            $cep = str_replace("-", "", $cep);
            
            $objCidade = new Application_Model_DbTable_Cidade();
            $dadosCep = $objCidade->getDadosCEP($cep);

            echo json_encode($dadosCep);

        }
    }

    public function buscaCidadesAction() {
        $post = $this->getRequest()->getPost();
        if (!empty($post)) {
            $idEstado = $post['idEstado'];
            $idCidade = "";
            if (isset($post['idCidade'])) {
                $idCidade = $post['idCidade'];
            }

            $objCidades = new Application_Model_DbTable_Cidade();
            //$arrayListCidades = $objCidades->fetchAll("", "cidade ASC");
            $arrayListCidades = $objCidades->getAdapter()->fetchAll("SELECT * FROM enderecos_cidades WHERE idEstado = {$idEstado} GROUP BY cidade ORDER BY cidade ASC");
            if (!empty($arrayListCidades)) {
                $html[] = '<option value="">Selecione</option>';
                foreach ($arrayListCidades as $umaCidade) {
                    $selected = "";

                    if (strlen($idCidade) && $idCidade == $umaCidade['id']) {
                        $selected = "selected";
                    }

                    $html[] = '<option value="' . $umaCidade['id'] . '" '.$selected.' >' . $umaCidade['cidade'] . '</option>';
                }
                echo join("\n", $html);
            }
        }
    }

//    public function processarCartaoCreditoAction() {
//        $post = $this->getRequest()->getPost();
//        if (!empty($post)) {
//            error_log("ajax");
//            $arrayCartao = array(
//               'idPedido' => 123,
//               'cliente' => "nome",
//               'titular' => $post['owner'],
//               'bandeira' => $post['bandeira'],
//               'valor' => 12.36,
//               'numeroCartao' => $post['cardNumber'],
//               'cvv' => $post['cvv'],
//               'validade' => $post['validade'],
//           );
//
//           //efeuta o pagamento, se cartão de crédito
////           if($post['forma_pagamento'] === "35"){
//               $objCielo = new Application_Model_Cielo();
//               $retorno = $objCielo->processarPagamentoCartaoCredito($arrayCartao);
////           }
//            
//            echo $retorno;
//        }
//    }
    
     public function salvarPedidoAction() {
        $objCliente = new Application_Model_DbTable_Cliente();
        $objMercado = new Application_Model_DbTable_Mercado();
        $objPedido = new Application_Model_DbTable_Pedido();
        $objProdutoMercado = new Application_Model_DbTable_ProdutoMercado();
        $objPedidoItens = new Application_Model_DbTable_PedidoItem();
        $objClienteEndereco = new Application_Model_DbTable_ClienteEndereco();
        $objRecuperacaoCarrinho = new Application_Model_DbTable_RecuperacaoCarrinho();
        
        $post = $this->getRequest()->getPost();

        $this->view->mercado = $_SESSION['Default']['usuario']['mercado'];
        $cepAtual = str_replace("-", "", $_SESSION['Default']['usuario']['endereco']['cep']);
        
        if (!empty($post)) {

            //verifica campos
            $validacao = new Application_Model_Validacao();
            $arrayListValidacao = array(
                'NotEmpty' => array(
                    'nome' => array('Nome', $post['user']),
                    'email' => array('E-Mail', $post['email']),
                    'cpf' => array('CPF', $post['cpf']),
                    'queroReceber' => array('Quer Receber', $post['queroReceber']),
                    'forma_pagamento' => array('Forma de Pagamento', $post['forma_pagamento'])
                )
            );

            $validacao->check($arrayListValidacao);
            $erros = $validacao->getErros();
            
            if (empty($erros)) {
                //verifica pra evitar duplicidade de cpf de cliente
                $dadosCliente = $objCliente->fetchRow("email = '{$post['email']}'");

                $arrayCliente = array(
                    'nome' => $post['user'],
                    'email' => $post['email'],
                    'cpf' => $post['cpf'],
                    'telefone' => $post['phone'],
                    'celular' => $post['celular']
                );

                //para novos usuários com senha
                if (!empty($post['pass'])) {
                    $arrayCliente['senha'] = hash('sha512', $post['pass']);
                }

                //verifica se é um cliente ou não e salva ou adiciona novo registro de cliente
                if (!empty($dadosCliente)) {
                    $idCliente = $dadosCliente['id'];
                    $objCliente->save($arrayCliente,"id = {$idCliente}");
                } else {
                    $objCliente->save($arrayCliente);
                    $idCliente = $objCliente->getAdapter()->lastInsertId();

                    // manda email de boas vindas
                    $front = Zend_Controller_Front::getInstance()->getBaseUrl();
                    $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;
                    $baseUrl2 = 'http://'. $_SERVER['HTTP_HOST'] . $front;
                    $baseUrl2 = str_replace("public","", $baseUrl2);

                    $mail = new Zend_Mail('utf-8');

                    $mail->setSubject("Bem vindo ao ".$this->mercadoMaster['nome']);

                    if (!empty($this->mercadoMaster['foto'])) {
                        $mail->setFrom('mercadofiqueemcasa@gmail.com', $this->mercadoMaster['nome']);
                    } else {
                        $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");
                    }
                      
                      $html = array();
                      $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
                      $html[] = '<tr>';
                      $html[] = '<td width="600" align="center">';
                      $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
                      $html[] = '<tr>';
                      $html[] = '<td height="120" bgcolor="#022e6d" align="center" valign="center"><p>';
                    if (!empty($this->mercadoMaster['foto'])) {
                      $html[] = '<img src="' . $baseUrl2 . '/admin/public/uploads/mercados/'.$this->mercadoMaster['foto'].'" />';      
                    } else {
                      $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
                    }
                      $html[] = '</p></td>';
                      $html[] = '</tr>';
                      $html[] = '<tr>';
                      $html[] = '<td bgcolor="#FFFFFF" valign="center">';
                      $html[] = '<p><font face="verdana" size="3" color="#000000">';
//                      $html[] = 'Bem vindo ao Mercado Fique Em Casa<br /><br />';
//                      $html[] = 'A partir de agora você poderá fazer suas compras em diversos supermercados, sem sair de casa. Com poucos cliques você escolhe seus produtos e envia seu pedido.<br /><br /> ';
//                      $html[] = 'O supermercado que receber o pedido fará a separação de seus produtos com todo cuidado e deixará pronto para a equipe de logística fazer a entrega onde desejar ou para você passar no supermercado para fazer a retirada. <br /><br />';
//                      $html[] = 'O processo é bem simples e rápido, saiba que a maioria dos supermercados fazem a entrega no mesmo dia de sua compra e você pode pagar somente quando receber a compra! <br /><br />';
                      $html[] = 'Seja Muito Bem vindo ao '.$this->mercadoMaster['nome'].'!<br /><br />';
                    $html[] = 'Nós existimos para garantir segurança e conforto a você e sua família durante e depois a pandemia que está rolando. <br /><br /> ';
                    $html[] = 'Gostariamos muito de mudar o nosso nome para "Mercado Vida Fácil" e nos comprometemos a fazer isso, assim que tivermos melhores noticias sobre a situação.  <br /><br />';
                    $html[] = 'Até lá, prometemos fazer de tudo para abastecer sua casa sem que você tenha que se expor a aglomerações desnecessárias, e sem ter que perder tempo indo e vindo ao mercado.   <br /><br />';
                    $html[] = 'Em caso de dúvidas, estamos a disposição, conta com a gente por meio do Whatsapp ou E-mail. <br /><br />';
                    $html[] = 'Equipe Mercado Fique Em Casa. <br /><br />';
                      $html[] = '<a href="'.$baseUrl.'">Quero fazer uma compra</a><br />';
                      $html[] = '</p>';
                      $html[] = '</td>';
                      $html[] = '</tr>';
                      $html[] = '<tr>';
                      $html[] = '<td height="30" bgcolor="#022e6d" align="center" valign="center">';
                      $html[] = '<p>&nbsp;</p>';
                      $html[] = '</td>';
                      $html[] = '</tr>';
                      $html[] = '</table>';
                      $html[] = '</td>';
                      $html[] = '</tr>';
                      $html[] = '</table>';

                      $mail->setBodyHtml(join("\n", $html));
                      $mail->addTo($post['email'], $post['email']);

                      $objConfigMail = new Application_Model_ConfigMail();
                      $transport = $objConfigMail->get_configuracao_mail();
                      $mail->send($transport);
                }

                $enderecoAtual = $_SESSION['Default']['usuario']['endereco'];

                //pega dados do cliente novo
                $dadosCliente = $objCliente->fetchRow("email = '{$post['email']}'");

                if ($dadosCliente) {

                    //verifica se já tenho um endereço com esse cep cadastrado
                    $qEndereco = $objClienteEndereco->fetchRow("cep = '{$enderecoAtual['cep']}' AND cliente = {$idCliente}");

                    $dadosCidadeEstado = explode(' - ',$enderecoAtual['cidade']);

                    $qCidade = $objClienteEndereco->getAdapter()->fetchRow("
                        SELECT
                            c.id
                        FROM
                            enderecos_cidades c,
                            enderecos_estados e
                        WHERE
                            c.cidade = '{$dadosCidadeEstado[0]}'
                            AND
                            e.uf = '{$dadosCidadeEstado[1]}'
                            AND
                            c.idEstado = e.id
                    ");

                    //se for entrega, verifica para já adicionar o endereço na lista do usuário
                    if (empty($qEndereco)) {
                        if ($post['queroReceber'] == 1) {
                            $arrayEndereco = array(
                                'cep' => $enderecoAtual['cep'],
                                'rua' => $enderecoAtual['rua'],
                                'cliente' => $idCliente,
                                'numero' => $post['numero'],
                                'complemento' => $post['complemento'],
                                'bairro' => $enderecoAtual['bairro'],
                                'cidade' => $qCidade['id']
                            );
                            $objClienteEndereco->save($arrayEndereco);
                        } else {
                            $arrayEndereco = array(
                                'cep' => $enderecoAtual['cep'],
                                'rua' => $enderecoAtual['rua'],
                                'cliente' => $idCliente,
                                'numero' => '',
                                'complemento' => '',
                                'bairro' => $enderecoAtual['bairro'],
                                'cidade' => $qCidade['id']
                            );
                            $objClienteEndereco->save($arrayEndereco);
                        }
                    }

//                    $dbAdapter = Zend_Db_Table::getDefaultAdapter();
//                    $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
//                    $authAdapter->setAmbiguityIdentity(true);
//                    
//
//                    //valida base do mercado
//                    $authAdapter->setTableName('clientes')
//                      ->setIdentityColumn('email')
//                      ->setCredentialColumn('senha');
//
//                    $authAdapter->setIdentity($dadosCliente['email'])->setCredential($dadosCliente['senha']);
//
//                    $auth = Zend_Auth::getInstance();
//                    $autenticado = $auth->authenticate($authAdapter);
//                    
//                    if (!empty($autenticado) && $autenticado->isValid()) {
//                        $objUsuarioAutenticado = $authAdapter->getResultRowObject(null, 'senha');
//                        
//                        $storage = $auth->getStorage();
//                        $storage->write($storage);
//
//                        $sessaoUser = new Zend_Session_Namespace();
//                        $sessaoUser->usuario = array(
//                        'id' => $objUsuarioAutenticado->id,
//                        'nome' => $objUsuarioAutenticado->nome,
//                        'email' => $objUsuarioAutenticado->email
//                        );
//                        
//                        $objSessionNamespace = new Zend_Session_Namespace();
//                        $objSessionNamespace->setExpirationSeconds(86400);
//
//                        $_SESSION['Default']['usuario']['admin'] = false;
//                        $_SESSION['Default']['usuario']['dados'] = $objCliente->getDadosCliente($objUsuarioAutenticado->id);
//                        $_SESSION['Default']['usuario']['endereco'] = $enderecoAtual;
//                    }
                }

                //se for retirada nao adiciona a taxa cobrada porque é retirada
                if ($post['queroReceber'] == 2) {
                    $taxa = 0;
                } else {
                    $taxa = $this->view->mercado['taxacobrada'];
                }
                
                if(empty($post['hora_retirada'])){
                    $horaretirada = null;
                }else{
                    $horaretirada = $post['hora_retirada'];
                }
                
                //adiciona dados do pedido
                $arrayPedido = array(
                    'idCliente' => $dadosCliente['id'],
                    'idMercado' => $this->view->mercado['id'],
                    'status' => 1,
                    'valortotal' => $this->view->mercado['totalProdutos'],
                    'taxacobrada' => $taxa,
                    'statuspagamento' => 0,
                    'tipoentrega' => $post['queroReceber'],
                    'horaentrega' => $post['hora_entrega'],
                    'horaretirada' => $horaretirada,
                    'formapagamento' => $post['forma_pagamento'],
                    'numero' => $post['numero'],
                    'complemento' => $post['complemento'],
                    'bairro' => $_SESSION['Default']['usuario']['endereco']['bairro'],
                    'troco' => $post['troco'],
                    'cep' => $_SESSION['Default']['usuario']['endereco']['cep'],
                    'endereco' => $_SESSION['Default']['usuario']['endereco']['rua'],
                    'cidade' => $_SESSION['Default']['usuario']['endereco']['cidade']

                );

                
                if (!empty($post['dia_entrega'])) {
                    $diaEntrega = join('-', array_reverse(explode('/', $post['dia_entrega'])));
                    $arrayPedido['dataentregaretirada'] = $diaEntrega;
                } else {
                    $diaRetirada = join('-', array_reverse(explode('/', $post['dia_retirada'])));
                    $arrayPedido['dataentregaretirada'] = $diaRetirada;
                }

                $arrayPedido['outrapessoa'] = 0;

                if (!empty($post['outro_nome'])) {
                    $arrayPedido['outrapessoanome'] = $post['outro_nome'];
                    $arrayPedido['outrapessoa'] = 1;
                }

                if (!empty($post['outro_telefone'])) {
                    $arrayPedido['outrapessoatelefone'] = $post['outro_telefone'];
                }

                if (!empty($post['outro_rg'])) {
                    $arrayPedido['outrapessoarg'] = $post['outro_rg'];
                }

                $objPedido->save($arrayPedido);
                $idPedido = $objPedido->getAdapter()->lastInsertId();

                //verifica se esse cliente tem alguma recuperacao pendente
                $objRecuperacaoCarrinho->save(array("pedido" => $idPedido),"cliente = {$dadosCliente['id']} AND pedido is null");
                
                //loop pelos itens
                $values = $this->view->mercado['produtos'];
//                foreach ($this->view->mercado['produtos'] as $umProduto) {
                if (is_array($values) || is_object($values)){
                    foreach ($values as $umProduto)
                    {

                        // só faz uma verificacao para nao duplicar itens
                        $dadoItenPedido = $objPedidoItens->fetchRow("idPedido = {$idPedido} AND idProduto = {$umProduto['id']}");

                        if (empty($dadoItenPedido)) {
                            $arrayProduto = array(
                                'idProduto' => $umProduto['id'],
                                'idPedido' => $idPedido,
                                //'preco' => $umProduto['valorMercado']['preco'],
                                'quantidade' => $umProduto['quantidade']
                            );

                            if ($umProduto['tipo'] == 0) {
                                $arrayProduto['preco'] = $umProduto['valorMercado'] / 1000;
                            } else {
                                $arrayProduto['preco'] = $umProduto['valorMercado'];
                            }

                            $objPedidoItens->save($arrayProduto);
                        } else {
                            $arrayProduto = array(
                                'quantidade' => $umProduto['quantidade'] + $dadoItenPedido['quantidade']
                            );

                            $objPedidoItens->save($arrayProduto, "id = {$dadoItenPedido['id']}");
                        }

                        $dadoItenPedido = $objPedidoItens->fetchRow("idPedido = {$idPedido} AND idProduto = {$umProduto['id']}");

                        // remove estoque do item no mercado
                        $qEstoqueAtual = $objProdutoMercado->fetchRow("idProduto = {$umProduto['id']} AND idMercado = {$this->view->mercado['id']}");

                        if (!empty($qEstoqueAtual)) {
                            $arrayProduto = array(
                                'estoque' => $qEstoqueAtual['estoque'] - $dadoItenPedido['quantidade']
                            );
                            $objProdutoMercado->save($arrayProduto, "id = {$qEstoqueAtual['id']}");
                        }
                    }
                }                

//                $dadosPedido = $objPedido->getDadosPedido($idPedido);
                
                $valor = $arrayPedido['valortotal'] + $arrayPedido['taxacobrada'];
                
                
                $retorno = "1";
                $validade = $post['mes_validade']."/".$post['ano_validade'];
                //efeuta o pagamento, se cartão de crédito ou boleto do mercado 19
                if($post['forma_pagamento'] === "35"){
                    $arrayCartao = array(
                        'idPedido' => $idPedido,
                        'cliente' => $dadosCliente['nome'],
                        'titular' => $post['owner'],
                        'bandeira' => $post['bandeira'],
                        'valor' => $valor,
                        'numeroCartao' => $post['cardNumber'],
                        'cvv' => $post['cvv'],
                        'validade' => $validade,
                    );
                    
                    $objCielo = new Application_Model_Cielo();
                    $retorno = $objCielo->processarPagamentoCartaoCredito($arrayCartao);
                    error_log("RETORNO ".$retorno);
                    error_log("RETORNO 2 ". substr($retorno, 0, strpos($retorno, ";")));
                    if(substr($retorno, 0, strpos($retorno, ";")) === "1"){
        //              finaliza o pedido
                        $arrayAtualizaPedido = array(
                            'status' => 1,
                            'statuspagamento' => 1
                        );
                        $objPedido->save($arrayAtualizaPedido, "id = {$idPedido}");

//                        $_SESSION['Default']['usuario']['pedidoFinalizado'] = $dadosPedido;

//                        unset($_SESSION['Default']['usuario']['carrinho']);
//                        unset($_SESSION['Default']['usuario']['mercado']);
//                        $this->_redirect('/carrinho/fim');
                    }else{
                        $arrayAtualizaPedido = array(
                            'status' => 6
                        );
                        $objPedido->save($arrayAtualizaPedido, "id = {$idPedido}");
                        
                        
                    }
                }else if($post['forma_pagamento'] === "36"){
                    $dadosCliente = $objCliente->fetchRow("email = '{$post['email']}'");
                    $arrayBoleto = array(
                        'descricao' => "Boleto Referente ao seu Pedido nº ".$idPedido." no Mercado Fique Em Casa",
                        'id' => $dadosCliente['id'],
                        'nome' => $dadosCliente['nome'],
                        'email' => $dadosCliente['email'],
                        'telefone' => $dadosCliente['telefone'],
                        'cpf' => $dadosCliente['cpf'],
                        'cep' => $arrayPedido['cep'],
                        'rua' => $arrayPedido['endereco'],
                        'numero' => $arrayPedido['numero'],
                        'cidade' => $arrayPedido['cidade'],
                        'bairro' => $arrayPedido['bairro'],
                        'valor' => $valor
                    );
                    
//                    $valor = str_replace(".", "", $valor);
//                    $valor = str_replace(",", ".", $valor);
                    error_log("VALOR ".$valor);
//                    $boletoFacil = new Application_Model_BoletoFacil("A40982DE6FC39081B34BC9845126A14100781991FD004AFFC7679FA892A5C1C7", true);
                    $boletoFacil = new Application_Model_BoletoFacil("8F5649EFCA91C16953E45F326B14D6D2755EFDC837F379DFBDB6BDFF3B05A91E", false);
                    $boletoFacil->createCharge($arrayBoleto);
//                    $boletoFacil->createCharge($dadosCliente['nome'], $dadosCliente['cpf'], "Boleto Referente Compra no Mercado Fique Em Casa", $valor);
                    $retorno = $boletoFacil->issueCharge();
//                    error_log("RETORNO BF ".$retorno['charges']);
//                    $obj = new Application_Model_BoletoFacil();
//                    $retorno = $obj->gerarBoleto($arrayBoleto);
                    if (strpos($retorno, 'https') !== false) {
                        $arrayAtualizaPedido = array(
                            'status' => 8,
                            'linkBoleto' => $retorno
                        );
                    }else{
                        $arrayAtualizaPedido = array(
                            'status' => 6
                        );
                    }
                    
                    $objPedido->save($arrayAtualizaPedido, "id = {$idPedido}");
//
//                    unset($_SESSION['Default']['usuario']['carrinho']);
//                    unset($_SESSION['Default']['usuario']['mercado']);
//                    $this->_redirect('/carrinho/fim-boleto');
                }else{
//                    $_SESSION['Default']['usuario']['pedidoFinalizado'] = $dadosPedido;
//
//                    unset($_SESSION['Default']['usuario']['carrinho']);
//                    unset($_SESSION['Default']['usuario']['mercado']);
//                    $this->_redirect('/carrinho/fim');
                }
                
                echo $idPedido.";".$retorno;
            }else{
                echo "erros";
            }
        }
    }
    
    public function enviarBoletoEmailAction() {
        $post = $this->getRequest()->getPost();
        if (!empty($post)) {
            $urlBoleto = $post['urlBoleto'];
            $email = $post['email'];
            $nomeCliente = $post['nomeCliente'];
            
            $mail = new Zend_Mail('utf-8');
            $mail->setSubject("Boleto Compra ". $this->mercadoMaster['nome']);

            if (!empty($this->mercadoMaster['nome'])) {
                $mail->setFrom('mercadofiqueemcasa@gmail.com', $this->mercadoMaster['nome']);
            } else {
                $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");
            }
            
            $html = array();
            $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
            $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
            $html[] = '<tr>';
            $html[] = '<td width="600" align="center">';
            $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
            $html[] = '<tr>';
            $html[] = '<td height="90" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';

            if (!empty($this->mercadoMaster['foto2'])) {
                $html[] = '<img src="' . $baseUrl2 . '/admin/public/uploads/mercados/'.$this->mercadoMaster['foto2'].'" width="200" />';      
              } else {
                $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
              }

            $html[] = '</td>';
            $html[] = '</tr>';
            $html[] = '<tr>';
            $html[] = '<tr><td><table style="width:540px!important;" align="center">';
            $html[] = '<tr><td bgcolor="#FFFFFF" valign="center">';
            $html[] = '<p style="color:#666666!important;font-size:14px;">';
            $html[] = 'Olá '.$dadosUsuario['nome'].', <br><br> Obrigado por nós dar o privilégio de abastecer sua casa ❤<br /><br />';
            $html[] = 'Falta só um último esforço! <br><br> No botão abaixo você pode visualizar o boleto do seu pedido, basta efetuar o pagamento e assim que ele for confirmado em nosso sistema começaremos a separação dos produtos.<br /><br />
            <a style="background:#71a505!important;color:#ffffff!important;display:block!important;height:40px!important;line-height:40px!important;text-decoration:none!important;width:200px!important;text-align:center!important;margin:0 auto!important;" href="'.$urlBoleto.'">Gerar Boleto</a>
            <br />Obs.: por conta da situação atual, boletos demoram até 48 horas para dar baixa, então segura firme, já já estaremos te entregando suas compras =)';
            $html[] = '</p>';
            $html[] = '</td>';
            $html[] = '</tr></table>';
            $html[] = '</td></tr>';
                  $html[] = '<tr>';
            $html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
            $html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">'.$baseUrl.'</a>';
            $html[] = '</td>';
            $html[] = '</tr>';
            $html[] = '</table>';
            $html[] = '</td>';
            $html[] = '</tr>';
            $html[] = '</table>';
            $html[] = '</table></td></tr>';
            
//            $html = array();
//            $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
//            $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
//            $html[] = '<tr>';
//            $html[] = '<td width="600" align="center">';
//            $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
//            $html[] = '<tr>';
//            $html[] = '<td height="80" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
//            $front = Zend_Controller_Front::getInstance()->getBaseUrl();
//            $baseUrl = 'https://'. $_SERVER['HTTP_HOST'] . $front;
//            $baseUrl2 = 'https://'. $_SERVER['HTTP_HOST'] . $front;
//            $baseUrl2 = str_replace("public","", $baseUrl2);
//            if (!empty($this->mercadoMaster['foto'])) {
//              $html[] = '<img src="' . $baseUrl2 . '/admin/public/uploads/mercados/'.$this->mercadoMaster['foto'].'" />';      
//            } else {
//              $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
//            }
//            $html[] = '</td>';
//            $html[] = '</tr>';
//            $html[] = '<tr><td><table style="width:540px!important;" align="center">';
//            $html[] = '<tr><td bgcolor="#FFFFFF" valign="center" colspan="4">';
//            $html[] = '<p style="color:#666666!important;font-size:20px!important;">';
//            $html[] = 'Olá '.$nomeCliente.', <br><br> Obrigado por nós dar o privilégio de abastecer sua casa ❤ <br/><br>';
//            $html[] = 'Falta só um último esforço! <br/><br>';
//            $html[] = 'No botão abaixo você pode visualizar o boleto do seu pedido, basta efetuar o pagamento e assim que ele for confirmado em nosso sistema começaremos a separação dos produtos. <br><br>';
//            $html[] = 'Obs.: por conta da situação atual, boletos demoram até 48 horas para dar baixa, então segura firme, já já estaremos te entregando suas compras =) <br/>';
//            $html[] = '<a href="'.$urlBoleto.'">Gerar Boleto</a><br />';
//            $html[] = '</p>';
//            $html[] = '</td>';
//            $html[] = '</tr>';
//            $html[] = '<tr>';
//            $html[] = '<td height="30" bgcolor="#022e6d" align="center" valign="center">';
//            $html[] = '<p>&nbsp;</p>';
//            $html[] = '</td>';
//            $html[] = '</tr>';
//            $html[] = '</table>';
//            $html[] = '</td>';
//            $html[] = '</tr>';
//            $html[] = '</table>';
            
            if (!empty($this->mercadoMaster['nome'])) {
                $from = $this->mercadoMaster['nome'];
            } else {
                $from = "Mercado Fique Em Casa";
            }
                        
            $mail->setBodyHtml(join("\n", $html));
            $mail->addTo($email, $email);
            
            $objConfigMail = new Application_Model_ConfigMail();
            $transport = $objConfigMail->get_configuracao_mail();
            $mail->send($transport);
        
        }
    }
}