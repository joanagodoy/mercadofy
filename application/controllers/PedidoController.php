<?php

class Default_PedidoController extends PainelBW_Painel {

    public function init() {
    	Zend_Auth::getInstance()->hasIdentity();
        parent::init();
    }

    public function indexAction() {
        $this->_redirect('/usuario/');
    }

    public function visualizarAction() {
    	$id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();
        $objPedido = new Application_Model_DbTable_Pedido();
        $objPedidoItem = new Application_Model_DbTable_PedidoItem();
        $objStatus = new Application_Model_DbTable_PedidoStatus();

        if (!empty($id)) {
            $this->view->id = $id;

            $dadosUsuario = $objPedido->getDadosPedido($id, $_SESSION['Default']['usuario']['id']);

            if ($dadosUsuario['status']['id'] == 1) {
                $dadosUsuario['statusClasse'] = "recebido";
            } else if ($dadosUsuario['status']['id'] == 2) {
                $dadosUsuario['statusClasse'] = "separacao";
            } else if ($dadosUsuario['status']['id'] == 3) {
                $dadosUsuario['statusClasse'] = "pronto";
            } else if ($dadosUsuario['status']['id'] == 4) {
                $dadosUsuario['statusClasse'] = "pronto";
            } else if ($dadosUsuario['status']['id'] == 5) {
                $dadosUsuario['statusClasse'] = "entregue";
            } else if ($dadosUsuario['status']['id'] == 6) {
                $dadosUsuario['statusClasse'] = "cancelado";
            }

            if (!empty($dadosUsuario)) {
                $this->view->dadosPedido = $dadosUsuario;
            } else {
                $this->_redirect('/pedido/');
            }

        }

    }


}
