<?php

class Default_IndexController extends PainelBW_Painel {

    public function init() {
        Zend_Auth::getInstance()->hasIdentity();
        parent::init();
    }

    public function adicionarNewsletterAction() {
        $post = $this->getRequest()->getPost();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if (!empty($post) && !empty($post['email_news'])) {

            $objNews = new Application_Model_DbTable_Newsletter();

            $qVerifica = $objNews->fetchAll("email = '{$post['email_news']}'");

            if (empty($qVerifica)) {
                $dadosUsuario = array(
                    'email' => $post['email_news']
                );

                $objNews->save($dadosUsuario);

                $_SESSION['msgNews'] = "Cadastro realizado com sucesso.";
            }
        }
        $this->_redirect('/');
    }
    
    public function alterarCepAction() {
        $post = $this->getRequest()->getPost();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $objMercado = new Application_Model_DbTable_Mercado();
        $objCidade = new Application_Model_DbTable_Cidade();

        if (!empty($post)) {

            $cep = str_replace(".", "", $post['cep']);
            $cep = str_replace("-", "", $cep);

            $dadosCep = $objCidade->getDadosCEP($cep);
            $cepAtual = str_replace("-", "", $post['cep']);

            //verifica se tem mercados que atendem
            $sql = "SELECT"
            . " m.id "
            . " ,m.nome  "
            . " FROM mercados m "
            . " LEFT JOIN regrasentrega r ON (m.id = r.mercado AND {$cepAtual} BETWEEN CAST(REPLACE(r.faixacepde, '-', '') AS UNSIGNED) AND CAST(REPLACE(r.faixacepate, '-', '') AS UNSIGNED) )"
            . " LEFT JOIN regrasretiradas rr ON (m.id = rr.mercado AND {$cepAtual} BETWEEN CAST(REPLACE(rr.faixacepde, '-', '') AS UNSIGNED) AND CAST(REPLACE(rr.faixacepate, '-', '') AS UNSIGNED) )"
            . " WHERE ";

            if (!empty($_SESSION['idMercadoMaster'])) {
                $sql .= " m.ativo in (2,3) ";
            } else {
                $sql .= " m.ativo in (1,2) ";
            }

            $sql .= " AND (r.comprasde is not null OR rr.comprasde is not null) "
            . " GROUP BY m.id ";

            $mercados = $objMercado->getAdapter()->fetchAll($sql);

            if (!empty($dadosCep)) {
                $_SESSION['Default']['usuario']['endereco']['cidade'] = $dadosCep[0]['localidade'] . " - " . $dadosCep[0]['estado'];
                $_SESSION['Default']['usuario']['endereco']['rua'] = $dadosCep[0]['abreviado'];
                $_SESSION['Default']['usuario']['endereco']['cep'] = $post['cep'];  
                if (!empty($mercados)) {
                    $_SESSION['Default']['usuario']['endereco']['atendimento'] = true;     
                } else {
                    $_SESSION['Default']['usuario']['endereco']['atendimento'] = false;     
                }
            }

            if (!empty($post['carrinho'])) {
                $this->_redirect('/carrinho/checkout');
            } else {
                $this->_redirect('/');
            }
        }
        
    }

    public function indexAction() {
    	$post = $this->getRequest()->getPost();
        $objCategoria = new Application_Model_DbTable_Categoria();
        $objProduto = new Application_Model_DbTable_Produto();
        $objRecuperacaoCarrinho = new Application_Model_DbTable_RecuperacaoCarrinho();

        //recupera o ultimo carrinho caso não finalizado do cliente
        if (empty($_SESSION['Default']['usuario']['carrinho']) && !empty($_SESSION['Default']['usuario']['id'])) {
            
            $arrayLista = $objRecuperacaoCarrinho->fetchRow("pedido is null AND cliente = {$_SESSION['Default']['usuario']['id']}");

            if (!empty($arrayLista['dadosCarrinho'])) {
                $dadosCarrinhoAntigo = json_decode($arrayLista['dadosCarrinho'], TRUE);

                $_SESSION['Default']['usuario']['carrinho'] =  $dadosCarrinhoAntigo['carrinho'];
                $_SESSION['Default']['usuario']['endereco'] =  $dadosCarrinhoAntigo['endereco'];
            }
        }


        $arrayListCategoriaFinal = array();
        $idsProdutos = "";

        $categorias = $objCategoria->fetchAll("categoria is null OR categoria = 0");

        //loop pelas categorias para pegar produtos também de suas filhas
        foreach ($categorias as $key => $umaCategoria) {
            $categorias[$key]['produtos'] = array();
            $totalProdutos = 0;

            $qProdutos = $objProduto->getProdutos($umaCategoria['id'], null, 4 - $totalProdutos, $idsProdutos, null, null, null, null, null, null, $this->mercadoMaster['id']);

            if (!empty($qProdutos)) {
                //array_push($categorias[$key]['produtos'], $qProdutos);
                foreach ($qProdutos as $umProduto) {
                    $categorias[$key]['produtos'][] = $umProduto;
                    if (empty($idsProdutos)) {
                        $idsProdutos = $umProduto['id'];
                    } else {
                        $idsProdutos .= "," . $umProduto['id'];
                    }
                }
            }

            $totalProdutos = count($categorias[$key]['produtos']);

            if ($totalProdutos >= 4) {
                continue;
            }

            $sql = "SELECT 
                        c.id as idFilha, 
                        c.nome as nomeFilha,
                        c2.id as idSubFilha,
                        c2.nome as nomeSubFilha
                    FROM 
                        categorias c 
                    LEFT JOIN
                        categorias c2 ON (c2.categoria = c.id)
                    WHERE 
                        c.categoria = {$umaCategoria['id']}";

            $qSubCategorias = $objCategoria->getAdapter()->fetchAll($sql);

            foreach ($qSubCategorias as $umaSub) {

                $qProdutos = $objProduto->getProdutos($umaSub['idFilha'], null, 4 - $totalProdutos, $idsProdutos, null, null, null, null, null, null, $this->mercadoMaster['id']);

                if (!empty($qProdutos)) {
                    foreach ($qProdutos as $umProduto) {
                        $categorias[$key]['produtos'][] = $umProduto;
                        if (empty($idsProdutos)) {
                            $idsProdutos = $umProduto['id'];
                        } else {
                            $idsProdutos .= "," . $umProduto['id'];
                        }
                    }
                }

                $totalProdutos = count($categorias[$key]['produtos']);

                if ($totalProdutos >= 4) {
                    break;
                }

                if (!empty($umaSub['idSubFilha'])) {
                    $qProdutos = $objProduto->getProdutos($umaSub['idSubFilha'],null,  4 - $totalProdutos, $idsProdutos, null, null, null, null, null, null, $this->mercadoMaster['id']);                    

                    if (!empty($qProdutos)) {
                        foreach ($qProdutos as $umProduto) {
                            $categorias[$key]['produtos'][] = $umProduto;
                            if (empty($idsProdutos)) {
                                $idsProdutos = $umProduto['id'];
                            } else {
                                $idsProdutos .= "," . $umProduto['id'];
                            }
                        }
                    }

                    $totalProdutos = count($categorias[$key]['produtos']);

                    if ($totalProdutos >= 4) {
                        break;
                    }
                }
            }

        }

        $this->view->subCategorias = $categorias;

        if (!empty($_SESSION['Default']['usuario']['mercado'])) {
           unset($_SESSION['Default']['usuario']['mercado']);
        }

        if (!empty($_SESSION['Default']['usuario']['mercados'])) {
           unset($_SESSION['Default']['usuario']['mercados']);
        }

    }

    public function logoutAction() {
    	Zend_Auth::getInstance()->hasIdentity();

        if (!empty($_SESSION['Default'])) {
            unset($_SESSION['Default']);
            Zend_Auth::getInstance()->clearIdentity();
        }

        $this->_redirect('/');
    }

}

