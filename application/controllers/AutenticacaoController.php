<?php

class Default_AutenticacaoController extends PainelBW_Painel {

    public function init() {
        Zend_Auth::getInstance()->hasIdentity();
        parent::init();
    }

    public function indexAction() {

    }

    public function cadastroAction() {
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);
      $post = $this->getRequest()->getPost();

      if (!empty($post)) {

        if (!empty($post['signin_password']) && !empty($post['signin_email']) && !empty($post['criarConta'])) {
          $objCliente = new Application_Model_DbTable_Cliente();
          $objCidade = new Application_Model_DbTable_Cidade();

          $dadosUsuario = $objCliente->fetchRow("email = '{$post['signin_email']}'");

          if (empty($dadosUsuario)) {

            // cria o usuario
            $arrayUsuario = array(
              'senha' => hash('sha512', $post['signin_password']),
              //'nome' => $post['signin_email'],
              'email' => $post['signin_email']
            );
            $objCliente->save($arrayUsuario);

            $senha = hash('sha512', $post['signin_password']);
            $dadosUsuario = $objCliente->fetchRow("email = '{$post['signin_email']}' && senha = '{$senha}'");

            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
            $authAdapter->setAmbiguityIdentity(true);

            //valida base do mercado
            $authAdapter->setTableName('clientes')
            ->setIdentityColumn('email')
            ->setCredentialColumn('senha');

            $authAdapter->setIdentity($post['signin_email'])->setCredential(hash('sha512', $post['signin_password']));

            $auth = Zend_Auth::getInstance();
            $autenticado = $auth->authenticate($authAdapter);

            if (!empty($autenticado) && $autenticado->isValid()) {
              $objUsuarioAutenticado = $authAdapter->getResultRowObject(null, 'senha');

              $storage = $auth->getStorage();
              $storage->write($storage);

              $sessaoUser = new Zend_Session_Namespace();
              $sessaoUser->usuario = array(
                'id' => $objUsuarioAutenticado->id,
                'nome' => $objUsuarioAutenticado->nome,
                'email' => $objUsuarioAutenticado->email
              );

              $objSessionNamespace = new Zend_Session_Namespace();
              $objSessionNamespace->setExpirationSeconds(86400);

              $_SESSION['Default']['usuario']['admin'] = false;
              $_SESSION['Default']['usuario']['dados'] = $objCliente->getDadosCliente($objUsuarioAutenticado->id);

              $front = Zend_Controller_Front::getInstance()->getBaseUrl();
              $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;
              $baseUrl2 = 'http://'. $_SERVER['HTTP_HOST'] . $front;
              $baseUrl2 = str_replace("public","", $baseUrl2);

              $mail = new Zend_Mail('utf-8');
              $mail->setSubject("Bem vindo ao ".$this->mercadoMaster['nome']);
              $mail->setFrom('mercadofiqueemcasa@gmail.com', $this->mercadoMaster['nome']);
              
              $html = array();
              $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
		      $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
			  $html[] = '<tr>';
			  $html[] = '<td width="600" align="center">';
              $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
              $html[] = '<tr>';
              $html[] = '<td height="80" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';

              if (!empty($this->mercadoMaster['foto2'])) {
                $html[] = '<img src="' . $baseUrl2 . '/admin/public/uploads/mercados/'.$this->mercadoMaster['foto2'].'" />';      
              } else {
                $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
              }

              
              $html[] = '</td>';
              $html[] = '</tr>';
              $html[] = '<tr><td><table style="width:540px!important;" align="center">';
			  $html[] = '<tr><td bgcolor="#FFFFFF" valign="center">';
              $html[] = '<p style="color:#666666!important;font-size:14px;">';
              $html[] = 'Seja Muito Bem vindo ao '.$this->mercadoMaster['nome'].'!<br /><br />';
              $html[] = 'Nós existimos para garantir segurança e conforto a você e sua família durante e depois a pandemia que está rolando. <br /><br /> ';
              $html[] = 'Gostariamos muito de mudar o nosso nome para "Mercado Vida Fácil" e nos comprometemos a fazer isso, assim que tivermos melhores noticias sobre a situação.  <br /><br />';
              $html[] = 'Até lá, prometemos fazer de tudo para abastecer sua casa sem que você tenha que se expor a aglomerações desnecessárias, e sem ter que perder tempo indo e vindo ao mercado.   <br /><br />';
              $html[] = 'Em caso de dúvidas, estamos a disposição, conta com a gente por meio do Whatsapp ou E-mail. <br /><br />';
              $html[] = 'Equipe Mercado Fique Em Casa. <br /><br />';
              $html[] = '<a href="'.$baseUrl.'" style="background:#71a505!important;color:#ffffff!important;display:block!important;height:40px!important;line-height:40px!important;text-decoration:none!important;width:200px!important;text-align:center!important;margin:0 auto!important;">Quero fazer uma compra!</a><br />';
              $html[] = '</p>';
              $html[] = '</td>';
			  $html[] = '</tr></table>';
			  $html[] = '</td></tr>';
              $html[] = '<tr>';
              $html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
              $html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">'.$baseUrl.'</a>';
              $html[] = '</td>';
              $html[] = '</tr>';
              $html[] = '</table>';
              $html[] = '</td>';
              $html[] = '</tr>';
              $html[] = '</table>';
			  $html[] = '</table></td></tr>';

              $mail->setBodyHtml(join("\n", $html));
              $mail->addTo($objUsuarioAutenticado->email, $objUsuarioAutenticado->email);
             
            $objConfigMail = new Application_Model_ConfigMail();
            $transport = $objConfigMail->get_configuracao_mail();
            $mail->send($transport);

            }

          } else {
            $_SESSION['msgErroCadastro'] = "Já existe um e-mail com esse cadastro, tente realizar o login.";
          }
        }
      }

      $this->_redirect('/');
      
    }


    public function loginAction() {
    	$post = $this->getRequest()->getPost();
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(true);

    	if (!empty($post)) {


    		if (!empty($post['login_password']) && !empty($post['login_email']) && !empty($post['login'])) {
    			$objCliente = new Application_Model_DbTable_Cliente();
          $objCidade = new Application_Model_DbTable_Cidade();

    			$senha = hash('sha512', $post['login_password']);
       		$dadosUsuario = $objCliente->fetchRow("email = '{$post['login_email']}' && senha = '{$senha}'");


          if ($dadosUsuario) {
            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
            $authAdapter->setAmbiguityIdentity(true);

            //valida base do mercado
            $authAdapter->setTableName('clientes')
              ->setIdentityColumn('email')
              ->setCredentialColumn('senha');

            $authAdapter->setIdentity($post['login_email'])->setCredential(hash('sha512', $post['login_password']));

            $auth = Zend_Auth::getInstance();
            $autenticado = $auth->authenticate($authAdapter);

            if (!empty($autenticado) && $autenticado->isValid()) {
              $objUsuarioAutenticado = $authAdapter->getResultRowObject(null, 'senha');

              $storage = $auth->getStorage();
              $storage->write($storage);

              $sessaoUser = new Zend_Session_Namespace();
              $sessaoUser->usuario = array(
                'id' => $objUsuarioAutenticado->id,
                'nome' => $objUsuarioAutenticado->nome,
                'email' => $objUsuarioAutenticado->email
              );

              $objSessionNamespace = new Zend_Session_Namespace();
              $objSessionNamespace->setExpirationSeconds(86400);

              $_SESSION['Default']['usuario']['admin'] = false;
              $_SESSION['Default']['usuario']['dados'] = $objCliente->getDadosCliente($objUsuarioAutenticado->id);


              $this->_redirect('/');
            }
          }

          $_SESSION['msgErroLogin'] = "Usuário ou senha inválidos";

    		}

    		if (!empty($post['recuperar_senha'])) {
            $objCliente = new Application_Model_DbTable_Cliente();
       			$dadosUsuario = $objCliente->fetchRow("email = '{$post['recuperar_senha']}'");

       			if (!empty($dadosUsuario)) {

       				$tokenSenha = time();

       				$arrayUsuario['tokenSenha'] = $tokenSenha;
       				$objCliente->save($arrayUsuario, "id = {$dadosUsuario['id']}");

       				//$tr = new Zend_Mail_Transport_Smtp('localhost');
              $front = Zend_Controller_Front::getInstance()->getBaseUrl();
              $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;
              $baseUrl2 = 'http://'. $_SERVER['HTTP_HOST'] . $front;
              $baseUrl2 = str_replace("public","", $baseUrl2);

              $mail = new Zend_Mail('utf-8');
              $mail->setSubject("Recuperar Senha");
              $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");

              $html = array();
              $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
              $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
              $html[] = '<tr>';
              $html[] = '<td width="600" align="center">';
              $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
              $html[] = '<tr>';
              $html[] = '<td height="90" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';

              if (!empty($this->mercadoMaster['foto2'])) {
      				  $html[] = '<img src="' . $baseUrl2 . '/admin/public/uploads/mercados/'.$this->mercadoMaster['foto2'].'" width="200" />';      
      				} else {
      				  $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
      				}

              $html[] = '</td>';
              $html[] = '</tr>';
              $html[] = '<tr>';
              $html[] = '<tr><td><table style="width:540px!important;" align="center">';
              $html[] = '<tr><td bgcolor="#FFFFFF" valign="center">';
                    $html[] = '<p style="color:#666666!important;font-size:14px;">';
                    $html[] = 'Olá '.$dadosUsuario['nome'].', <br><br> Se você esqueceu sua senha, clique no link abaixo para redefini-la.<br /><br />
              <a style="background:#71a505!important;color:#ffffff!important;display:block!important;height:40px!important;line-height:40px!important;text-decoration:none!important;width:200px!important;text-align:center!important;margin:0 auto!important;" href="'.$baseUrl.'/esqueci-senha/'.$tokenSenha.'">Redefinir minha senha</a>
              <br />Se você não fez essa solicitação, ignore esta mensagem e sua senha permanecerá a mesma.';
              	
              $html[] = '</p>';
              $html[] = '</td>';
              $html[] = '</tr></table>';
              $html[] = '</td></tr>';
                    $html[] = '<tr>';
              $html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
              $html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">'.$baseUrl.'</a>';
              $html[] = '</td>';
              $html[] = '</tr>';
              $html[] = '</table>';
              $html[] = '</td>';
              $html[] = '</tr>';
              $html[] = '</table>';
              $html[] = '</table></td></tr>';

              $mail->setBodyHtml(join("\n", $html));
              $mail->addTo($dadosUsuario['email'], $dadosUsuario['email']);
              
              $objConfigMail = new Application_Model_ConfigMail();
              $transport = $objConfigMail->get_configuracao_mail();
              $mail->send($transport);

              $this->_redirect('/');
		        } else {
              $_SESSION['msgErroLogin'] = "E-mail não encontrado, crie uma conta nos campos acima.";
            }
    		}
    	}

      $this->_redirect('/');
    }
}