<?php

class Default_ProdutoController extends PainelBW_Painel {

    public function init() {
        Zend_Auth::getInstance()->hasIdentity();
        parent::init();
    }

    public function indexAction() {
        $post = $this->getRequest()->getPost();

        if (!empty($post['search'])) {

            //protege para ter no mínimo 3 caracteres
            if (strlen($post['search']) < 3) {
                $this->_redirect('/');                   
            }

            $this->_redirect('/produto/buscar/'.$post['search']);
        }

        $this->_redirect('/');   

    }

    public function filtrarProdutosAction() {
        $post = $this->getRequest()->getPost();

        if (empty($post)) {
            $this->_redirect('/');
        }

        $listaDeCategorias = implode(',', $post['categorias']);

        if (empty($listaDeCategorias)) {
            $this->_redirect('/');
        }

        $objCategoria = new Application_Model_DbTable_Categoria();
        $objProduto = new Application_Model_DbTable_Produto();

        $sql = "SELECT "
            . "p.* "
            . ", (SELECT MIN(preco) as valor FROM produtos_mercados WHERE ativo = 1 AND idProduto = p.id) as precoMin "
            . ", (SELECT MAX(preco) as valor FROM produtos_mercados WHERE ativo = 1 AND idProduto = p.id) as precoMax "
            . ", c.id as idcategoriaPai "
            . ", c.nome as categoriaPai "
            . ", cp.id as idCategoriaVo "
            . ", cp.nome as categoriaVo "
            . ", cp2.nome as categoriaBisavo "
            . ", m.valor as marca "
            . ", t.valor as tipoProduto "
            . "FROM produtos p "
            . "LEFT JOIN categorias c ON (p.categoria = c.id) "
            . "LEFT JOIN categorias cp ON (c.categoria = cp.id) "
            . "LEFT JOIN categorias cp2 ON (cp.categoria = cp2.id) "
            . "LEFT JOIN atributos m ON (m.id = p.marca) "
            . "LEFT JOIN atributos s ON (s.id = p.sabor) "
            . "LEFT JOIN atributos t ON (t.id = p.tipoAt) "

            . "WHERE p.ativo = 1 AND c.id is not null ";

        

        $sql .= " AND p.categoria in ({$listaDeCategorias})";

        if (!empty($_SESSION['idMercadoMaster'])) {
            $sql .= " AND (SELECT SUM(preco) FROM produtos_mercados pm, mercados mm WHERE pm.idMercado = mm.id AND mm.ativo in (2,3) AND pm.ativo = 1 AND pm.idProduto = p.id AND mm.id = {$_SESSION['idMercadoMaster']}) > 0 ";

        } else {
            $sql .= " AND (SELECT SUM(preco) FROM produtos_mercados pm, mercados mm WHERE pm.idMercado = mm.id AND mm.ativo in (1,2) AND pm.ativo = 1 AND pm.idProduto = p.id) > 0 ";
        }

        $sql .= " ORDER BY cp2.nome, cp.nome, c.nome, p.nome ";   

        $dadosProdutos = $objProduto->getAdapter()->fetchAll($sql);

        $categorias = array();

        foreach ($dadosProdutos as $umProduto) {

            $key = array_search($umProduto['categoriaVo'],array_column($categorias, 'nome'));

            if (!is_numeric($key)) {
                $contador = count($categorias);
                $categorias[$contador]['id'] = $umProduto['idCategoriaVo'];
                $categorias[$contador]['nome'] = $umProduto['categoriaVo'];
                $categorias[$contador]['subCategorias'] = array();
                $key = $contador;
            }

            $key2 = array_search($umProduto['categoriaPai'],array_column($categorias[$key]['subCategorias'], 'nome'));

            if (!is_numeric($key2)) {
                $contador = count($categorias[$key]['subCategorias']);
                $categorias[$key]['subCategorias'][$contador]['id'] = $umProduto['idcategoriaPai'];
                $categorias[$key]['subCategorias'][$contador]['nome'] = $umProduto['categoriaPai'];
                $categorias[$key]['subCategorias'][$contador]['tipos'] = array();
                $categorias[$key]['subCategorias'][$contador]['marcas'] = array();
                $key2 = $contador;
            }

            if (!empty($umProduto['marca']) && !in_array($umProduto['marca'], $categorias[$key]['subCategorias'][$contador]['marcas'])) {
                array_push($categorias[$key]['subCategorias'][$contador]['marcas'], $umProduto['marca']);
            }

            if (!empty($umProduto['tipoProduto']) && !in_array($umProduto['tipoProduto'], $categorias[$key]['subCategorias'][$contador]['tipos'])) {
                array_push($categorias[$key]['subCategorias'][$contador]['tipos'], $umProduto['tipoProduto']);
            }

            $categorias[$key]['subCategorias'][$key2]['produtos'][] = $umProduto;

        }

        Zend_Layout::getMvcInstance()->assign('subCategorias', $categorias);

        $this->view->produtos = $categorias;

    }

    public function buscarAction() {
        $id = $this->getRequest()->getParam('search');
        
        $objProduto = new Application_Model_DbTable_Produto();
        $objCategoria = new Application_Model_DbTable_Produto();

        if (empty($id)) {
            $this->_redirect('/');   
        }

        $termo = addslashes($id);

        $sql = "SELECT "
            . "p.* "
            . ", (SELECT MIN(preco) as valor FROM produtos_mercados WHERE ativo = 1 AND idProduto = p.id) as precoMin "
            . ", (SELECT MAX(preco) as valor FROM produtos_mercados WHERE ativo = 1 AND idProduto = p.id) as precoMax "
            . ", c.id as idcategoriaPai "
            . ", c.nome as categoriaPai "
            . ", cp.id as idCategoriaVo "
            . ", cp.nome as categoriaVo "
            . ", cp2.nome as categoriaBisavo "

            . ", m.valor as marca "
            . ", t.valor as tipoProduto "

            . "FROM produtos p "
            . "LEFT JOIN categorias c ON (p.categoria = c.id) "
            . "LEFT JOIN categorias cp ON (c.categoria = cp.id) "
            . "LEFT JOIN categorias cp2 ON (cp.categoria = cp2.id) "
            . "LEFT JOIN atributos m ON (m.id = p.marca) "
            . "LEFT JOIN atributos s ON (s.id = p.sabor) "
            . "LEFT JOIN atributos t ON (t.id = p.tipoAt) "

            . "WHERE p.ativo = 1 AND c.id is not null ";

        if (!empty($_SESSION['idMercadoMaster'])) {
            $sql .= " AND (SELECT SUM(preco) FROM produtos_mercados pm, mercados mm WHERE pm.idMercado = mm.id AND mm.ativo in (2,3) AND pm.ativo = 1 AND pm.idProduto = p.id AND mm.id = {$_SESSION['idMercadoMaster']}) > 0 ";

        } else {
            $sql .= " AND (SELECT SUM(preco) FROM produtos_mercados pm, mercados mm WHERE pm.idMercado = mm.id AND mm.ativo in (1,2) AND pm.ativo = 1 AND pm.idProduto = p.id) > 0 ";
        }
        
        $sql .= " AND (p.nome like '%{$termo}%' 
                        OR 
                    m.valor like '%{$termo}%'
                        OR 
                    s.valor like '%{$termo}%') ";

        $sql .= " ORDER BY cp2.nome, cp.nome, c.nome, p.nome ";   

        $dadosProdutos = $objProduto->getAdapter()->fetchAll($sql);

        $categorias = array();

        foreach ($dadosProdutos as $umProduto) {

            $key = array_search($umProduto['categoriaVo'],array_column($categorias, 'nome'));

            if (!is_numeric($key)) {
                $contador = count($categorias);
                $categorias[$contador]['id'] = $umProduto['idCategoriaVo'];
                $categorias[$contador]['nome'] = $umProduto['categoriaVo'];
                $categorias[$contador]['subCategorias'] = array();
                $key = $contador;
                
            }

            $key2 = array_search($umProduto['categoriaPai'],array_column($categorias[$key]['subCategorias'], 'nome'));

            if (!is_numeric($key2)) {
                $contador = count($categorias[$key]['subCategorias']);
                $categorias[$key]['subCategorias'][$contador]['id'] = $umProduto['idcategoriaPai'];
                $categorias[$key]['subCategorias'][$contador]['nome'] = $umProduto['categoriaPai'];

                $categorias[$key]['subCategorias'][$contador]['tipos'] = array();
                $categorias[$key]['subCategorias'][$contador]['marcas'] = array();

                $key2 = $contador;
            }

            if (!empty($umProduto['marca']) && !in_array($umProduto['marca'], $categorias[$key]['subCategorias'][$contador]['marcas'])) {
                array_push($categorias[$key]['subCategorias'][$contador]['marcas'], $umProduto['marca']);
            }

            if (!empty($umProduto['tipoProduto']) && !in_array($umProduto['tipoProduto'], $categorias[$key]['subCategorias'][$contador]['tipos'])) {
                array_push($categorias[$key]['subCategorias'][$contador]['tipos'], $umProduto['tipoProduto']);
            }

            $categorias[$key]['subCategorias'][$key2]['produtos'][] = $umProduto;

        }

        Zend_Layout::getMvcInstance()->assign('subCategorias', $categorias);

        $this->view->produtos = $categorias;
        $this->view->post = $id;
        
    }

    public function visualizarAction() {
    	$id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();
        $objProduto = new Application_Model_DbTable_Produto();
        $objProdutoMercado = new Application_Model_DbTable_ProdutoMercado();
        $objCategoria = new Application_Model_DbTable_Categoria();
        $objAtributo = new Application_Model_DbTable_Atributo();

        if (empty($id)) {
        	$this->_redirect('/');
        } else {

            $this->view->id = $id;
            $dadosRegistro = $objProduto->getProdutos(null, $id, null, null, null, null, null, null, null, null, $this->mercadoMaster['id']);

            if (empty($dadosRegistro)) {
        		$this->_redirect('/');
        	}

            $arrayBreadcrumbs = $objCategoria->montaBreadCrumbs($dadosRegistro['categoria']);
            $arrayBreadcrumbs = $objCategoria->getBreadCrumbs();

            if (!empty($dadosRegistro['marca'])) {
                $dadosRegistro['marca'] = $objAtributo->fetchRow("id = {$dadosRegistro['marca']}");    
            }
            if (!empty($dadosRegistro['embalagem'])) {
                $dadosRegistro['embalagem'] = $objAtributo->fetchRow("id = {$dadosRegistro['embalagem']}");
            }
            if (!empty($dadosRegistro['pais'])) {
                $dadosRegistro['pais'] = $objAtributo->fetchRow("id = {$dadosRegistro['pais']}");
            }
            if (!empty($dadosRegistro['unidademedida'])) {
                $dadosRegistro['unidademedida'] = $objAtributo->fetchRow("id = {$dadosRegistro['unidademedida']}");
            }
            if (!empty($dadosRegistro['sabor'])) {
                $dadosRegistro['sabor'] = $objAtributo->fetchRow("id = {$dadosRegistro['sabor']}");
            }
            if (!empty($dadosRegistro['tamanho'])) {
                $dadosRegistro['tamanho'] = $objAtributo->fetchRow("id = {$dadosRegistro['tamanho']}");
            }
        	if (!empty($dadosRegistro['tipoAt'])) {
                $dadosRegistro['tipoAt'] = $objAtributo->fetchRow("id = {$dadosRegistro['tipoAt']}");
            }

        	$this->view->dadosRegistro = $dadosRegistro;

            Zend_Layout::getMvcInstance()->assign('arrayBreadcrumbs', $arrayBreadcrumbs);


        }
    }

}
