<?php

class Default_CronController extends PainelBW_Painel {

    public $request;

    public function init() {
        parent::init();
    }

    public function enviaRecuperacaoCarrinhoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $objCliente = new Application_Model_DbTable_Cliente();
        $objRecuperacaoCarrinho = new Application_Model_DbTable_RecuperacaoCarrinho();

        $front = Zend_Controller_Front::getInstance()->getBaseUrl();
        $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;
        $baseUrl2 = 'http://'. $_SERVER['HTTP_HOST'] . $front;
        $baseUrl2 = str_replace("public","", $baseUrl2);

        $arrayLista = $objRecuperacaoCarrinho->fetchAll("dataUltimoEmail is null AND pedido is null AND counterEmails = 0 AND dataUltimoEmail is null AND DATE_ADD(data, INTERVAL 15 MINUTE) < now()");

        //1 - em 15 minutos após fechar o carrinho ou site
        if (!empty($arrayLista)) {
            foreach($arrayLista as $umPedido) {

                // envia e-mail para o mercado
                $dadosMeuCarrinho = json_decode($umPedido['dadosCarrinho'], TRUE);

                $mail = new Zend_Mail('utf-8');
                $mail->setSubject("Lembra do seu pedido?");
                $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");
                $html = array();
                $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
                $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
                $html[] = '<tr>';
                $html[] = '<td width="600" align="center">';
                $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
                $html[] = '<tr>';
                $html[] = '<td height="80" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
                $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '<tr><td><table style="width:540px!important;" align="center">';
                $html[] = '<tr><td bgcolor="#FFFFFF" valign="center" colspan="4">';
                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Olá '.$dadosMeuCarrinho['nome'].' tudo bem?<br />';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Percebemos que você deixou alguns produtos em seu carrinho de compras.<br /> Podemos te ajudar a completar seu pedido?<br />';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = '<a href="'.$baseUrl.'">Clique aqui</a> para retornar as compras.';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Um grande abraço.';
                $html[] = '</p>';

/*                $html[] = '<table style="width:100%!important;margin-bottom:20px;border-bottom:1px solid #eaeaea!important;"><tr>';
                $html[] = '</tr></table>';
                $html[] = '</td></tr>';
                
                foreach ($dadosMeuCarrinho['carrinho']['itens'] as $umItem) {
                    $totalQde = $umItem['quantidade'];
                    $totalPreco = "De R$ " .$umItem['precoMin'] . " até R$ ". $umItem['precoMax'];

                    $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;">'.$umItem['name'].'</td>';
                    $html[] = '<td cellpadding="0" cellspacing="0" style="color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;"><table style="width:100%!important;"><tr><td style="width:70%!important;text-align:right!important;">'.$totalQde.'</td><td style="width:30%!important;text-align:right!important;">'.$totalPreco.'</td></tr></table></td></tr>';    
                }
                $html[] = '<tr><td cellpadding="0" cellspacing="0" style="width: 100%!important;height: 10px!important;margin: 0!important;padding: 0!important;" colspan="2">&nbsp;</td></tr>';

                $html[] = '</table></td></tr>';
                $html[] = '<tr>';
                $html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
                $html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">www.suacomprafacil.com.br</a>';
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '</table>';*/
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '</table>';
                $html[] = '</table></td></tr>';

                $mail->setBodyHtml(join("\n", $html));
                $mail->addTo($dadosMeuCarrinho['carrinho']['cliente']['email'], $dadosMeuCarrinho['carrinho']['cliente']['email']);
                
                $objConfigMail = new Application_Model_ConfigMail();
                $transport = $objConfigMail->get_configuracao_mail();
                $mail->send($transport);

                $objRecuperacaoCarrinho->save(array('dataUltimoEmail' => Date("Y-m-d H:i"), 'counterEmails' => $umPedido['counterEmails']+1), "id = {$umPedido['id']}");

            }
        }

        //2 - 3 horas depois
        $arrayLista = $objRecuperacaoCarrinho->fetchAll("dataUltimoEmail is not null AND pedido is null AND counterEmails = 1 AND dataUltimoEmail AND DATE_ADD(dataUltimoEmail, INTERVAL 3 HOUR) < now()");
        if (!empty($arrayLista)) {
            foreach($arrayLista as $umPedido) {

                // envia e-mail para o mercado
                $dadosMeuCarrinho = json_decode($umPedido['dadosCarrinho'], TRUE);
                
                $mail = new Zend_Mail('utf-8');
                $mail->setSubject("Lembra do seu pedido?");
                $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");
                $html = array();
                $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
                $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
                $html[] = '<tr>';
                $html[] = '<td width="600" align="center">';
                $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
                $html[] = '<tr>';
                $html[] = '<td height="80" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
                $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '<tr><td><table style="width:540px!important;" align="center">';
                $html[] = '<tr><td bgcolor="#FFFFFF" valign="center" colspan="4">';
                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Olá '.$dadosMeuCarrinho['nome'].' tudo bem?<br />';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Percebemos que você deixou alguns produtos em seu carrinho de compras.<br /> Podemos te ajudar a completar seu pedido?<br />';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = '<a href="'.$baseUrl.'">Clique aqui</a> para retornar as compras.';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Um grande abraço.';
                $html[] = '</p>';

/*                $html[] = '<table style="width:100%!important;margin-bottom:20px;border-bottom:1px solid #eaeaea!important;"><tr>';
                $html[] = '</tr></table>';
                $html[] = '</td></tr>';
                
                foreach ($dadosMeuCarrinho['carrinho']['itens'] as $umItem) {
                    $totalQde = $umItem['quantidade'];
                    $totalPreco = "De R$ " .$umItem['precoMin'] . " até R$ ". $umItem['precoMax'];

                    $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;">'.$umItem['name'].'</td>';
                    $html[] = '<td cellpadding="0" cellspacing="0" style="color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;"><table style="width:100%!important;"><tr><td style="width:70%!important;text-align:right!important;">'.$totalQde.'</td><td style="width:30%!important;text-align:right!important;">'.$totalPreco.'</td></tr></table></td></tr>';    
                }
                $html[] = '<tr><td cellpadding="0" cellspacing="0" style="width: 100%!important;height: 10px!important;margin: 0!important;padding: 0!important;" colspan="2">&nbsp;</td></tr>';

                $html[] = '</table></td></tr>';
                $html[] = '<tr>';
                $html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
                $html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">www.suacomprafacil.com.br</a>';
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '</table>';*/
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '</table>';
                $html[] = '</table></td></tr>';

                $mail->setBodyHtml(join("\n", $html));
                $mail->addTo($dadosMeuCarrinho['carrinho']['cliente']['email'], $dadosMeuCarrinho['carrinho']['cliente']['email']);
                $objConfigMail = new Application_Model_ConfigMail();
                $transport = $objConfigMail->get_configuracao_mail();
                $mail->send($transport);

                $objRecuperacaoCarrinho->save(array('dataUltimoEmail' => Date("Y-m-d H:i"), 'counterEmails' => $umPedido['counterEmails']+1), "id = {$umPedido['id']}");
            }
        }


        //3 - 24 horas após
        $arrayLista = $objRecuperacaoCarrinho->fetchAll("dataUltimoEmail is not null AND pedido is null AND counterEmails = 2 AND dataUltimoEmail AND DATE_ADD(dataUltimoEmail, INTERVAL 24 HOUR) < now()");
        if (!empty($arrayLista)) {
            foreach($arrayLista as $umPedido) {

                // envia e-mail para o mercado
                $dadosMeuCarrinho = json_decode($umPedido['dadosCarrinho'], TRUE);
                
                $mail = new Zend_Mail('utf-8');
                $mail->setSubject("Lembra do seu pedido?");
                $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");
                $html = array();
                $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
                $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
                $html[] = '<tr>';
                $html[] = '<td width="600" align="center">';
                $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
                $html[] = '<tr>';
                $html[] = '<td height="80" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
                $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '<tr><td><table style="width:540px!important;" align="center">';
                $html[] = '<tr><td bgcolor="#FFFFFF" valign="center" colspan="4">';
                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Olá '.$dadosMeuCarrinho['nome'].' tudo bem?<br />';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Percebemos que você deixou alguns produtos em seu carrinho de compras.<br /> Podemos te ajudar a completar seu pedido?<br />';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = '<a href="'.$baseUrl.'">Clique aqui</a> para retornar as compras.';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Um grande abraço.';
                $html[] = '</p>';

/*                $html[] = '<table style="width:100%!important;margin-bottom:20px;border-bottom:1px solid #eaeaea!important;"><tr>';
                $html[] = '</tr></table>';
                $html[] = '</td></tr>';
                
                foreach ($dadosMeuCarrinho['carrinho']['itens'] as $umItem) {
                    $totalQde = $umItem['quantidade'];
                    $totalPreco = "De R$ " .$umItem['precoMin'] . " até R$ ". $umItem['precoMax'];

                    $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;">'.$umItem['name'].'</td>';
                    $html[] = '<td cellpadding="0" cellspacing="0" style="color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;"><table style="width:100%!important;"><tr><td style="width:70%!important;text-align:right!important;">'.$totalQde.'</td><td style="width:30%!important;text-align:right!important;">'.$totalPreco.'</td></tr></table></td></tr>';    
                }
                $html[] = '<tr><td cellpadding="0" cellspacing="0" style="width: 100%!important;height: 10px!important;margin: 0!important;padding: 0!important;" colspan="2">&nbsp;</td></tr>';

                $html[] = '</table></td></tr>';
                $html[] = '<tr>';
                $html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
                $html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">www.suacomprafacil.com.br</a>';
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '</table>';*/
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '</table>';
                $html[] = '</table></td></tr>';

                $mail->setBodyHtml(join("\n", $html));
                $mail->addTo($dadosMeuCarrinho['carrinho']['cliente']['email'], $dadosMeuCarrinho['carrinho']['cliente']['email']);
                $objConfigMail = new Application_Model_ConfigMail();
                $transport = $objConfigMail->get_configuracao_mail();
                $mail->send($transport);

                $objRecuperacaoCarrinho->save(array('dataUltimoEmail' => Date("Y-m-d H:i"), 'counterEmails' => $umPedido['counterEmails']+1), "id = {$umPedido['id']}");
            }
        }

        //4 - 48 horas depois
        $arrayLista = $objRecuperacaoCarrinho->fetchAll("dataUltimoEmail is not null AND pedido is null AND counterEmails = 3 AND dataUltimoEmail AND DATE_ADD(dataUltimoEmail, INTERVAL 48 HOUR) < now()");
        if (!empty($arrayLista)) {
            foreach($arrayLista as $umPedido) {

                // envia e-mail para o mercado
                $dadosMeuCarrinho = json_decode($umPedido['dadosCarrinho'], TRUE);
                
                $mail = new Zend_Mail('utf-8');
                $mail->setSubject("Lembra do seu pedido?");
                $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");
                $html = array();
                $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
                $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
                $html[] = '<tr>';
                $html[] = '<td width="600" align="center">';
                $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
                $html[] = '<tr>';
                $html[] = '<td height="80" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
                $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '<tr><td><table style="width:540px!important;" align="center">';
                $html[] = '<tr><td bgcolor="#FFFFFF" valign="center" colspan="4">';
                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Olá '.$dadosMeuCarrinho['nome'].' tudo bem?<br />';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Percebemos que você deixou alguns produtos em seu carrinho de compras.<br /> Podemos te ajudar a completar seu pedido?<br />';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = '<a href="'.$baseUrl.'">Clique aqui</a> para retornar as compras.';
                $html[] = '</p>';

                $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
                $html[] = 'Um grande abraço.';
                $html[] = '</p>';

/*                $html[] = '<table style="width:100%!important;margin-bottom:20px;border-bottom:1px solid #eaeaea!important;"><tr>';
                $html[] = '</tr></table>';
                $html[] = '</td></tr>';
                
                foreach ($dadosMeuCarrinho['carrinho']['itens'] as $umItem) {
                    $totalQde = $umItem['quantidade'];
                    $totalPreco = "De R$ " .$umItem['precoMin'] . " até R$ ". $umItem['precoMax'];

                    $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;">'.$umItem['name'].'</td>';
                    $html[] = '<td cellpadding="0" cellspacing="0" style="color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;"><table style="width:100%!important;"><tr><td style="width:70%!important;text-align:right!important;">'.$totalQde.'</td><td style="width:30%!important;text-align:right!important;">'.$totalPreco.'</td></tr></table></td></tr>';    
                }
                $html[] = '<tr><td cellpadding="0" cellspacing="0" style="width: 100%!important;height: 10px!important;margin: 0!important;padding: 0!important;" colspan="2">&nbsp;</td></tr>';

                $html[] = '</table></td></tr>';
                $html[] = '<tr>';
                $html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
                $html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">www.suacomprafacil.com.br</a>';
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '</table>';*/
                $html[] = '</td>';
                $html[] = '</tr>';
                $html[] = '</table>';
                $html[] = '</table></td></tr>';

                $mail->setBodyHtml(join("\n", $html));
                $mail->addTo($dadosMeuCarrinho['carrinho']['cliente']['email'], $dadosMeuCarrinho['carrinho']['cliente']['email']);
                $objConfigMail = new Application_Model_ConfigMail();
                $transport = $objConfigMail->get_configuracao_mail();
                $mail->send($transport);

                $objRecuperacaoCarrinho->save(array('dataUltimoEmail' => Date("Y-m-d H:i"), 'counterEmails' => $umPedido['counterEmails']+1), "id = {$umPedido['id']}");
            }
        }
        

    }
}