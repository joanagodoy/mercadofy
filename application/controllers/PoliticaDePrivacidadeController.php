<?php
class Default_PoliticaDePrivacidadeController extends PainelBW_Painel
{
    public function init() {
        parent::init();

    }
	
	public function indexAction() {
	 	//verifico se mostro dados gerais ou do mercado especifico
		if (!empty($this->mercadoMaster['id'])) {
			$objMercadosTextos = new Application_Model_DbTable_MercadoTextos();
			$this->view->textosDuvidas = $objMercadosTextos->fetchAll("mercado = {$this->mercadoMaster['id']} AND tipo = 2", "texto asc");
		}
	}
}
    