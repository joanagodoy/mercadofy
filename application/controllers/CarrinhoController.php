<?php

class Default_CarrinhoController extends PainelBW_Painel {

    public function init() {
        Zend_Auth::getInstance()->hasIdentity();
        parent::init();
    }

    public function modificaCarrinhoMercadosAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $objMercado = new Application_Model_DbTable_Mercado();
        $objProduto = new Application_Model_DbTable_Produto();
        $objCategoria = new Application_Model_DbTable_Categoria();

        if (!empty($post)) {

            if ($post['action'] == 'remover') {

                foreach($_SESSION['Default']['usuario']['mercados'] as $key => $umMercado) {
                    if ($umMercado['id'] == $post['mercado']) {
                        foreach($umMercado['produtoFaltante'] as $key2 => $umProdutoFaltante) {
                            if ($umProdutoFaltante['id'] == $post['produto']) {
                                array_splice($_SESSION['Default']['usuario']['mercados'][$key]['produtoFaltante'],$key2,1);
                                break;
                            }
                        }
                        break;
                    }
                }
            }

            if ($post['action'] == 'trocar') {

                foreach($_SESSION['Default']['usuario']['mercados'] as $key => $umMercado) {
                    if ($umMercado['id'] == $post['mercado']) {
                        foreach($umMercado['produtoFaltante'] as $key2 => $umProdutoFaltante) {
                            $totalProdutos = 0;

                            if ($umProdutoFaltante['id'] == $post['produtoOriginal']) {

                                $dadosRegistro = $objProduto->getProdutos(null, $post['produtoNovo'], null, null, null, null, null, null, null, null, $this->mercadoMaster['id']);

                                if (!empty($dadosRegistro)) {

                                    $itemArray = array(
                                        'foto'=>strtolower($dadosRegistro["foto"]), 
                                        'webp'=>strtolower($dadosRegistro["webp"]), 
                                        'name'=>$dadosRegistro["nome"], 
                                        'id'=>$dadosRegistro["id"], 
                                        'tipo'=>$dadosRegistro["tipo"], 
                                        'categoria'=>$dadosRegistro["categoria"], 
                                        'categoria2'=>$dadosRegistro["categoriaPai"], 
                                        'categoria3'=>$dadosRegistro["categoriaPai2"], 
                                        'categoria4'=>$dadosRegistro["categoriaPai3"], 
                                        'quantidade'=>$umProdutoFaltante['quantidade'], 
                                        'precoMin'=>number_format($dadosRegistro['precoMin'], 2, ',', '.'), 
                                        'valorMin'=>number_format($dadosRegistro['precoMin'],2,'.',''), 
                                        'precoMax'=>number_format($dadosRegistro['precoMax'], 2 , ',', '.'), 
                                        'valorMax'=>number_format($dadosRegistro['precoMax'],2,'.','')
                                    );

                                    $sql = "SELECT"
                                        . " preco  "
                                        . " FROM produtos_mercados "
                                        . " WHERE "
                                        . " ativo = 1 "
                                        . " AND estoque >= {$umProdutoFaltante['quantidade']} "
                                        . " AND idMercado = {$umMercado['id']} "
                                        . " AND idProduto = {$dadosRegistro['id']} ";

                                    $qValorProduto = $objMercado->getAdapter()->fetchRow($sql);

                                    $itemArray['valorMercado'] = $qValorProduto['preco'];
                                    $itemArray['quantidade'] = $umProdutoFaltante['quantidade'];

                                    // se for item normal, faz o preco * valor do produto
                                    // se for item granel, o preco eh sempre uma regra de 3 baseado em 1 kg
                                    if ($dadosRegistro == 0) {
                                        $totalProdutos += ($qValorProduto['preco'] * $umProdutoFaltante['quantidade']) / 1000;    
                                    } else {
                                        $totalProdutos += ($qValorProduto['preco'] * $umProdutoFaltante['quantidade']);    
                                    }

                                    array_splice($_SESSION['Default']['usuario']['mercados'][$key]['produtoFaltante'],$key2,1);

                                    $_SESSION['Default']['usuario']['mercados'][$key]['produtos'][] = $itemArray;    
                                    $_SESSION['Default']['usuario']['mercados'][$key]['totalProdutos'] += $totalProdutos;    
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        echo json_encode($_SESSION['Default']['usuario']['mercados']);
    }

    public function modificaCarrinhoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $post = $this->getRequest()->getPost();
        $objProduto = new Application_Model_DbTable_Produto();
        $objCategoria = new Application_Model_DbTable_Categoria();
        $objRecuperacaoCarrinho = new Application_Model_DbTable_RecuperacaoCarrinho();

        if (empty($_SESSION['Default']['usuario']['carrinho'])) {
            $_SESSION['Default']['usuario']['carrinho']['itens'] = array();
            $_SESSION['Default']['usuario']['carrinho']['totalItens'] = 0;
        }

        $_SESSION['Default']['usuario']['carrinho']['adicionarCep'] = false;

        if (!empty($_SESSION['Default']['usuario']['carrinho']['idProdutoCep'])) {
            unset($_SESSION['Default']['usuario']['carrinho']['idProdutoCep']); 
        }

        if (!empty($_SESSION['Default']['usuario']['carrinho']['qtProdutoCep'])) {
            unset($_SESSION['Default']['usuario']['carrinho']['qtProdutoCep']); 
        }

        if (empty($_SESSION['Default']['usuario']['endereco']['cep']) || $_SESSION['Default']['usuario']['endereco']['atendimento'] == false) {
            $_SESSION['Default']['usuario']['carrinho']['adicionarCep'] = true;
            $_SESSION['Default']['usuario']['carrinho']['qtProdutoCep'] = $post['valor'];
            $_SESSION['Default']['usuario']['carrinho']['idProdutoCep'] = $post['produto'];
            $_SESSION['Default']['usuario']['carrinho']['itens'] = array();
            $_SESSION['Default']['usuario']['carrinho']['totalItens'] = 0;

        } else {


            if (!empty($post['produto']) && isset($post['valor']) && is_numeric($post['produto']) && is_numeric($post['valor'])) {

                $action = $post['action'];

                //verifica se o produto está ativo
                $dadosRegistro = $objProduto->getProdutos(null, $post['produto'], null, null, null, null, null, null, null, null, $this->mercadoMaster['id']);

                if (!empty($dadosRegistro)) {
                    
                    if ($action == 'rem') {

                        if(!empty($_SESSION['Default']['usuario']['carrinho']['itens'])) {
                            foreach($_SESSION['Default']['usuario']['carrinho']['itens'] as $k => $v) {
                                if($dadosRegistro["id"] == $v['id']) {
                                    unset($_SESSION['Default']['usuario']['carrinho']['itens'][$k]);              
                                }
                            }
                        }

                    } else {

                        $itemArray = array(
                            $dadosRegistro["id"] => array(
                                    'foto'=>strtolower($dadosRegistro["foto"]),
                                    'webp'=>strtolower($dadosRegistro["webp"]),
                                    'tipo'=>$dadosRegistro["tipo"], 
                                    'name'=>$dadosRegistro["nome"], 
                                    'id'=>$dadosRegistro["id"], 
                                    'categoria'=>$dadosRegistro["categoria"], 
                                    'categoria2'=>$dadosRegistro["categoriaPai"], 
                                    'categoria3'=>$dadosRegistro["categoriaPai2"], 
                                    'categoria4'=>$dadosRegistro["categoriaPai3"], 
                                    'quantidade'=>$post['valor'], 
                                    'precoMin'=>number_format($dadosRegistro['precoMin'], 2, ',', '.'), 
                                    'valorMin'=>number_format($dadosRegistro['precoMin'],2,'.',''),
                                    'precoMax'=>number_format($dadosRegistro['precoMax'], 2 , ',', '.'), 
                                    'valorMax'=>number_format($dadosRegistro['precoMax'],2,'.','')
                                )
                        );


                        if(!empty($_SESSION['Default']['usuario']['carrinho']['itens'])) {

                            $entrou = false;

                            foreach($_SESSION['Default']['usuario']['carrinho']['itens'] as $k => $v) {

                                if($dadosRegistro["id"] == $v['id']) {
                                    $entrou = true;

                                    if ($action == 'add') {
                                        $_SESSION['Default']['usuario']['carrinho']['itens'][$k]["quantidade"] += $post['valor'];
                                    } else {
                                        $_SESSION['Default']['usuario']['carrinho']['itens'][$k]["quantidade"] = $post['valor'];    
                                    }

                                    
                                }
                            }

                            if(!$entrou) {
                                $_SESSION['Default']['usuario']['carrinho']['itens'] = array_merge($_SESSION['Default']['usuario']['carrinho']['itens'],$itemArray);
                            }
                        } else {
                            $_SESSION['Default']['usuario']['carrinho']['itens'] = $itemArray;
                        }
                    }

                }

            } 

            //organiza carrinho por categorias para carrinho mobile
            $arrayPorCategorias = array();

            $totalDeItens = 0;

            if (!empty($_SESSION['Default']['usuario']['carrinho']['itens'])) {
                foreach ($_SESSION['Default']['usuario']['carrinho']['itens'] as $umItem) {

                    if ($umItem['tipo'] == 0) {
                        $totalDeItens++;
                    } else {
                        $totalDeItens += $umItem['quantidade'];
                    }

                    $categoriaPrincipal = $umItem['categoria4'];

                    if (empty($categoriaPrincipal)) {
                        $categoriaPrincipal = $umItem['categoria3'];
                    }

                    if (empty($categoriaPrincipal)) {
                        $categoriaPrincipal = $umItem['categoria2'];
                    }

                    if (empty($categoriaPrincipal)) {
                        $categoriaPrincipal = $umItem['categoria'];
                    }

                    $dadoCategoria = $objCategoria->fetchRow("id = '{$categoriaPrincipal}'");

                    $keyNova = 0;
                    $adicionou = false;
                    foreach($arrayPorCategorias as $key => $umaCategoria) {
                        if ($arrayPorCategorias[$key]['categoria']['id'] == $categoriaPrincipal) {
                            $adicionou = true;
                            $arrayPorCategorias[$key]['itens'][] = $umItem;
                        }
                        $keyNova = $key+1;
                    }

                    if (!$adicionou) {
                        $arrayPorCategorias[$keyNova]['categoria'] = $dadoCategoria;
                        $arrayPorCategorias[$keyNova]['itens'][] = $umItem;    
                    }

                }
            }

            if (!empty($_SESSION['Default']['usuario']['carrinho']['mercado'])) {
                unset($_SESSION['Default']['usuario']['carrinho']['mercado']);
            }

            $_SESSION['Default']['usuario']['carrinho']['categorias'] = $arrayPorCategorias;

            //$_SESSION['Default']['usuario']['carrinho']['totalItens'] = count($_SESSION['Default']['usuario']['carrinho']['itens']);
            $_SESSION['Default']['usuario']['carrinho']['totalItens'] = $totalDeItens;
        }


        $strCarrinho = json_encode($_SESSION['Default']['usuario'], JSON_UNESCAPED_UNICODE);

        $idCliente = 0;

        if (!empty($_SESSION['Default']['usuario']['id'])) {
            $idCliente = $_SESSION['Default']['usuario']['id'];

        } else if (!empty($_SESSION['Default']['usuario']['carrinho']['cliente']['id'])) {
            $idCliente = $_SESSION['Default']['usuario']['carrinho']['cliente']['id'];
        }

        if (!empty($idCliente)) {

            $meuCarrinhoAtual = $objRecuperacaoCarrinho->fetchRow("counterEmails = 0 AND pedido is null AND cliente = {$idCliente}");
            $idCarrinho = $meuCarrinhoAtual['id'];

            $dadosCarrinho = array (
                'cliente' => $idCliente,
                'dadoscarrinho' => $strCarrinho
            );

            if (!empty($meuCarrinhoAtual)) {
                $objRecuperacaoCarrinho->save($dadosCarrinho,"counterEmails = 0 AND pedido is null AND cliente = {$idCliente}");    

            } else {
                $objRecuperacaoCarrinho->save($dadosCarrinho);
                $idCarrinho = $objRecuperacaoCarrinho->getAdapter()->lastInsertId();
            }

            if (empty($totalDeItens)) {
                $objRecuperacaoCarrinho->delete("id = {$idCarrinho}");
            }

        }

        echo json_encode($_SESSION['Default']['usuario']['carrinho']);

    }

    public function loginAction() {
        $post = $this->getRequest()->getPost();
        $objCliente = new Application_Model_DbTable_Cliente();
        $objRecuperacaoCarrinho = new Application_Model_DbTable_RecuperacaoCarrinho();

        if (empty($_SESSION['Default']['usuario']['carrinho'])) {
            $this->_redirect('/');
        }

        if (empty($_SESSION['Default']['usuario']['carrinho']['itens'])) {
            $this->_redirect('/');
        }

        if (!empty($_SESSION['Default']['usuario']['id'])) {
            $dadosUsuario = $objCliente->fetchRow("id = {$_SESSION['Default']['usuario']['id']}");
            unset($dadosUsuario['senha']);
            unset($dadosUsuario['tokenSenha']);
            $_SESSION['Default']['usuario']['carrinho']['cliente'] = $dadosUsuario;

            $this->_redirect('/carrinho/checkout');
        }

        if (!empty($_SESSION['Default']['usuario']['carrinho']['mercado'])) {
            unset($_SESSION['Default']['usuario']['carrinho']['mercado']);
        }

        $carrinhoAtual = $_SESSION['Default']['usuario']['carrinho'];
        $enderecoAtual = $_SESSION['Default']['usuario']['endereco'];

        if (!empty($post) && !empty($post['continuar'])) {

            
            $objCidade = new Application_Model_DbTable_Cidade();
            $dadosUsuario = $objCliente->fetchRow("email = '{$post['email']}'");

            if ($dadosUsuario) {
                unset($dadosUsuario['senha']);
                unset($dadosUsuario['tokenSenha']);
                $_SESSION['Default']['usuario']['admin'] = false;
                $_SESSION['Default']['usuario']['carrinho'] = $carrinhoAtual;
                $_SESSION['Default']['usuario']['endereco'] = $enderecoAtual;
                $_SESSION['Default']['usuario']['carrinho']['cliente'] = $dadosUsuario;
                $_SESSION['Default']['usuario']['dados'] = $objCliente->getDadosCliente($dadosUsuario['id']);


                $strCarrinho = json_encode($_SESSION['Default']['usuario'], JSON_UNESCAPED_UNICODE);

                $meuCarrinhoAtual = $objRecuperacaoCarrinho->fetchRow("counterEmails = 0 AND pedido is null AND cliente = {$dadosUsuario['id']}");

                $dadosCarrinho = array (
                    'cliente' => $dadosUsuario['id'],
                    'dadoscarrinho' => $strCarrinho
                );

                if (!empty($meuCarrinhoAtual)) {
                    $objRecuperacaoCarrinho->save($dadosCarrinho,"counterEmails = 0 AND pedido is null AND cliente = {$dadosUsuario['id']}");    
                } else {
                    $objRecuperacaoCarrinho->save($dadosCarrinho);
                }

                $this->_redirect('/carrinho/checkout');
            } else {
                $_SESSION['Default']['usuario']['carrinho']['emailCliente'] = $post['email'];

                $this->_redirect('/carrinho/checkout');
            }

        }

    }

    public function checkoutAction() {
        $post = $this->getRequest()->getPost();
        $objMercado = new Application_Model_DbTable_Mercado();
        $objProduto = new Application_Model_DbTable_Produto();

        if (empty($_SESSION['Default']['usuario']['carrinho']) && empty($_SESSION['Default']['usuario']['mercados'])) {
            $this->_redirect('/');
        }

        if (empty($_SESSION['Default']['usuario']['carrinho']['itens']) && empty($_SESSION['Default']['usuario']['mercados'])) {
            $this->_redirect('/');
        }

        if (!empty($_SESSION['Default']['usuario']['mercado'])) {
           unset($_SESSION['Default']['usuario']['mercado']);
        }

        if (!empty($post) && empty($post['ordenacao'])) {  

            $_SESSION['Default']['usuario']['mercado'] = $_SESSION['Default']['usuario']['mercados'][$post['idMercado']];

            $this->_redirect('/carrinho/confirmar-dados');
        }

        if (empty($_SESSION['Default']['usuario']['endereco']['cep'])) {
           $this->_redirect('/');
        }

        $cepAtual = intval(str_replace("-", "", $_SESSION['Default']['usuario']['endereco']['cep']));

        if (empty($post['ordenacao'])) {
            $order = "m.nome asc";
        } else {
            //0 = nome
            if ($post['ordenacao'] == 0) {
                $order = "m.nome asc";
            }
            //1 = maximo produtos
            else if ($post['ordenacao'] == 1) {
                $order = "m.nome asc";
            }
            //2 = menor preco entrega
            else if ($post['ordenacao'] == 2) {
                $order = "r.taxacobrada asc";
            }
            //3 = menor preco retirada
            else if ($post['ordenacao'] == 3) {
                $order = "rr.taxa asc";
            }
        }

        //valida os mercados e quais entregam na faixa do cep definido
        $sql = "SELECT"
            . " m.id "
            . " ,m.nome  "
            . " , LOWER(m.foto) as foto "
            . " ,m.horariodeatendimento "
            
            . " ,r.valor"
            . " ,r.valorsemtaxa"
            . " ,r.taxacobrada "
            . " ,r.comprasde "
            . " ,r.comprasate "
            . " ,r.entregaem "
            . " ,r.comprasde2 "
            . " ,r.comprasate2 "
            . " ,r.entregaem2 "
            . " ,r.comprasde3 "
            . " ,r.comprasate3 "
            . " ,r.entregaem3 "

            . " ,rr.taxa "
            . " ,rr.comprasde as retcomprasde "
            . " ,rr.comprasate as retcomprasate "
            . " ,rr.retiradaem "
            . " ,rr.comprasde2 as retcomprasde2 "
            . " ,rr.comprasate2 as retcomprasate2 "
            . " ,rr.retiradaem2 "
            . " ,rr.comprasde3 as retcomprasde3 "
            . " ,rr.comprasate3 as retcomprasate3 "
            . " ,rr.retiradaem3 "
            . " ,rr.comprasde4 as retcomprasde4 "
            . " ,rr.comprasate4 as retcomprasate4 "
            . " ,rr.retiradaem4 "
            
            . " FROM mercados m "

            . " LEFT JOIN regrasentrega r ON (m.id = r.mercado AND {$cepAtual} BETWEEN CAST(REPLACE(r.faixacepde, '-', '') AS UNSIGNED) AND CAST(REPLACE(r.faixacepate, '-', '') AS UNSIGNED) )"
            . " LEFT JOIN regrasretiradas rr ON (m.id = rr.mercado AND {$cepAtual} BETWEEN CAST(REPLACE(rr.faixacepde, '-', '') AS UNSIGNED) AND CAST(REPLACE(rr.faixacepate, '-', '') AS UNSIGNED) )"

            . " WHERE ";

            if (!empty($this->mercadoMaster['id'])) {
                $sql .= " m.ativo in (2,3) ";    
            } else {
                $sql .= " m.ativo in (1,2) ";    
            }
            
            
            $sql .= " AND (r.comprasde is not null OR rr.comprasde is not null) "

            . " GROUP BY m.id "
            . " ORDER BY {$order} ";

    
        $mercados = $objMercado->getAdapter()->fetchAll($sql);

        //loop pelos produtos do carrinho para ver se o mercado vende
        foreach($mercados as $key => $umMercado) {

            $totalProdutos = 0;

            $sqlProdutos = "";

            foreach($_SESSION['Default']['usuario']['carrinho']['itens'] as $umItem) {
                $sqlProdutos .= " AND p.id <> {$umItem['id']} ";
            }

            $mostraMercado = false;

            foreach($_SESSION['Default']['usuario']['carrinho']['itens'] as $umItem) {

                $umItem['foto'] = strtolower($umItem['foto']);
                $umItem['webp'] = strtolower($umItem['webp']);

                $sql = "SELECT"
                    . " preco  "
                    . " FROM produtos_mercados "
                    . " WHERE "
                    . " ativo = 1 "
                    . " AND estoque >= {$umItem['quantidade']} "
                    . " AND idMercado = {$umMercado['id']} "
                    . " AND idProduto = {$umItem['id']} ";

                $qValorProduto = $objMercado->getAdapter()->fetchRow($sql);

                if (empty($qValorProduto)) {

                     $sql = "SELECT"
                        . " p.*, "
                        . " pm.preco "
                        . " FROM produtos p, produtos_mercados pm "
                        . " WHERE "
                        . " p.ativo = 1 "
                        . " AND p.id = pm.idProduto "
                        . " AND pm.idMercado = {$umMercado['id']} "
                        . $sqlProdutos
                        . " AND pm.ativo = 1 "
                        . " AND pm.estoque >= {$umItem['quantidade']} "
                        . " AND p.categoria = {$umItem['categoria']} "
                        . " ORDER BY p.nome LIMIT 0,4 ";


                    $listaProdutos = $objProduto->getAdapter()->fetchAll($sql);
                    $outrosProdutos = array();

                    if (!empty($listaProdutos)) {

                        $mostraMercado = true;

                        foreach ($listaProdutos as $umProdutoDica) {
                            $outrosProdutos[] = $umProdutoDica;
                        }

                        //itens da mesma categoria do produto faltante
                        $umItem['dicas'] = $outrosProdutos;
                        $mercados[$key]['produtoFaltante'][] = $umItem;
                    } else {
                        $mostraMercado = false;                        
                    }

                } else {

                    $mostraMercado = true;

                    if ($umItem['tipo'] == 0) {
                        $totalProdutos += ($qValorProduto['preco'] * $umItem['quantidade']) / 1000;
                    } else {
                        $totalProdutos += ($qValorProduto['preco'] * $umItem['quantidade']);    
                    }
                    

                    $umItem['valorMercado'] = $qValorProduto['preco'];
                    $mercados[$key]['produtos'][] = $umItem;
                }


            }

            if (!$mostraMercado) {
                unset($mercados[$key]);
                continue;
            }

            $mercados[$key]['mostrar'] = $mostraMercado;

            $sql = "SELECT"
                . " * "
                . " FROM formaspagamentos "
                . " WHERE "
                . " status = 1 "
                . " AND mercado =  {$umMercado['id']} "
                . " ORDER BY nome ";

            $qFormasPgto = $objMercado->getAdapter()->fetchAll($sql);

            $mercados[$key]['formasPagamento'] = $qFormasPgto;
            $mercados[$key]['totalProdutos'] = $totalProdutos;

            //deixa o array de produtos faltantes vazio para controle
            if (empty($mercados[$key]['produtoFaltante'])) {
                $mercados[$key]['produtoFaltante'] = array();
            }

            $horaAtual = (double) Date("H.i");

            $comprasDe = (double) str_replace(":", ".", $umMercado['comprasde']);
            $comprasAte = (double) str_replace(":", ".", $umMercado['comprasate']);

            $comprasDe2 = (double) str_replace(":", ".", $umMercado['comprasde2']);
            $comprasAte2 = (double) str_replace(":", ".", $umMercado['comprasate2']);

            $comprasDe3 = (double) str_replace(":", ".", $umMercado['comprasde3']);
            $comprasAte3 = (double) str_replace(":", ".", $umMercado['comprasate3']);

            
            //verifico qual a proxima possivel entrega
            $mercados[$key]['proximaEntrega'] = "";

            if ($horaAtual >= $comprasDe && $horaAtual <= $comprasAte) {
                $mercados[$key]['proximaEntrega'] = $umMercado['entregaem'];
            }

            if ($horaAtual >= $comprasDe2 && $horaAtual <= $comprasAte2) {
                $mercados[$key]['proximaEntrega'] = $umMercado['entregaem2'];
            }

            if ($horaAtual >= $comprasDe3 && $horaAtual <= $comprasAte3) {
                $mercados[$key]['proximaEntrega'] = $umMercado['entregaem3'];
            }

            $comprasDe = (double) str_replace(":", ".", $umMercado['retcomprasde']);
            $comprasAte = (double) str_replace(":", ".", $umMercado['retcomprasate']);

            $comprasDe2 = (double) str_replace(":", ".", $umMercado['retcomprasde2']);
            $comprasAte2 = (double) str_replace(":", ".", $umMercado['retcomprasate2']);

            $comprasDe3 = (double) str_replace(":", ".", $umMercado['retcomprasde3']);
            $comprasAte3 = (double) str_replace(":", ".", $umMercado['retcomprasate3']);

            $comprasDe4 = (double) str_replace(":", ".", $umMercado['retcomprasde4']);
            $comprasAte4 = (double) str_replace(":", ".", $umMercado['retcomprasate4']);

            //verifico qual a proxima possivel retirada
            $mercados[$key]['proximaRetirada'] = "";

            if ($horaAtual >= $comprasDe && $horaAtual <= $comprasAte) {
                $mercados[$key]['proximaRetirada'] = $umMercado['retiradaem'];
            }

            if ($horaAtual >= $comprasDe2 && $horaAtual <= $comprasAte2) {
                $mercados[$key]['proximaRetirada'] = $umMercado['retiradaem2'];
            }

            if ($horaAtual >= $comprasDe3 && $horaAtual <= $comprasAte3) {
                $mercados[$key]['proximaRetirada'] = $umMercado['retiradaem3'];
            }

            if ($horaAtual >= $comprasDe4 && $horaAtual <= $comprasAte4) {
                $mercados[$key]['proximaRetirada'] = $umMercado['retiradaem4'];
            }


            // se estiver em um subdominio de um mercado, passa direto
            if (!empty($this->mercadoMaster['id'])) {

                if ($this->mercadoMaster['id'] == $umMercado['id']) {
                    $_SESSION['Default']['usuario']['mercado'] = $mercados[$key];
                    $this->_redirect('/carrinho/confirmar-dados');
                }

                
            }

        }

        /*if (!empty($post['ordenacao']) && $post['ordenacao'] == 1) {
            usort($mercados, function($a, $b) {
                return $a['totalProdutos'] <=> $b['totalProdutos'];
            });
        }*/

        $this->view->mercados = $mercados;

        $_SESSION['Default']['usuario']['mercados'] = $mercados;

    }
        
    public function confirmarDadosAction() {
        $objCliente = new Application_Model_DbTable_Cliente();
//        $objMercado = new Application_Model_DbTable_Mercado();
        $objPedido = new Application_Model_DbTable_Pedido();
//        $objProdutoMercado = new Application_Model_DbTable_ProdutoMercado();
//        $objPedidoItens = new Application_Model_DbTable_PedidoItem();
//        $objClienteEndereco = new Application_Model_DbTable_ClienteEndereco();
//        $objRecuperacaoCarrinho = new Application_Model_DbTable_RecuperacaoCarrinho();
        $post = $this->getRequest()->getPost();

        if (empty($_SESSION['Default']['usuario']['mercado'])) {
           $this->_redirect('/carrinho/checkout'); 
        }
        if ($_SESSION['Default']['usuario']['mercado']['totalProdutos'] == 0) {
            $this->_redirect('/'); 
        }

        if (!empty($this->mercadoMaster['id'])) {
            if ($this->mercadoMaster['id'] != $_SESSION['Default']['usuario']['mercado']['id']) {
                $this->_redirect('/'); 
            }
        }
        
        $this->view->mercado = $_SESSION['Default']['usuario']['mercado'];
        $cepAtual = str_replace("-", "", $_SESSION['Default']['usuario']['endereco']['cep']);

        if (!empty($post)) {
                $dadosPedido = $objPedido->getDadosPedido($post['pedido']);
                
                $dadosCliente = $objCliente->fetchRow("id = '{$dadosPedido['idCliente']}'");
                
                $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                    $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
                    $authAdapter->setAmbiguityIdentity(true);
                    
                    //valida base do mercado
                    $authAdapter->setTableName('clientes')
                      ->setIdentityColumn('email')
                      ->setCredentialColumn('senha');

                    $authAdapter->setIdentity($dadosCliente['email'])->setCredential($dadosCliente['senha']);

                    $auth = Zend_Auth::getInstance();
                    $autenticado = $auth->authenticate($authAdapter);

                    if (!empty($autenticado) && $autenticado->isValid()) {
                        $objUsuarioAutenticado = $authAdapter->getResultRowObject(null, 'senha');

                        $storage = $auth->getStorage();
                        $storage->write($storage);

                        $sessaoUser = new Zend_Session_Namespace();
                        $sessaoUser->usuario = array(
                        'id' => $objUsuarioAutenticado->id,
                        'nome' => $objUsuarioAutenticado->nome,
                        'email' => $objUsuarioAutenticado->email
                        );

                        $objSessionNamespace = new Zend_Session_Namespace();
                        $objSessionNamespace->setExpirationSeconds(86400);

                        $_SESSION['Default']['usuario']['admin'] = false;
                        $_SESSION['Default']['usuario']['dados'] = $objCliente->getDadosCliente($objUsuarioAutenticado->id);
                        $_SESSION['Default']['usuario']['endereco'] = $enderecoAtual;
                    }
                    
                
                //efeuta o pagamento, se cartão de crédito ou boleto do mercado 19
                if($post['forma_pagamento'] === "35"){

                        $_SESSION['Default']['usuario']['pedidoFinalizado'] = $dadosPedido;

                        unset($_SESSION['Default']['usuario']['carrinho']);
                        unset($_SESSION['Default']['usuario']['mercado']);

                        $this->_redirect('/carrinho/fim');
                        
                }else if($post['forma_pagamento'] === "36"){
                    
//                    //envia boleto por email
//                    //redireciona para fim com link boleto
                    $_SESSION['Default']['usuario']['pedidoFinalizado'] = $dadosPedido;
                    $_SESSION['Default']['usuario']['linkBoleto'] = $post['urlBoleto'];
                                        
                    unset($_SESSION['Default']['usuario']['carrinho']);
                    unset($_SESSION['Default']['usuario']['mercado']);
                    $this->_redirect('/carrinho/fim-boleto');
                }else{
                    $_SESSION['Default']['usuario']['pedidoFinalizado'] = $dadosPedido;

                    unset($_SESSION['Default']['usuario']['carrinho']);
                    unset($_SESSION['Default']['usuario']['mercado']);
                    $this->_redirect('/carrinho/fim');
                }
        }
        
        if (!empty($_SESSION['Default']['usuario']['carrinho']['cliente'])) {
            $this->view->cliente = $objCliente->fetchRow("id = {$_SESSION['Default']['usuario']['carrinho']['cliente']['id']}");
        } else {
            if (!empty($_SESSION['Default']['usuario']['carrinho']['emailCliente'])) {  
                $this->view->cliente =  array (
                  'email' =>  $_SESSION['Default']['usuario']['carrinho']['emailCliente']
                );
            } 
        }
        
        if (empty($_POST['bairro']) && !empty($_SESSION['Default']['usuario']['endereco']['bairro'])) {
            $_POST['bairro'] = $_SESSION['Default']['usuario']['endereco']['bairro'];
        }

    }

//    public function confirmarDadosActionOLD() {
//        $objCliente = new Application_Model_DbTable_Cliente();
//        $objMercado = new Application_Model_DbTable_Mercado();
//        $objPedido = new Application_Model_DbTable_Pedido();
//        $objProdutoMercado = new Application_Model_DbTable_ProdutoMercado();
//        $objPedidoItens = new Application_Model_DbTable_PedidoItem();
//        $objClienteEndereco = new Application_Model_DbTable_ClienteEndereco();
//        $objRecuperacaoCarrinho = new Application_Model_DbTable_RecuperacaoCarrinho();
//        $post = $this->getRequest()->getPost();
//
//        if (empty($_SESSION['Default']['usuario']['mercado'])) {
//           $this->_redirect('/carrinho/checkout'); 
//        }
//
//        if ($_SESSION['Default']['usuario']['mercado']['totalProdutos'] == 0) {
//            $this->_redirect('/'); 
//        }
//
//        if (!empty($this->mercadoMaster['id'])) {
//            if ($this->mercadoMaster['id'] != $_SESSION['Default']['usuario']['mercado']['id']) {
//                $this->_redirect('/'); 
//            }
//        }
//
//        $this->view->mercado = $_SESSION['Default']['usuario']['mercado'];
//        $cepAtual = str_replace("-", "", $_SESSION['Default']['usuario']['endereco']['cep']);
//
//        if (!empty($post)) {
//
//            //verifica campos
//            $validacao = new Application_Model_Validacao();
//            $arrayListValidacao = array(
//                'NotEmpty' => array(
//                    'nome' => array('Nome', $post['user']),
//                    'email' => array('E-Mail', $post['email']),
//                    'cpf' => array('CPF', $post['cpf']),
//                    'queroReceber' => array('Quer Receber', $post['queroReceber']),
//                    'forma_pagamento' => array('Forma de Pagamento', $post['forma_pagamento'])
//                )
//            );
//
//            $validacao->check($arrayListValidacao);
//            $erros = $validacao->getErros();
//
//            if (empty($erros)) {
//                //verifica pra evitar duplicidade de cpf de cliente
//                $dadosCliente = $objCliente->fetchRow("email = '{$post['email']}'");
//
//                $arrayCliente = array(
//                    'nome' => $post['user'],
//                    'email' => $post['email'],
//                    'cpf' => $post['cpf'],
//                    'telefone' => $post['phone'],
//                    'celular' => $post['celular']
//                );
//
//                //para novos usuários com senha
//                if (!empty($post['pass'])) {
//                    $arrayCliente['senha'] = hash('sha512', $post['pass']);
//                }
//
//                //verifica se é um cliente ou não e salva ou adiciona novo registro de cliente
//                if (!empty($dadosCliente)) {
//                    $idCliente = $dadosCliente['id'];
//                    $objCliente->save($arrayCliente,"id = {$idCliente}");
//                } else {
//                    $objCliente->save($arrayCliente);
//                    $idCliente = $objCliente->getAdapter()->lastInsertId();
//
//                    // manda email de boas vindas
//                    $front = Zend_Controller_Front::getInstance()->getBaseUrl();
//                    $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;
//                    $baseUrl2 = 'http://'. $_SERVER['HTTP_HOST'] . $front;
//                    $baseUrl2 = str_replace("public","", $baseUrl2);
//
//                    $mail = new Zend_Mail('utf-8');
//
//                    $mail->setSubject("Recuperar Senha - ".$this->mercadoMaster['nome']);
//
//                    if (!empty($this->mercadoMaster['foto'])) {
//                      $mail->setFrom('naoresponda@suacomprafacil.com.br', $this->mercadoMaster['nome']);
//                    } else {
//                      $mail->setFrom('naoresponda@suacomprafacil.com.br', "Mercado Fique Em Casa");
//                    }
//                      
//                      $html = array();
//                      $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
//                      $html[] = '<tr>';
//                      $html[] = '<td width="600" align="center">';
//                      $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
//                      $html[] = '<tr>';
//                      $html[] = '<td height="120" bgcolor="#022e6d" align="center" valign="center"><p>';
//                    if (!empty($this->mercadoMaster['foto'])) {
//                      $html[] = '<img src="' . $baseUrl2 . '/admin/public/uploads/mercados/'.$this->mercadoMaster['foto'].'" />';      
//                    } else {
//                      $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
//                    }
//                      $html[] = '</p></td>';
//                      $html[] = '</tr>';
//                      $html[] = '<tr>';
//                      $html[] = '<td bgcolor="#FFFFFF" valign="center">';
//                      $html[] = '<p><font face="verdana" size="3" color="#000000">';
//                      $html[] = 'Bem vindo ao '.$this->mercadoMaster['nome'].'<br /><br />';
//                      $html[] = 'A partir de agora você poderá fazer suas compras em diversos supermercados, sem sair de casa. Com poucos cliques você escolhe seus produtos e envia seu pedido.<br /><br /> ';
//                      $html[] = 'O supermercado que receber o pedido fará a separação de seus produtos com todo cuidado e deixará pronto para a equipe de logística fazer a entrega onde desejar ou para você passar no supermercado para fazer a retirada. <br /><br />';
//                      $html[] = 'O processo é bem simples e rápido, saiba que a maioria dos supermercados fazem a entrega no mesmo dia de sua compra e você pode pagar somente quando receber a compra! <br /><br />';
//                      $html[] = '<a href="'.$baseUrl.'">Quero fazer uma compra</a><br />';
//                      $html[] = '</p>';
//                      $html[] = '</td>';
//                      $html[] = '</tr>';
//                      $html[] = '<tr>';
//                      $html[] = '<td height="30" bgcolor="#022e6d" align="center" valign="center">';
//                      $html[] = '<p>&nbsp;</p>';
//                      $html[] = '</td>';
//                      $html[] = '</tr>';
//                      $html[] = '</table>';
//                      $html[] = '</td>';
//                      $html[] = '</tr>';
//                      $html[] = '</table>';
//
//                      $mail->setBodyHtml(join("\n", $html));
//                      $mail->addTo($post['email'], $post['email']);
//                      $retorno = $mail->send($tr);
//                      
//                }
//
//                $enderecoAtual = $_SESSION['Default']['usuario']['endereco'];
//
//                //pega dados do cliente novo
//                $dadosCliente = $objCliente->fetchRow("email = '{$post['email']}'");
//
//                if ($dadosCliente) {
//
//                    //verifica se já tenho um endereço com esse cep cadastrado
//                    $qEndereco = $objClienteEndereco->fetchRow("cep = '{$enderecoAtual['cep']}' AND cliente = {$idCliente}");
//
//                    $dadosCidadeEstado = explode(' - ',$enderecoAtual['cidade']);
//
//                    $qCidade = $objClienteEndereco->getAdapter()->fetchRow("
//                        SELECT
//                            c.id
//                        FROM
//                            enderecos_cidades c,
//                            enderecos_estados e
//                        WHERE
//                            c.cidade = '{$dadosCidadeEstado[0]}'
//                            AND
//                            e.uf = '{$dadosCidadeEstado[1]}'
//                            AND
//                            c.idEstado = e.id
//                    ");
//
//                    //se for entrega, verifica para já adicionar o endereço na lista do usuário
//                    if (empty($qEndereco)) {
//                        if ($post['queroReceber'] == 1) {
//                            $arrayEndereco = array(
//                                'cep' => $enderecoAtual['cep'],
//                                'rua' => $enderecoAtual['rua'],
//                                'cliente' => $idCliente,
//                                'numero' => $post['numero'],
//                                'complemento' => $post['complemento'],
//                                'bairro' => $enderecoAtual['bairro'],
//                                'cidade' => $qCidade['id']
//                            );
//                            $objClienteEndereco->save($arrayEndereco);
//                        } else {
//                            $arrayEndereco = array(
//                                'cep' => $enderecoAtual['cep'],
//                                'rua' => $enderecoAtual['rua'],
//                                'cliente' => $idCliente,
//                                'numero' => '',
//                                'complemento' => '',
//                                'bairro' => $enderecoAtual['bairro'],
//                                'cidade' => $qCidade['id']
//                            );
//                            $objClienteEndereco->save($arrayEndereco);
//                        }
//                    }
//
//                    $dbAdapter = Zend_Db_Table::getDefaultAdapter();
//                    $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
//                    $authAdapter->setAmbiguityIdentity(true);
//
//                    //valida base do mercado
//                    $authAdapter->setTableName('clientes')
//                      ->setIdentityColumn('email')
//                      ->setCredentialColumn('senha');
//
//                    $authAdapter->setIdentity($dadosCliente['email'])->setCredential($dadosCliente['senha']);
//
//                    $auth = Zend_Auth::getInstance();
//                    $autenticado = $auth->authenticate($authAdapter);
//
//                    if (!empty($autenticado) && $autenticado->isValid()) {
//                        $objUsuarioAutenticado = $authAdapter->getResultRowObject(null, 'senha');
//
//                        $storage = $auth->getStorage();
//                        $storage->write($storage);
//
//                        $sessaoUser = new Zend_Session_Namespace();
//                        $sessaoUser->usuario = array(
//                        'id' => $objUsuarioAutenticado->id,
//                        'nome' => $objUsuarioAutenticado->nome,
//                        'email' => $objUsuarioAutenticado->email
//                        );
//
//                        $objSessionNamespace = new Zend_Session_Namespace();
//                        $objSessionNamespace->setExpirationSeconds(86400);
//
//                        $_SESSION['Default']['usuario']['admin'] = false;
//                        $_SESSION['Default']['usuario']['dados'] = $objCliente->getDadosCliente($objUsuarioAutenticado->id);
//                        $_SESSION['Default']['usuario']['endereco'] = $enderecoAtual;
//                    }
//                }
//
//                //se for retirada nao adiciona a taxa cobrada porque é retirada
//                if ($post['queroReceber'] == 2) {
//                    $taxa = 0;
//                } else {
//                    $taxa = $this->view->mercado['taxacobrada'];
//                }
//
//                //adiciona dados do pedido
//                $arrayPedido = array(
//                    'idCliente' => $dadosCliente['id'],
//                    'idMercado' => $this->view->mercado['id'],
//                    'status' => 1,
//                    'valortotal' => $this->view->mercado['totalProdutos'],
//                    'taxacobrada' => $taxa,
//                    'statuspagamento' => 0,
//                    'tipoentrega' => $post['queroReceber'],
//                    'horaentrega' => $post['hora_entrega'],
//                    'horaretirada' => $post['hora_retirada'],
//                    'formapagamento' => $post['forma_pagamento'],
//                    'numero' => $post['numero'],
//                    'complemento' => $post['complemento'],
//                    'bairro' => $_SESSION['Default']['usuario']['endereco']['bairro'],
//                    'troco' => $post['troco'],
//                    'cep' => $_SESSION['Default']['usuario']['endereco']['cep'],
//                    'endereco' => $_SESSION['Default']['usuario']['endereco']['rua'],
//                    'cidade' => $_SESSION['Default']['usuario']['endereco']['cidade']
//
//                );
//
//                
//                if (!empty($post['dia_entrega'])) {
//                    $diaEntrega = join('-', array_reverse(explode('/', $post['dia_entrega'])));
//                    $arrayPedido['dataentregaretirada'] = $diaEntrega;
//                } else {
//                    $diaRetirada = join('-', array_reverse(explode('/', $post['dia_retirada'])));
//                    $arrayPedido['dataentregaretirada'] = $diaRetirada;
//                }
//
//                $arrayPedido['outrapessoa'] = 0;
//
//                if (!empty($post['outro_nome'])) {
//                    $arrayPedido['outrapessoanome'] = $post['outro_nome'];
//                    $arrayPedido['outrapessoa'] = 1;
//                }
//
//                if (!empty($post['outro_telefone'])) {
//                    $arrayPedido['outrapessoatelefone'] = $post['outro_telefone'];
//                }
//
//                if (!empty($post['outro_rg'])) {
//                    $arrayPedido['outrapessoarg'] = $post['outro_rg'];
//                }
//
//                $objPedido->save($arrayPedido);
//                $idPedido = $objPedido->getAdapter()->lastInsertId();
//
//                //verifica se esse cliente tem alguma recuperacao pendente
//                $objRecuperacaoCarrinho->save(array("pedido" => $idPedido),"cliente = {$dadosCliente['id']} AND pedido is null");
//
//                //loop pelos itens
//                foreach ($this->view->mercado['produtos'] as $umProduto) {
//
//                    // só faz uma verificacao para nao duplicar itens
//                    $dadoItenPedido = $objPedidoItens->fetchRow("idPedido = {$idPedido} AND idProduto = {$umProduto['id']}");
//
//                    if (empty($dadoItenPedido)) {
//                        $arrayProduto = array(
//                            'idProduto' => $umProduto['id'],
//                            'idPedido' => $idPedido,
//                            //'preco' => $umProduto['valorMercado']['preco'],
//                            'quantidade' => $umProduto['quantidade']
//                        );
//
//                        if ($umProduto['tipo'] == 0) {
//                            $arrayProduto['preco'] = $umProduto['valorMercado'] / 1000;
//                        } else {
//                            $arrayProduto['preco'] = $umProduto['valorMercado'];
//                        }
//
//                        $objPedidoItens->save($arrayProduto);
//                    } else {
//                        $arrayProduto = array(
//                            'quantidade' => $umProduto['quantidade'] + $dadoItenPedido['quantidade']
//                        );
//
//                        $objPedidoItens->save($arrayProduto, "id = {$dadoItenPedido['id']}");
//                    }
//
//                    $dadoItenPedido = $objPedidoItens->fetchRow("idPedido = {$idPedido} AND idProduto = {$umProduto['id']}");
//
//                    // remove estoque do item no mercado
//                    $qEstoqueAtual = $objProdutoMercado->fetchRow("idProduto = {$umProduto['id']} AND idMercado = {$this->view->mercado['id']}");
//
//                    if (!empty($qEstoqueAtual)) {
//                        $arrayProduto = array(
//                            'estoque' => $qEstoqueAtual['estoque'] - $dadoItenPedido['quantidade']
//                        );
//                        $objProdutoMercado->save($arrayProduto, "id = {$qEstoqueAtual['id']}");
//                    }
//                }
//
//                $dadosPedido = $objPedido->getDadosPedido($idPedido);
//
//                // finaliza o pedido
//                $_SESSION['Default']['usuario']['pedidoFinalizado'] = $dadosPedido;
//
//                unset($_SESSION['Default']['usuario']['carrinho']);
//                unset($_SESSION['Default']['usuario']['mercado']);
//                $this->_redirect('/carrinho/fim');
//            }
//        }
//
//        if (!empty($_SESSION['Default']['usuario']['carrinho']['cliente'])) {
//            $this->view->cliente = $objCliente->fetchRow("id = {$_SESSION['Default']['usuario']['carrinho']['cliente']['id']}");
//        } else {
//            if (!empty($_SESSION['Default']['usuario']['carrinho']['emailCliente'])) {  
//                $this->view->cliente =  array (
//                  'email' =>  $_SESSION['Default']['usuario']['carrinho']['emailCliente']
//                );
//            } 
//        }
//
//
//        if (empty($_POST['bairro']) && !empty($_SESSION['Default']['usuario']['endereco']['bairro'])) {
//            $_POST['bairro'] = $_SESSION['Default']['usuario']['endereco']['bairro'];
//        }
//
//    }

    public function fimAction() {
        
        if (empty($_SESSION['Default']['usuario']['pedidoFinalizado'])) {
            $this->_redirect('/'); 
        }

        $dadosUsuario = $_SESSION['Default']['usuario']['pedidoFinalizado'];

        $troco = "";

        if (!empty($dadosUsuario['troco'])) { 
            $troco = " Troco para: " . $dadosUsuario['troco'];
        }

        // envia e-mail para o mercado
        $front = Zend_Controller_Front::getInstance()->getBaseUrl();
        $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;
        $baseUrl2 = 'http://'. $_SERVER['HTTP_HOST'] . $front;
        $baseUrl2 = str_replace("public","", $baseUrl2);

        $mail = new Zend_Mail('utf-8');
        $mail->setSubject("Pedido Realizado");

        if (!empty($this->mercadoMaster['foto'])) {
            $mail->setFrom('mercadofiqueemcasa@gmail.com', $this->mercadoMaster['nome']);
        } else {
            $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");
        }

        $html = array();
        $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
        $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
        $html[] = '<tr>';
        $html[] = '<td width="600" align="center">';
        $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
        $html[] = '<tr>';
        $html[] = '<td height="80" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
        
        if (!empty($this->mercadoMaster['foto'])) {
          $html[] = '<img src="' . $baseUrl2 . '/admin/public/uploads/mercados/'.$this->mercadoMaster['foto'].'" />';      
        } else {
          $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
        }

        $html[] = '</td>';
        $html[] = '</tr>';
        $html[] = '<tr><td><table style="width:540px!important;" align="center">';
        $html[] = '<tr><td bgcolor="#FFFFFF" valign="center" colspan="4">';
        $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
        $html[] = 'O cliente '.$dadosUsuario['cliente']['nome'].' realizou o pedido abaixo:<br />';
        $html[] = '</p>';
        $html[] = '<table style="width:100%!important;margin-bottom:20px;border-bottom:1px solid #eaeaea!important;"><tr>';
        $html[] = '</tr></table>';
        $html[] = '</td></tr>';
        
        foreach ($dadosUsuario['itens'] as $umItem) {

            $totalQde = 0;
            $totalPreco = 0;

            if ($umItem['produto']['tipo'] == 0 ) { 
                $totalPreco = number_format($umItem['preco'] * $umItem['quantidade'], 2, ',', '.');
                
                if ($umItem['quantidade'] < 1000) {
                    $totalQde = $umItem['quantidade'] . " grama(s)"; 
                } else {
                    $totalQde = $umItem['quantidade'] / 1000 . " kg"; 
                }
                
            } else {
                $totalPreco = number_format($umItem['preco'], 2, ',', '.');
                $totalQde = $umItem['quantidade'];
            } 


            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;">'.$umItem['produto']['nome'].'</td>';
            $html[] = '<td cellpadding="0" cellspacing="0" style="color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;"><table style="width:100%!important;"><tr><td style="width:70%!important;text-align:right!important;">'.$totalQde.'</td><td style="width:30%!important;text-align:right!important;">R$ '.$totalPreco.'</td></tr></table></td></tr>';    
        }
        
        
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="width: 100%!important;height: 10px!important;margin: 0!important;padding: 0!important;" colspan="2">&nbsp;</td></tr>';
        
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-top:1px solid #eaeaea!important;border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Supermercado</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-top:1px solid #eaeaea!important;border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'.$dadosUsuario['mercado']['nome'].'</td>';
        $html[] = '</tr>';
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;width:50%;">Número do pedido</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;width:50%;">#'. str_pad($dadosUsuario['id'], 10, '0', STR_PAD_LEFT) .'</td></tr>';

        if ($dadosUsuario['tipoentrega'] == 1) {
            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Entrega</td>';
        } else {
            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Retirada</td>';    
        }
        
        $html[] = '<td cellpadding="0" cellspacing="0" style="text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'. $dadosUsuario['horaentrega'] .'</td></tr>';
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Forma de Pagamento</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'.$dadosUsuario['formaDePagamento']['nome'] .  $troco  .' </td></tr>';
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Valor da compra</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">R$ '.number_format($dadosUsuario['valortotal'], 2, ',', '.').'</td></tr>';
        if ($dadosUsuario['tipoentrega'] == 1) {
            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Taxa de Serviço</td>';
            $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">R$ '.number_format($dadosUsuario['taxacobrada'], 2, ',', '.').'</td></tr>';
        }
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px solid #eaeaea!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;font-weight:bold!important;">Valor total</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px solid #eaeaea!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;font-weight:bold!important;">R$ '.number_format($dadosUsuario['taxacobrada'] + $dadosUsuario['valortotal'], 2, ',', '.').'</td></tr>';
        $html[] = '</table></td></tr>';
        $html[] = '<tr>';
        $html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
        $html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">'.$baseUrl2.'</a>';
        $html[] = '</td>';
        $html[] = '</tr>';
        $html[] = '</table>';
        $html[] = '</td>';
        $html[] = '</tr>';
        $html[] = '</table>';
        $html[] = '</table></td></tr>';

        $mail->setBodyHtml(join("\n", $html));
        $mail->addTo($_SESSION['Default']['usuario']['pedidoFinalizado']['mercado']['email'], $_SESSION['Default']['usuario']['pedidoFinalizado']['mercado']['email']);
        
        $objConfigMail = new Application_Model_ConfigMail();
        $transport = $objConfigMail->get_configuracao_mail();
        $mail->send($transport);

        enviaEmailClientePedidoRecebido($dadosUsuario);
        
        $this->view->dadosPedido = $_SESSION['Default']['usuario']['pedidoFinalizado'];
        
        unset($_SESSION['Default']['usuario']['pedidoFinalizado']);

    }
    
    public function fimBoletoAction() {

        if (empty($_SESSION['Default']['usuario']['pedidoFinalizado'])) {
            $this->_redirect('/'); 
        }

        $dadosUsuario = $_SESSION['Default']['usuario']['pedidoFinalizado'];

        $troco = "";

        if (!empty($dadosUsuario['troco'])) { 
            $troco = " Troco para: " . $dadosUsuario['troco'];
        }

        // envia e-mail para o mercado
        $front = Zend_Controller_Front::getInstance()->getBaseUrl();
        $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;
        $baseUrl2 = 'http://'. $_SERVER['HTTP_HOST'] . $front;
        $baseUrl2 = str_replace("public","", $baseUrl2);

        $mail = new Zend_Mail('utf-8');
        $mail->setSubject("Pedido Realizado");

        if (!empty($this->mercadoMaster['foto'])) {
            $mail->setFrom('mercadofiqueemcasa@gmail.com', $this->mercadoMaster['nome']);
        } else {
            $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");
        }

        $html = array();
        $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
        $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
        $html[] = '<tr>';
        $html[] = '<td width="600" align="center">';
        $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
        $html[] = '<tr>';
        $html[] = '<td height="80" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
        
        if (!empty($this->mercadoMaster['foto'])) {
          $html[] = '<img src="' . $baseUrl2 . '/admin/public/uploads/mercados/'.$this->mercadoMaster['foto'].'" />';      
        } else {
          $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
        }

        $html[] = '</td>';
        $html[] = '</tr>';
        $html[] = '<tr><td><table style="width:540px!important;" align="center">';
        $html[] = '<tr><td bgcolor="#FFFFFF" valign="center" colspan="4">';
        $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
        $html[] = 'O cliente '.$dadosUsuario['cliente']['nome'].' realizou o pedido abaixo:<br />';
        $html[] = '</p>';
        $html[] = '<table style="width:100%!important;margin-bottom:20px;border-bottom:1px solid #eaeaea!important;"><tr>';
        $html[] = '</tr></table>';
        $html[] = '</td></tr>';
        
        foreach ($dadosUsuario['itens'] as $umItem) {

            $totalQde = 0;
            $totalPreco = 0;

            if ($umItem['produto']['tipo'] == 0 ) { 
                $totalPreco = number_format($umItem['preco'] * $umItem['quantidade'], 2, ',', '.');
                
                if ($umItem['quantidade'] < 1000) {
                    $totalQde = $umItem['quantidade'] . " grama(s)"; 
                } else {
                    $totalQde = $umItem['quantidade'] / 1000 . " kg"; 
                }
                
            } else {
                $totalPreco = number_format($umItem['preco'], 2, ',', '.');
                $totalQde = $umItem['quantidade'];
            } 


            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;">'.$umItem['produto']['nome'].'</td>';
            $html[] = '<td cellpadding="0" cellspacing="0" style="color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;"><table style="width:100%!important;"><tr><td style="width:70%!important;text-align:right!important;">'.$totalQde.'</td><td style="width:30%!important;text-align:right!important;">R$ '.$totalPreco.'</td></tr></table></td></tr>';    
        }
        
        
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="width: 100%!important;height: 10px!important;margin: 0!important;padding: 0!important;" colspan="2">&nbsp;</td></tr>';
        
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-top:1px solid #eaeaea!important;border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Supermercado</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-top:1px solid #eaeaea!important;border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'.$dadosUsuario['mercado']['nome'].'</td>';
        $html[] = '</tr>';
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;width:50%;">Número do pedido</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;width:50%;">#'. str_pad($dadosUsuario['id'], 10, '0', STR_PAD_LEFT) .'</td></tr>';

        if ($dadosUsuario['tipoentrega'] == 1) {
            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Entrega</td>';
        } else {
            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Retirada</td>';    
        }
        
        $html[] = '<td cellpadding="0" cellspacing="0" style="text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'. $dadosUsuario['horaentrega'] .'</td></tr>';
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Forma de Pagamento</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'.$dadosUsuario['formaDePagamento']['nome'] .  $troco  .' </td></tr>';
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Valor da compra</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">R$ '.number_format($dadosUsuario['valortotal'], 2, ',', '.').'</td></tr>';
        if ($dadosUsuario['tipoentrega'] == 1) {
            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Taxa de Serviço</td>';
            $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">R$ '.number_format($dadosUsuario['taxacobrada'], 2, ',', '.').'</td></tr>';
        }
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px solid #eaeaea!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;font-weight:bold!important;">Valor total</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px solid #eaeaea!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;font-weight:bold!important;">R$ '.number_format($dadosUsuario['taxacobrada'] + $dadosUsuario['valortotal'], 2, ',', '.').'</td></tr>';
        $html[] = '</table></td></tr>';
        $html[] = '<tr>';
        $html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
        $html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">'.$baseUrl2.'</a>';
        $html[] = '</td>';
        $html[] = '</tr>';
        $html[] = '</table>';
        $html[] = '</td>';
        $html[] = '</tr>';
        $html[] = '</table>';
        $html[] = '</table></td></tr>';

        $mail->setBodyHtml(join("\n", $html));
        $mail->addTo($_SESSION['Default']['usuario']['pedidoFinalizado']['mercado']['email'], $_SESSION['Default']['usuario']['pedidoFinalizado']['mercado']['email']);
       
        $objConfigMail = new Application_Model_ConfigMail();
        $transport = $objConfigMail->get_configuracao_mail();
        $mail->send($transport);

        $this->view->dadosPedido = $_SESSION['Default']['usuario']['pedidoFinalizado'];
        $this->view->linkBoleto = $_SESSION['Default']['usuario']['linkBoleto'];
        
        unset($_SESSION['Default']['usuario']['pedidoFinalizado']);

    }
    
    private function enviaEmailClientePedidoRecebido($dadosUsuario){
        // envia e-mail para o mercado
        $front = Zend_Controller_Front::getInstance()->getBaseUrl();
        $baseUrl = 'http://'. $_SERVER['HTTP_HOST'] . $front;
        $baseUrl2 = 'http://'. $_SERVER['HTTP_HOST'] . $front;
        $baseUrl2 = str_replace("public","", $baseUrl2);

        $mail = new Zend_Mail('utf-8');
        $mail->setSubject("Pedido Realizado");

        if (!empty($this->mercadoMaster['foto'])) {
            $mail->setFrom('mercadofiqueemcasa@gmail.com', $this->mercadoMaster['nome']);
        } else {
            $mail->setFrom('mercadofiqueemcasa@gmail.com', "Mercado Fique Em Casa");
        }

        $html = array();
        $html[] = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="padding:20px!important;background:#f7f7f7!important;">';
        $html[] = '<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff!important;width:600px!important;">';
        $html[] = '<tr>';
        $html[] = '<td width="600" align="center">';
        $html[] = '<table width="600" border="0" cellpadding="10" cellspacing="0">';
        $html[] = '<tr>';
        $html[] = '<td height="80" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
        
        if (!empty($this->mercadoMaster['foto'])) {
          $html[] = '<img src="' . $baseUrl2 . '/admin/public/uploads/mercados/'.$this->mercadoMaster['foto'].'" />';      
        } else {
          $html[] = '<img src="' . $baseUrl . '/img/logo-email.png" />';      
        }

        $html[] = '</td>';
        $html[] = '</tr>';
        $html[] = '<tr><td><table style="width:540px!important;" align="center">';
        $html[] = '<tr><td bgcolor="#FFFFFF" valign="center" colspan="4">';
        $html[] = '<p style="color:#666666!important;font-size:14px!important;">';
        $html[] = 'Olá '.$dadosUsuario['cliente']['nome'].'<br /><br />';
        $html[] = 'Seu pedido foi devidamente pago e já entrou no sistema para tratamento!<br /><br />';
        $html[] = 'Obrigado pelo privilégio de poder abastecer sua casa. Sua segurança e conforto são a nossa missão e prioridade ❤!<br /><br />';
        $html[] = 'Enviamos abaixo o relatório de suas compras, este serve como recibo, é bom sempre dar uma olhadinha só pra garantir que não faltou nada e se tiver algum problema já pode dar uma avisadinha pra gente no Whatsapp ou aqui no W-mail para que possamos garantir sua satisfação. <br /><br />';
        $html[] = 'É importante resaltar que precisamos de um adulto acima de 18 anos presenta para fazer a entrega, principalmente caso haja bebidas alcóolicas no seu pedido. <br /><br />';
        $html[] = 'Pela situação atual, podem faltar alguns produtos no nosso estoque, caso isso aconteça a gente substituirá por um produto similar da mesma qualidade ou superior.  <br /><br />';
        $html[] = 'Mais uma ultima coisa, por favor continue se cuidando, a gente sabe que é meio chato então escolhemos aqui um vídeo engraçado para animar sua quarentena, fique seguro e dê MUITAS risadas. <br /><br />';
        $html[] = 'Forte abraço (virtual obviamente), em breve estaremos entregando suas comprinhas.<br /><br />';
        $html[] = '<a href="'.$baseUrl.'" style="background:#71a505!important;color:#ffffff!important;display:block!important;height:40px!important;line-height:40px!important;text-decoration:none!important;width:200px!important;text-align:center!important;margin:0 auto!important;">Assistir Vídeo!</a><br />';
        $html[] = '</p>';
        $html[] = '<table style="width:100%!important;margin-bottom:20px;border-bottom:1px solid #eaeaea!important;"><tr>';
        $html[] = '</tr></table>';
        $html[] = '</td></tr>';
        
        foreach ($dadosUsuario['itens'] as $umItem) {

            $totalQde = 0;
            $totalPreco = 0;

            if ($umItem['produto']['tipo'] == 0 ) { 
                $totalPreco = number_format($umItem['preco'] * $umItem['quantidade'], 2, ',', '.');
                
                if ($umItem['quantidade'] < 1000) {
                    $totalQde = $umItem['quantidade'] . " grama(s)"; 
                } else {
                    $totalQde = $umItem['quantidade'] / 1000 . " kg"; 
                }
                
            } else {
                $totalPreco = number_format($umItem['preco'], 2, ',', '.');
                $totalQde = $umItem['quantidade'];
            } 


            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;">'.$umItem['produto']['nome'].'</td>';
            $html[] = '<td cellpadding="0" cellspacing="0" style="color:#666666!important;font-size:11px!important;padding:4px 0!important;width:50%!important;"><table style="width:100%!important;"><tr><td style="width:70%!important;text-align:right!important;">'.$totalQde.'</td><td style="width:30%!important;text-align:right!important;">R$ '.$totalPreco.'</td></tr></table></td></tr>';    
        }
        
        
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="width: 100%!important;height: 10px!important;margin: 0!important;padding: 0!important;" colspan="2">&nbsp;</td></tr>';
        
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-top:1px solid #eaeaea!important;border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Supermercado</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-top:1px solid #eaeaea!important;border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'.$dadosUsuario['mercado']['nome'].'</td>';
        $html[] = '</tr>';
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;width:50%;">Número do pedido</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;width:50%;">#'. str_pad($dadosUsuario['id'], 10, '0', STR_PAD_LEFT) .'</td></tr>';

        if ($dadosUsuario['tipoentrega'] == 1) {
            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Entrega</td>';
        } else {
            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Retirada</td>';    
        }
        
        $html[] = '<td cellpadding="0" cellspacing="0" style="text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'. $dadosUsuario['horaentrega'] .'</td></tr>';
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Forma de Pagamento</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">'.$dadosUsuario['formaDePagamento']['nome'] .  $troco  .' </td></tr>';
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Valor da compra</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">R$ '.number_format($dadosUsuario['valortotal'], 2, ',', '.').'</td></tr>';
        if ($dadosUsuario['tipoentrega'] == 1) {
            $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">Taxa de Serviço</td>';
            $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px dashed #aaaaaa!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;">R$ '.number_format($dadosUsuario['taxacobrada'], 2, ',', '.').'</td></tr>';
        }
        $html[] = '<tr><td cellpadding="0" cellspacing="0" style="border-bottom:1px solid #eaeaea!important;text-align:left!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;font-weight:bold!important;">Valor total</td>';
        $html[] = '<td cellpadding="0" cellspacing="0" style="border-bottom:1px solid #eaeaea!important;text-align:right!important;color:#666666!important;font-size:14px!important;padding:10px 0!important;width:50%!important;font-weight:bold!important;">R$ '.number_format($dadosUsuario['taxacobrada'] + $dadosUsuario['valortotal'], 2, ',', '.').'</td></tr>';
        $html[] = '</table></td></tr>';
        $html[] = '<tr>';
        $html[] = '<td height="20" bgcolor="#004280" align="center" valign="center" cellpadding="0" cellspacing="0">';
        $html[] = '<a style="color:#ffffff!important;text-decoration:none!important;" href="'.$baseUrl.'">'.$baseUrl2.'</a>';
        $html[] = '</td>';
        $html[] = '</tr>';
        $html[] = '</table>';
        $html[] = '</td>';
        $html[] = '</tr>';
        $html[] = '</table>';
        $html[] = '</table></td></tr>';

        $mail->setBodyHtml(join("\n", $html));
        $mail->addTo($_SESSION['Default']['usuario']['pedidoFinalizado']['mercado']['email'], $_SESSION['Default']['usuario']['pedidoFinalizado']['mercado']['email']);
        
        $objConfigMail = new Application_Model_ConfigMail();
        $transport = $objConfigMail->get_configuracao_mail();
        $mail->send($transport);
    }


}
