<?php

class Default_CategoriaController extends PainelBW_Painel {

    public function init() {
        Zend_Auth::getInstance()->hasIdentity();
        parent::init();
        $this->base = $this->getFrontController()->getBaseUrl();
    }

    public function indexAction() {
    	$id = $this->getRequest()->getParam('id');
        $post = $this->getRequest()->getPost();
        $objCategoria = new Application_Model_DbTable_Categoria();
        $objProduto = new Application_Model_DbTable_Produto();

        if (empty($id)) {
        	$this->_redirect('/');
        } else {

        	
            $dadosRegistro = $objCategoria->fetchRow("codigofiltro = '{$id}'");

            if (empty($dadosRegistro)) {
        		$this->_redirect('/');
        	}

            $this->view->id = $dadosRegistro['id'];

            $arrayBreadcrumbs = $objCategoria->montaBreadCrumbs($dadosRegistro['id']);
            $arrayBreadcrumbs = $objCategoria->getBreadCrumbs();
            
        	$this->view->dadosRegistro = $dadosRegistro;

        	$arrayListCategoria = $objCategoria->fetchAll("categoria = {$dadosRegistro['id']}");

        	$arrayListCategoriaFinal = array();

            if (empty($arrayListCategoria)) {
                $arrayListCategoriaFinal[0]['dadosCategoria'] = $dadosRegistro;
                $arrayListCategoriaFinal[0]['subCategorias'] = [];
                $arrayListCategoriaFinal[0]['produtos'] = $objProduto->getProdutos($dadosRegistro['id'], null, 4, null, null, null, null, null, null, null, $this->mercadoMaster['id']);
                $key = 0;
                $ids = $dadosRegistro['id'];

                //tipos e marcas para filtros
                $sqlMarcas = "SELECT 
                                        a.* 
                                    FROM 
                                        atributos a
                                    LEFT JOIN
                                        produtos p ON (p.marca = a.id)
                                    WHERE 
                                        p.categoria = {$ids}
                                        AND
                                        (select count(id) from produtos_mercados where idProduto = p.id) > 0
                                    GROUP BY
                                        a.valor
                                    ORDER BY
                                        a.valor";
                $arrayMarcas = $objCategoria->getAdapter()->fetchAll($sqlMarcas);

                $arrayListCategoriaFinal[$key]['marcas'] = $arrayMarcas;

                $sqlTipos = "SELECT 
                                a.* 
                            FROM 
                                atributos a
                            LEFT JOIN
                                produtos p ON (p.tipoAt = a.id)
                            WHERE 
                                p.categoria = {$ids}
                                AND
                                (select count(id) from produtos_mercados where idProduto = p.id) > 0
                            GROUP BY
                                a.valor
                            ORDER BY
                                a.valor";

                $arrayTipos = $objCategoria->getAdapter()->fetchAll($sqlTipos);

                $arrayListCategoriaFinal[$key]['tipos'] = $arrayTipos;

            } else {

                foreach ($arrayListCategoria as $key => $umaCategoria) {

                    $arrayListCategoriaFinal[$key]['dadosCategoria'] = $umaCategoria;
                    $arrayListCategoriaFinal[$key]['subCategorias'] = [];
                    $arrayListCategoriaFinal[$key]['produtos'] = $objProduto->getProdutos($umaCategoria['id'], null, 4, null, null, null, null, null, null, null, $this->mercadoMaster['id']);

                    // quando nao tiver produtos nao mostra na listagem de categorias
                    if (empty($arrayListCategoriaFinal[$key]['produtos'])) {

                        $sqlSubCategorias = "SELECT id FROM categorias WHERE categoria = {$umaCategoria['id']}";

                        $arrayListCategoria2 = $objCategoria->getAdapter()->fetchAll($sqlSubCategorias);

                        if (!empty($arrayListCategoria2)) {

                            foreach ($arrayListCategoria2 as $umaSub2) {
                                $arrayListCategoriaFinal[$key]['subCategorias'][] = $umaSub2['id'];    
                            }

                            $arrayListCategoriaFinal[$key]['produtos'] = $objProduto->getProdutos(null, null, 4, null, null, null, null, null, $arrayListCategoria2, null, $this->mercadoMaster['id']);

                            if (empty($arrayListCategoriaFinal[$key]['produtos'])) {
                                unset($arrayListCategoriaFinal[$key]);
                                unset($arrayListCategoria[$key]);
                                continue;    
                            } else {

                                //tipos e marcas para filtros
                                $sqlMarcas = "SELECT 
                                                a.* 
                                            FROM 
                                                atributos a
                                            LEFT JOIN
                                                produtos p ON (p.marca = a.id )
                                            LEFT JOIN
                                                categorias c ON (p.categoria = c.id)
                                            WHERE 
                                                c.categoria = {$umaCategoria['id']}
                                                AND
                                                (select count(id) from produtos_mercados where idProduto = p.id) > 0
                                            GROUP BY
                                                a.valor
                                            ORDER BY
                                                a.valor";

                                $arrayMarcas = $objCategoria->getAdapter()->fetchAll($sqlMarcas);

                                $arrayListCategoriaFinal[$key]['marcas'] = $arrayMarcas;

                                $sqlTipos = "SELECT 
                                                a.* 
                                            FROM 
                                                atributos a
                                            LEFT JOIN
                                                produtos p ON (p.tipoAt = a.id)
                                            LEFT JOIN
                                                categorias c ON (p.categoria = c.id)
                                            WHERE 
                                                c.categoria = {$umaCategoria['id']}
                                                AND
                                                (select count(id) from produtos_mercados where idProduto = p.id) > 0
                                            GROUP BY
                                                a.valor
                                            ORDER BY
                                                a.valor";

                                $arrayTipos = $objCategoria->getAdapter()->fetchAll($sqlTipos);

                                $arrayListCategoriaFinal[$key]['tipos'] = $arrayTipos;

                            }

                        } else {
                            unset($arrayListCategoriaFinal[$key]);
                            unset($arrayListCategoria[$key]);
                            continue;    
                        }

                        

                    } else {

                        //tipos e marcas para filtros
                        $sqlMarcas = "SELECT 
                                        a.* 
                                    FROM 
                                        atributos a
                                    LEFT JOIN
                                        produtos p ON (p.marca = a.id)
                                    WHERE 
                                        p.categoria = {$umaCategoria['id']}
                                            AND
                                        (select count(id) from produtos_mercados where idProduto = p.id) > 0
                                    GROUP BY
                                        a.valor
                                    ORDER BY
                                        a.valor";

                        $arrayMarcas = $objCategoria->getAdapter()->fetchAll($sqlMarcas);

                        $arrayListCategoriaFinal[$key]['marcas'] = $arrayMarcas;

                        $sqlTipos = "SELECT 
                                        a.* 
                                    FROM 
                                        atributos a
                                    LEFT JOIN
                                        produtos p ON (p.tipoAt = a.id)
                                    WHERE 
                                        p.categoria = {$umaCategoria['id']}
                                            AND
                                        (select count(id) from produtos_mercados where idProduto = p.id) > 0
                                    GROUP BY
                                        a.valor
                                    ORDER BY
                                        a.valor";

                        $arrayTipos = $objCategoria->getAdapter()->fetchAll($sqlTipos);

                        $arrayListCategoriaFinal[$key]['tipos'] = $arrayTipos;

                    }
                }    
            }

        	$this->view->subCategorias = $arrayListCategoriaFinal;

            Zend_Layout::getMvcInstance()->assign('arrayBreadcrumbs', $arrayBreadcrumbs);
        	Zend_Layout::getMvcInstance()->assign('subCategorias', $arrayListCategoria);
        	Zend_Layout::getMvcInstance()->assign('categoriaPai', $dadosRegistro);

        }
    }

}
